const namespace = 'HomeAutomation.User';

export function displayAdminPanel() {

  return new Promise ((resolve, reject) => {

    let oLoggedInUser = HomeAutomation.Auth.checkPrivilege();

    if(oLoggedInUser.loggedIn && oLoggedInUser.admin) {

      let oData = {};
      oData.rootURL = HomeAutomation.Config.ROOT_URL;

      HomeAutomation.Util.executeHandlebars('admin-panel', 'AdminPanel', oData, 'clear');
    }

    return resolve();
  });
}

export function displayUserSettings() {

  let oLoggedInUser = HomeAutomation.Auth.checkPrivilege();

  if(oLoggedInUser.loggedIn && oLoggedInUser.admin) {
    HomeAutomation.Util.executeHandlebars('adminChangePasswordFormHolder', 'AdminPasswordChangeForm', oLoggedInUser, 'clear');
    HomeAutomation.Util.executeHandlebars('guestUserListHolder', 'GuestUserListHolder', oLoggedInUser, 'clear', displayGuestUserList());
  } else {
    document.getElementById('adminChangePasswordFormHolder').innerHTML = '';
    document.getElementById('guestUserListHolder').innerHTML = '';
  }

  HomeAutomation.Util.executeHandlebars('loggedInUserHolder', 'LoggedInUser', oLoggedInUser, 'clear');

}

export function onChangeAdminPassword() {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onChangeAdminPassword',
      params: []
    }
  };

  let eCurrentPasswordField = document.getElementById('existingPasswordField');
  let eNewPasswordField = document.getElementById('newPasswordField');
  let eNewPasswordRepeatField = document.getElementById('newPasswordRepeatField');

  let oChangePassword = {
    existingPassword: eCurrentPasswordField.value,
    newPassword: eNewPasswordField.value,
    newPasswordRepeat: eNewPasswordRepeatField.value
  }

  let oErrorModel = validateAdminPasswordChange(oChangePassword);

  let oData = {
    errors: oErrorModel,
    values: oChangePassword,
    admin: true
  }

  HomeAutomation.Util.executeHandlebars('adminChangePasswordFormHolder', 'AdminPasswordChangeForm', oData, 'clear');

  if(Object.keys(oErrorModel).length > 0) {
    return;
  }

  HomeAutomation.User.Service.patchAdminPassword(oAuthData, oChangePassword).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {

      if (oResult.type == "ERR") {
        oData.errors.responseError = `${oResult.header} ${oResult.details}`;
        HomeAutomation.Util.executeHandlebars('adminChangePasswordFormHolder', 'AdminPasswordChangeForm', oData, 'clear');
        return;
      }

    } else {
      oData.values = {};
      oData.errors = {};
      oData.showSuccess = true;
      HomeAutomation.Util.executeHandlebars('adminChangePasswordFormHolder', 'AdminPasswordChangeForm', oData, 'clear');
    }
  });
}

function validateAdminPasswordChange(oChangePassword) {

  let oErrorModel = {};

  let sRequiredMessage = 'This field is required.';
  let bIsError = false;

  if (!validateRequiredVariable(oChangePassword.existingPassword)) {
    addError('existingPassword', createFieldError(true, sRequiredMessage), oErrorModel);
    bIsError = true;
  }

  if (!validateRequiredVariable(oChangePassword.newPassword)) {
    addError('newPassword', createFieldError(true, sRequiredMessage), oErrorModel);
    bIsError = true;
  }

  if (!validateRequiredVariable(oChangePassword.newPasswordRepeat)) {
    addError('newPasswordRepeat', createFieldError(true, sRequiredMessage), oErrorModel);
    bIsError = true;
  }

  if(bIsError) {
    return oErrorModel;
  }

  if(oChangePassword.newPassword != oChangePassword.newPasswordRepeat) {
    addError('newPassword', createFieldError(true, 'The passwords do not match.'), oErrorModel);
    addError('newPasswordRepeat', createFieldError(true, 'The passwords do not match.'), oErrorModel);
  }

  return oErrorModel;
}

function validateRequiredVariable(sValue) {
  if (sValue != "" && sValue != undefined) {
    return true;
  }
  return false;
}

function addError(sError, sErrorText, oErrorModel) {
  oErrorModel[sError] = sErrorText;
}

function createFieldError(bShowMessage, sMessage) {
  let oErrorSegment = {
    errClass: 'is-invalid'
  }
  if (bShowMessage) {
    oErrorSegment.errMsg = sMessage;
  }
  return oErrorSegment;
}

var _LOADED_TOPICS_ = null;

function setLoadedTopics(aTopics) {
  _LOADED_TOPICS_ = aTopics;
}

export function getLoadedTopics() {
  return _LOADED_TOPICS_;
}

export function displayGuestUserList() {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'displayGuestUserList',
      params: []
    }
  };

  HomeAutomation.User.Service.getAllGuestUsers(oAuthData).then(function(aUsers) {

    if (!HomeAutomation.Util.validateWebserviceCall(aUsers, 'guest-user-list-holder')) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aUsers, new HomeAutomation.Class.Message('No Users Found.'), 'guest-user-list-holder')) return;

    let oData = {};
    oData.users = aUsers;

    HomeAutomation.Util.executeHandlebars('guest-user-list-holder', 'GuestUserList', oData, 'clear');
  });

}

export function openGuestAddEditModal(iUserId, oArgs) {

  if (oArgs) {
    var iUserId = oArgs[0];
  }

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'openGuestAddEditModal',
      params: [iUserId]
    }
  };

  let oData = {
    view: {}
  };

  if (iUserId) {

    HomeAutomation.User.Service.getOneGuestUser(oAuthData, iUserId).then(function(oLoadedUser) {

      if (!HomeAutomation.Util.validateWebserviceCall(oLoadedUser)) return;
      oData.user = oLoadedUser;
      oData.view.edit = true;
      displayAddEditModel(oData);
    });

  } else {

    oData.view.add = true;
    displayAddEditModel(oData);
  }

  function displayAddEditModel(oData) {

    HomeAutomation.Topic.Service.getAll(oAuthData).then(function(aTopics) {

      if (!HomeAutomation.Util.validateWebserviceCall(aTopics)) return;

      if(oData.user && oData.user.topics) {
        aTopics.forEach((topic, i) => {

          oData.user.topics.forEach((userTopic, i) => {
            if(userTopic.id == topic.id) {
              topic.enabled = true;
            }
          });
        });
      }

      setLoadedTopics(aTopics);
      oData.view.topics = aTopics;

      HomeAutomation.Util.executeHandlebars('modal-content', 'GuestUserAddEdit', oData, 'clear');

    });
  }
}

export function onAddOrUpdate(iUserId, oArgs) {

  if (oArgs) {
    var iUserId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onAddOrUpdate',
      params: [iUserId]
    }
  };


  let eUsernameField = document.getElementById('usernameField');
  let ePasswordField = document.getElementById('passwordField');
  let eEnabledField = document.getElementById('enabledField');

  let oUser = {
    username: eUsernameField.value,
    enabled: eEnabledField.checked
  }

  if(ePasswordField.value != '') {
    oUser.password = ePasswordField.value;
  }

  let bPasswordRequired = false;
  if(!iUserId) {
    bPasswordRequired = true;
  } else {
    oUser.id = iUserId;
  }

  let oErrorModel = validateGuestUser(oUser, bPasswordRequired);

  let aTopics = getCheckedTopics();

  let oData = {
    view: {}
  };

  oData.view.add = true;
  if(iUserId) {
    oData.view.edit = true;
  }
  oData.view.topics = aTopics;
  oData.user = oUser;
  oData.errors = oErrorModel;

  HomeAutomation.Util.executeHandlebars('modal-content', 'GuestUserAddEdit', oData, 'clear');


  // There were validation errors
  if(Object.keys(oErrorModel).length > 0) {
    return;
  }

  // All is good, get only enabled topics and do put
  let aLoadedTopics = getLoadedTopics();

  let aSelectedTopics = new Array();

  aLoadedTopics.forEach((oTopic, i) => {
    if(oTopic.enabled && oTopic.enabled == true) {
      aSelectedTopics.push(oTopic);
    }
  });

  let oUserToSave = oData.user = oUser;
  oUserToSave.topics = aSelectedTopics;

  if(!oUserToSave.id) {
    addGuestUser(oUserToSave);
  } else {
    updateGuestUser(oUserToSave);
  }

  function addGuestUser(oUserToSave) {

    HomeAutomation.User.Service.addGuestUser(oAuthData, oUserToSave).then(function(oResult) {

      updateAndShowList(oResult);
    });
  }

  function updateGuestUser(oUserToSave) {

    HomeAutomation.User.Service.updateGuestUser(oAuthData, oUserToSave).then(function(oResult) {
      updateAndShowList(oResult);
    });

  }

  function updateAndShowList(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      if (oResult.type == "ERR") {
        oResult.displayText('addEditFormErrorHolder');
        return;
      }
    }

    displayGuestUserList();
    HomeAutomation.Util.closeModal();
  }

  function getCheckedTopics() {
    let aLoadedTopics = getLoadedTopics();

    aLoadedTopics.forEach((iTopic, i) => {

      let eCheckBox = document.getElementById(`topicCheck-${iTopic.id}`);

      iTopic.enabled = false;
      if(eCheckBox.checked) {
        iTopic.enabled = true;
      }
    });

    setLoadedTopics(aLoadedTopics);
    return aLoadedTopics;
  }

  function validateGuestUser(oUser, bPasswordRequired) {

    let oErrorModel = {};

    let sRequiredMessage = 'This field is required.';
    let bIsError = false;

    if (!validateRequiredVariable(oUser.username)) {
      addError('username', createFieldError(true, sRequiredMessage), oErrorModel);
      bIsError = true;
    }

    if(bPasswordRequired) {
      if (!validateRequiredVariable(oUser.password)) {
        addError('password', createFieldError(true, sRequiredMessage), oErrorModel);
        bIsError = true;
      }
    }

    if(bIsError) {
      return oErrorModel;
    }

    return oErrorModel;
  }
}

export function onDeleteConfirmation(iUserId) {

  let oData = {
    header: 'Confirm Deletion',
    message: 'Are you sure you want to delete this user?',
    callback: `HomeAutomation.User.onDelete(${iUserId})`
  };

  HomeAutomation.Util.executeHandlebars('modal-content', 'DeleteConfirmation', oData, 'clear');

}

export function onDelete(iUserId, oArgs) {

  if (oArgs) {
    var iUserId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onDelete',
      params: [iUserId]
    }
  }

  HomeAutomation.User.Service.removeGuestUser(oAuthData, iUserId).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {

      oResult.displayModal();

    } else {

      displayGuestUserList();
      HomeAutomation.Util.closeModal();
    }
  });
}

export function logoutUser() {

  HomeAutomation.Auth.clearAuthToken();
  HomeAutomation.User.displayUserSettings()
}

export function loginUser() {

  let oCallback = {
    namespace: namespace,
    func: 'displayUserSettings',
    params: []
  }

  HomeAutomation.Util.openModal(HomeAutomation.Auth.openAuthenticationModal(oCallback));
}
