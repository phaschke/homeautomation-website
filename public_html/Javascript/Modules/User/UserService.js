const namespace = 'HomeAutomation.User.Service';

export function patchAdminPassword(oAuthData, oUser) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', JSON.stringify(oUser), '', '/api/users/change_admin_password', oAuthData, true);

  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error updating admin password', xhrObj);
  });
}

export function getOneGuestUser(oAuthData, iUserId) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/users/guest_user/${iUserId}`, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Guest User', xhrObj);
  });

}

export function getAllGuestUsers(oAuthData) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', '/api/users/guest_users', oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return unpackResponse(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Guest Users', xhrObj);
  });

}

export function addGuestUser(oAuthData, oUser) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oUser), 'json', '/api/users/guest_user', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {


    return HomeAutomation.Util.buildErrorMessage('Error Adding Guest User', xhrObj);
  });
}

export function updateGuestUser(oAuthData, oUser) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oUser), 'json', `/api/users/guest_user/${oUser.id}`, oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Updating User', xhrObj);
  });
}

export function removeGuestUser(oAuthData, iUserId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, '', `/api/users/guest_user/${iUserId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Deleting Guest User', xhrObj);
  });
}

function unpackResponse(response) {

  if (response._embedded && response._embedded.users) {
    return response._embedded.users;
  } else {
    return [];
  }
}
