export function validateWebserviceCall(oResult, sErrorHolder) {

  if (oResult instanceof HomeAutomation.Class.Message) {
    oResult.setType("ERR");
    oResult.setAppend(false);
    if (sErrorHolder != undefined && sErrorHolder != "") {
      oResult.displayText(sErrorHolder);
    } else {
      oResult.displayModal();
    }

    return false;
  }
  return true;
}

export function validateListLengthGreaterThanZero(aArray, oErrorMessage, sErrorHolder) {

  if (!aArray instanceof Array) {
    showError(oErrorMessage, sErrorHolder);
    return false;
  }
  if (aArray.length == 0) {
    showError(oErrorMessage, sErrorHolder);
    return false;
  }

  return true;

  function showError(oErrorMessage, sErrorHolder) {

    if (oErrorMessage != undefined) {
      oErrorMessage.setType("ERR");
      oErrorMessage.setAppend(false);

      if (sErrorHolder != undefined && sErrorHolder != "") {
        oErrorMessage.displayText(sErrorHolder);

      } else {
        oErrorMessage.displayModal();
      }
    }
  }
}

export function buildErrorMessage(sErrorMessage, oXhrObj) {

  let oMessage = new HomeAutomation.Class.Message(sErrorMessage, oXhrObj.responseText);
  oMessage.setType("ERR");

  return oMessage;
}

export function executeHandlebars(templateHolderSelector, templateName, jsonData, appendClearFlag, fCallback) {

  if (appendClearFlag === "clear") {
    $("#" + templateHolderSelector).html("");
  }

  $("#" + templateHolderSelector).append(Handlebars.templates[templateName](jsonData));

  if (fCallback && typeof(fCallback) === "function") {
    fCallback.apply(this);
  }
}

export function scrollToElement(sElementId) {

  document.getElementById(sElementId).scrollIntoView();
}

export function openModal(oModalDetails, fCallback, aParameterList) {

  let sModalId = 'modal';
  let oModal = new bootstrap.Modal(document.getElementById(sModalId), {});

  oModal.show();

  if (fCallback && typeof(fCallback) === 'function') {
    fCallback.apply(this, aParameterList);
  }
}

export function openCustomModal(oModalDetails, fCallback, aParameterList) {

  let sModalId = 'modal';
  let sModalClasses = 'modal-dialog';

  if (oModalDetails != null) {

    if (oModalDetails.id) {
      sModalId = oModalDetails.id;
    }

    if (oModalDetails.size) {
      switch (oModalDetails.size) {
        case 'sm':
          sModalClasses += ' modal-sm';
          break;
        case 'lg':
          sModalClasses += ' modal-lg';
          break;
        case 'xl':
          sModalClasses += ' modal-xl';
          break;
        default:
          sModalClasses += ' modal-dialog';
      }
    }

    if (oModalDetails.position) {
      switch (oModalDetails.position) {
        case 'center':
          sModalClasses += ' modal-dialog-centered';
          break;
      }
    }

    if (oModalDetails.scrollable) {
      sModalClasses += ' modal-dialog-scrollable';
    }
  }

  //let eModal = document.getElementById('modal-custom');
  let eModal = document.getElementById(oModalDetails.id);
  let oModal = new bootstrap.Modal(eModal, {});

  let eDialog = eModal.firstElementChild;
  eDialog.className = sModalClasses;

  oModal.show();

  if (fCallback && typeof(fCallback) === "function") {
    fCallback.apply(this, aParameterList);
  }
}

export function closeModal(sModalId) {

  let isDefault = false;

  if (!sModalId) {
    sModalId = 'modal';
    isDefault = true;
  }

  let eModal = document.getElementById(sModalId)
  let oModal = bootstrap.Modal.getInstance(eModal)

  oModal.hide();

  let oBackDropElems = document.getElementsByClassName('modal-backdrop');
  //console.log(document.getElementsByClassName('modal-backdrop'));
  if (isDefault) {
    let oBackDropElems = document.getElementsByClassName('modal-backdrop');
    if (oBackDropElems.length > 0) {
      /*for(var i = 0; i < oBackDropElems.length; i++) {
          if(oBackDropElems[i].style) {
            oBackDropElems[i].style.display = "none";
          }
        }*/
      oBackDropElems[0].parentNode.removeChild(oBackDropElems[0]);
    }
    //if(oBackDropElems[0].style) {
    //  oBackDropElems[0].style.display = "none";
    //}
  }

}

export function displayLoadingModal() {
  HomeAutomation.Util.executeHandlebars("modal-content", "ModalLoading", "", "clear");
}

export function executeFunctionByName(functionName, context, args) {

  var args = Array.prototype.slice.call(arguments, 2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for (var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(context, args);
}


export function isEmpty(string) {
  return (!string || !string.length || !string.trim());
}

export function validateField(fieldId, fieldErrorHolderId) {
  var field = document.getElementById(fieldId);

  if (field.type === "text") {
    var value = field.value;
  }
  if (field.type.startsWith("select")) {
    var value = field.options[field.selectedIndex].value;
  }

  if (this.isEmpty(field.value) || value === "null") {
    this.addFieldError(fieldId, fieldErrorHolderId, "This field is required.");
    return false;
  }
  this.removeFieldError(fieldId, fieldErrorHolderId);
  return true;
}

export function validateObjectNameInput(sFieldId, sFieldErrorHolderId) {

  let eField = document.getElementById(sFieldId);

  let sValue = eField.value;

  if (! /^[a-zA-Z0-9\-\_]+$/.test(sValue)) {
    this.addFieldError(sFieldId, sFieldErrorHolderId, 'Object name can only contain alphanumeric characters, underscores(_), and dashes(-).');
    return false;
  }
  this.removeFieldError(sFieldId, sFieldErrorHolderId);
  return true;
}

export function addFieldError(sFieldId, sFieldErrorHolderId, sFieldError) {
  this.addInputFieldErrorStyling(sFieldId);
  this.setFieldError(sFieldErrorHolderId, sFieldError);
  let oField = document.getElementById(sFieldId);
  oField.focus();
}

export function removeFieldError(sFieldId, sFieldErrorHolderId) {
  this.clearInputFieldStyling(sFieldId);
  this.setFieldError(sFieldErrorHolderId, "");
}

export function setFieldError(errorHolderId, message) {
  $("#" + errorHolderId).html(message);
}

export function addInputFieldErrorStyling(elementId) {
  this.setElementClass(elementId, "form-control is-invalid");
}

export function clearInputFieldStyling(elementId) {
  this.setElementClass(elementId, "form-control");
}

export function setElementClass(elementId, classToSet) {

  document.getElementById(elementId).className = classToSet;
}

export function addElementClass(elementId, classToAdd) {
  document.getElementById(elementId).classList.add(classToAdd);
}

export function removeElementClass(elementId, classToRemove) {
  if(elementId && classToRemove) {
    document.getElementById(elementId).classList.remove(classToRemove);
  }
}

export function redirect(url, fCallback) {
  location.href = url;

  if (fCallback && typeof(fCallback) === "function") {
    fCallback.apply(this);
  }
  return true;
}

export function addToast(sType, sHeader, sMessage) {

  let iToastId = Math.floor((Math.random() * 1000) + 1);

  let oData = {
    id: iToastId,
    header: sHeader,
    message: sMessage
  }

  if(sType === 'success') {
    oData.type = 'success';
  }
  if(sType === 'error') {
    oData.type = 'danger';
  }

  HomeAutomation.Util.executeHandlebars('toast-container', 'Toast', oData, 'append');

  let eToast = document.getElementById(`toast-${iToastId}`);
  eToast.addEventListener('hidden.bs.toast', function () {
    removeToast(eToast);
  });

  $(`#toast-${iToastId}`).toast('show');
}

function removeToast(eToast) {
  eToast.parentNode.removeChild(eToast);
}
