export function registerHelpers() {
  Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper("select", function(value, options) {
    return options.fn(this)
      .split('\n')
      .map(function(v) {
        var t = 'value="' + value + '"'
        return !RegExp(t).test(v) ? v : v.replace(t, t + ' selected="selected"')
      })
      .join('\n')
  });

  Handlebars.registerHelper('maxLength', function(sText, iLength, options) {
    if(sText.length > iLength) {
      return sText.substring(0, iLength) + '..';
    }
    return sText;
  });
}

Handlebars.registerHelper('greaterThan', function (v1, v2, options) {
'use strict';
   if (v1>v2) {
     return options.fn(this);
  }
  return options.inverse(this);
});

export function registerPartials() {
  // Register templates as partials
  //Handlebars.partials = Handlebars.templates;
  Handlebars.registerPartial('SWITCH', Handlebars.templates.ControlSwitch);
  Handlebars.registerPartial('SENSOR', Handlebars.templates.ControlSensor);
  Handlebars.registerPartial('LED_CONTROLLER', Handlebars.templates.ControlLED);
  Handlebars.registerPartial('SWITCH_EDIT', Handlebars.templates.ControlSwitchEdit);
  Handlebars.registerPartial('SENSOR_EDIT', Handlebars.templates.ControlSensorEdit);
  Handlebars.registerPartial('LED_CONTROLLER_EDIT', Handlebars.templates.ControlLEDEdit);

  Handlebars.registerPartial('CONTROL_ADD_EDIT_HEADER', Handlebars.templates.ControlAddEditHeader);
  Handlebars.registerPartial('CONTROL_ADD_EDIT_FOOTER', Handlebars.templates.ControlAddEditFooter);
  Handlebars.registerPartial('CONTROL_ADD_EDIT_MAIN', Handlebars.templates.ControlAddEditMain);
  Handlebars.registerPartial('CONTROL_ADD_EDIT_PERSIST', Handlebars.templates.ControlAddEditPersist);
  Handlebars.registerPartial('STATUS_LIST_BUILDER', Handlebars.templates.ControlStatusListBuilder);
  Handlebars.registerPartial('EVENT_LIST_BUILDER', Handlebars.templates.ControlEventListBuilder);

  Handlebars.registerPartial('CONTROL_EDIT_TITLE', Handlebars.templates.ControlEditTitle);
  Handlebars.registerPartial('CONTROL_ADD_EDIT_SWITCH', Handlebars.templates.ControlSwitchAddEdit);
  Handlebars.registerPartial('CONTROL_ADD_EDIT_SENSOR', Handlebars.templates.ControlSensorAddEdit);
  Handlebars.registerPartial('CONTROL_ADD_EDIT_LED_CONTROLLER', Handlebars.templates.ControlLEDAddEdit);
  Handlebars.registerPartial('CONTROL_FILTER', Handlebars.templates.ControlFilter);
  Handlebars.registerPartial('CONTROL_SEARCH_LIST', Handlebars.templates.ControlSearchList);

  Handlebars.registerPartial('ACTION_ADD_EDIT_HEADER', Handlebars.templates.ActionAddEditHeader);
  Handlebars.registerPartial('ACTION_ADD_EDIT_FOOTER', Handlebars.templates.ActionAddEditFooter);
  Handlebars.registerPartial('ACTION_IF_FORM', Handlebars.templates.ActionIfForm);
  Handlebars.registerPartial('ACTION_TASK_CONTROL_FORM', Handlebars.templates.ActionTaskControlForm);
  Handlebars.registerPartial('ACTION_FILTER', Handlebars.templates.ActionFilter);
  Handlebars.registerPartial('ACTION_SEARCH_LIST', Handlebars.templates.ActionSearchList);
  Handlebars.registerPartial('ACTION_SUMMARY_IF', Handlebars.templates.ActionSummaryIfStep);
  Handlebars.registerPartial('ACTION_SUMMARY_TASK', Handlebars.templates.ActionSummaryTaskStep);

  Handlebars.registerPartial('EVENT_SELECT_FORM', Handlebars.templates.EventFormPart);
  Handlebars.registerPartial('EVENT_FILTER', Handlebars.templates.EventFilter);

  Handlebars.registerPartial('SCHEDULE_FILTER', Handlebars.templates.ScheduleFilter);
  Handlebars.registerPartial('HOURLY', Handlebars.templates.ScheduleHourly);
  Handlebars.registerPartial('DAILY', Handlebars.templates.ScheduleDaily);
  Handlebars.registerPartial('WEEKLY', Handlebars.templates.ScheduleWeekly);
  Handlebars.registerPartial('MONTHLY', Handlebars.templates.ScheduleMonthly);
  Handlebars.registerPartial('YEARLY', Handlebars.templates.ScheduleYearly);
  Handlebars.registerPartial('FIXED', Handlebars.templates.ScheduleFixed);

}
