const namespace = 'HomeAutomation.DataLoader';

export function loadTopicSettings() {

  let oCallback = {
    namespace: namespace,
    func: 'loadTopicSettings',
    params: []
  };

  let oCalls = [
    {
      namespace: 'HomeAutomation.Topic',
      func: 'displayList',
      params: ['edit', oCallback]
    }
  ];

  loadData(oCalls);

}

export function loadDashboard() {

  let oCallback = {
    namespace: namespace,
    func: 'loadDashboard',
    params: []
  };

  let oCalls = [
    {
      namespace: 'HomeAutomation.Action.Button',
      func: 'displayByTopic',
      params: [-1, oCallback]
    },
    {
      namespace: 'HomeAutomation.Topic',
      func: 'displayList',
      params: ['view', oCallback]
    },
    {
      namespace: 'HomeAutomation.User',
      func: 'displayAdminPanel',
      params: [oCallback]
    }
  ];

  loadData(oCalls);

}

export function loadTopic(iTopicId, aArgs) {

  if(aArgs) {
    var iTopicId = aArgs[0];
  }

  let oCallback = {
    namespace: namespace,
    func: 'loadTopic',
    params: [iTopicId]
  };

  let oCalls = [
    {
      namespace: 'HomeAutomation.Action.Button',
      func: 'displayByTopic',
      params: [iTopicId, oCallback]
    },
    {
      namespace: 'HomeAutomation.Control',
      func: 'displayList',
      params: [iTopicId, oCallback]
    }
  ];

  loadData(oCalls);
}


export function loadData(aFunctionCalls) {

  let continueIf = true;

  for (let i = 0; i < aFunctionCalls.length; i++) {

    let oCall = aFunctionCalls[i];

    HomeAutomation.Util.executeFunctionByName(oCall.namespace + '.' + oCall.func, window, oCall.params)
      .then(result => {})
      .catch(error => {
        continueIf = false;
      });

    if (!continueIf) {
      break;
    }
  }
}
