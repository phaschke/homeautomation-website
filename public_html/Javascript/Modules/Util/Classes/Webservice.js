export class WebserviceRequest {

  constructor(type, data, dataType, uri, auth, processData) {
    this.type = type;
    this.data = data;
    this.dataType = dataType;
    this.uri = uri;
    this.auth = auth;
    this.processData = processData;
    this.serviceURL = HomeAutomation.Config.ROOT_WEBSERVICE_URL;
  }

  setServiceURL(sServiceURL) {
    this.serviceURL = sServiceURL;
  }

  authenticate() {

    this.headers = {};

    if (this.auth.check) {

      var token = localStorage.getItem("token");
      if (token && token !== "") {

        this.headers = {
          "Authorization": token
        };

        return true;

      } else {
        HomeAutomation.Auth.authenticateWebservice(this.auth.callback);

        return false;
      }
    }
  }

  authenticateAndMakeRequest(callback) {

    if (!this.headers) {
      if (!this.authenticate()) {
        return Promise.reject({
          responseText: "User is not authenticated."
        }, "", "");
      }
    }

    return Promise.resolve(
      $.ajax({
        type: this.type,
        processData: this.processData,
        headers: this.headers,
        data: this.data,
        dataType: this.dataType,
        contentType: 'application/json',
        url: this.serviceURL + this.uri,


        complete: function(data) {

          // If auth error, display login dialog
          if (data.status === 403) {

            // TODO: UPDATE TO USE authenticateAndMakeRequest
            HomeAutomation.Auth.authenticateWebservice(callback);

            return;
          }

          if (callback && typeof(callback) === "function") {
            callback(data);
          }
          return data;
        }

      })
    );
  } // End make call

} // End webservice class
