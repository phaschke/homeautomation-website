export class Filter {

  constructor() {
    this.filters = new Array();
    this.filterActive = true;
  }

  setActive(bState) {
    this.filterActive = bState;
  }

  getActive() {
    return this.filterActive;
  }

  addFilter(sFilter, sValue) {
    if (sFilter != '' && sFilter != undefined && sValue != '' && sValue != undefined) {

      this.filters.push({
        filter: sFilter,
        value: sValue
      });
    }
  }

  filterCount() {
    return this.filters.length;
  }

  toJSON() {

    let oJson = {};

    this.filters.forEach((item, i) => {
      oJson[item.filter] = item.value;
    });

    return oJson;
  }

  toURLRequest() {

    let sFilterRequest = '';

    if (this.filters.length > 0) {

      this.filters.forEach((item, i) => {
        if(i != 0) {
          sFilterRequest += `&${item.filter}=${item.value}`;
        } else {
          sFilterRequest += `${item.filter}=${item.value}`;
        }
      });

    return sFilterRequest;
  }

  return sFilterRequest;

}


}
