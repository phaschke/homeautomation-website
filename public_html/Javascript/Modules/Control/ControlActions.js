const namespace = 'HomeAutomation.Control.Action';

export function refreshControl(iControlId, args) {

  if (args) {
    var controlId = args[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'refreshControl',
      params: [iControlId]
    }
  };

  let oLoadedControl = new HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  oLoadedControl.render();

  HomeAutomation.Control.Service.getStatus(oAuthData, iControlId).then(function(oResponseControl) {

    if (!HomeAutomation.Util.validateWebserviceCall(oResponseControl)) return;

    let oControl = new HomeAutomation.Control.Class.Control(oResponseControl);

    oControl.render();
  });

}

export function openControlErrorModal(sHeader, sMessage) {

  var oData = {
    'header': sHeader,
    'details': sMessage
  };

  HomeAutomation.Util.executeHandlebars('modal-content', 'MessageModal', oData, 'clear');
}

// TODO: Remove after updating toggle controls to use objects .render()
export function displaySwitchElementOn(switchElement) {
  $(switchElement).attr('checked');
}

// TODO: Remove after updating toggle controls to use objects .render()
export function displaySwitchElementOff(switchElement) {
  $(switchElement).removeAttr('checked');
  $(switchElement).prop('checked', false);
}

// TODO: Remove after updating toggle controls to use objects .render()
export function disableControl(controlElement) {
  $(controlElement).addClass('disabled');
}

// TODO: Remove after updating toggle controls to use objects .render()
export function enableControl(controlElement) {
  $(controlElement).removeClass('disabled');
}

// TODO: Remove after updating toggle controls to use objects .render()
export function showControlLoadingGif(controlElement) {
  let loadingImg = $(controlElement).find('img');
  loadingImg.removeClass('invisible');
}

// TODO: Remove after updating toggle controls to use objects .render()
export function hideControlLoadingGif(controlElement) {
  let loadingImg = $(controlElement).find('img');
  loadingImg.addClass('invisible');
}

// TODO: Remove after updating toggle controls to use objects .render()
export function showControlLoadingIcon(controlElement, messageHeader, messageDetails) {
  let errorIcon = $(controlElement).find('i.icon-error');
  errorIcon.removeClass('invisible');

  if (messageHeader != '') {
    errorIcon.attr('onClick', "HomeAutomation.Util.openModal(HomeAutomation.Control.Action.openControlErrorModal('" + messageHeader + "', '" + messageDetails + "'))");
  }
}

// TODO: Remove after updating toggle controls to use objects .render()
export function hideControlErrorIcon(controlElement) {
  let errorIcon = $(controlElement).find('i.icon-error');
  errorIcon.addClass('invisible');
}
