export class ControlBuilder {

  constructor(controlJSON) {

    if (controlJSON === undefined) {
      var controlJSON = {};
    }
    if (controlJSON.topic && controlJSON.topic.id) {
      controlJSON.topic = controlJSON.topic.id;
    }

    this.id = controlJSON.id === undefined ? "" : controlJSON.id;
    this.title = controlJSON.title === undefined ? "" : controlJSON.title;
    this.uniqueName = controlJSON.uniqueName === undefined ? "" : controlJSON.uniqueName;
    this.active = controlJSON.active === undefined ? "" : controlJSON.active;
    this.description = controlJSON.description === undefined ? "" : controlJSON.description;
    this.type = controlJSON.type === undefined ? "" : controlJSON.type;
    this.broker = controlJSON.broker === undefined ? "" : controlJSON.broker;
    this.topicId = controlJSON.topic === undefined ? "" : controlJSON.topic;
    this.subtopic = controlJSON.subtopic === undefined ? "" : controlJSON.subtopic;
    this.persistState = controlJSON.persistState === undefined ? 0 : controlJSON.persistState;
    this.propertiesJSON = controlJSON.propertiesJSON;
    this.eventPropertiesJSON = controlJSON.eventPropertiesJSON;
    this.statusPropertiesJSON = controlJSON.statusPropertiesJSON === undefined ? JSON.parse('[]') : controlJSON.statusPropertiesJSON;
    this.actionPropertiesJSON = controlJSON.actionPropertiesJSON;

    this.variableToFieldMap = this.generateVariableToFieldMap();

  }

  //Creates a map between internal class variable names and frontend HTML template field names
  generateVariableToFieldMap() {

    let fieldMap = new Map();

    fieldMap.set("title", "controlTitleField");
    fieldMap.set("uniqueName", "controlNameField");
    fieldMap.set("description", "descriptionField");
    fieldMap.set("type", "controlTypeSelect");
    fieldMap.set("broker", "brokerField");
    fieldMap.set("topic", "topicSelect");
    fieldMap.set("subtopic", "subTopicField");

    return fieldMap;
  }

  parseJSON(input) {
    if (input == undefined) {
      return {};
    }
    return JSON.parse(input)
  }

  getType() {
    return this.type;
  }

  setType(sType) {
    this.type = sType;
  }

  setTopicId(iTopicId) {
    this.topicId = iTopicId;
  }

  setPersistState(iState) {
    this.persistState = iState;
  }

  getProperties() {
    return this.properties;
  }

  validateDataFields() {

    let aErroredFields = [];

    aErroredFields.push(this.validateVariable("title"));
    aErroredFields.push(this.validateVariable("uniqueName"));
    aErroredFields.push(this.validateVariable("type"));
    aErroredFields.push(this.validateVariable("broker"));
    aErroredFields.push(this.validateVariable("topic"));
    aErroredFields.push(this.validateVariable("subtopic"));

    return aErroredFields;
  }

  validateVariable(sVariableName) {
    let sFieldName = this.variableToFieldMap.get(sVariableName);
    let oResult = {
      fieldId: sFieldName,
      fieldErrorId: sFieldName + "ErrorHolder",
      fieldError: "This field is required."
    }
    let isError = false;
    if (this[sVariableName] == "") {
      isError = true;
    }
    oResult.isError = isError;
    return oResult;
  }

  returnDataForRequest() {

    let returnRequest = {
      id: this.id,
      topicId: this.topicId,
      active: this.active,
      uniqueName: this.uniqueName,
      title: this.title,
      type: this.type,
      subtopic: this.subtopic,
      persistState: this.persistState,
      description: this.description
    }

    let sBroker = this.broker;
    if(!sBroker.startsWith("tcp://")) {
      sBroker = "tcp://" + sBroker;
    }
    returnRequest.broker = sBroker;

    return returnRequest;
  }

  toJSON() {
    let toJSON = ({
      id: this.id,
      active: this.active,
      title: this.title,
      uniqueName: this.uniqueName,
      description: this.description,
      type: this.type,
      broker: this.broker.replace("tcp://", ""),
      topic: this.topic,
      loadedTopicId: this.loadedTopicId,
      subtopic: this.subtopic,
      persistState: this.persistState
    });

    return toJSON;
  }

}

export class AddEditControlViewProperties {

  constructor() {
    this.modalType = "";
    this.topics = [];
    this.template = "";
  }

  setModalType(sModalType) {
    this.modalType = sModalType;
  }

  setTopics(topics) {
    this.topics = topics;
  }

  setControlTemplate(sTemplate) {
    this.template = sTemplate
  }

  toJSON() {
    return {
      modalType: this.modalType,
      topics: this.topics,
      template: this.template
    }
  }

}

export class ControlAddEditModel {

  constructor(oAddEditControl, oViewProperties) {
    this.data = oAddEditControl;
    this.view = oViewProperties;
    this.addEditControlObject = null;
    this.loadedTopicId = "";
  }

  setData(oData) {
    this.data = oData;
  }

  getData() {
    return this.data;
  }

  setView(oView) {
    this.view = oView;
  }

  setLoadedTopicId(iLoadedTopicId) {
    this.loadedTopicId = iLoadedTopicId;
  }

  setControlType(sType) {
    this.getData().setType(sType);
    this.setControlObject();
    this.getControlTemplateName();
  }

  getControlObject() {
    return this.addEditControlObject;
  }

  setControlObject() {
    this.addEditControlObject = this.getAddEditControlObject(this.getData().getType());
  }

  getControlTemplateName() {

    let sTemplate = "";
    if (this.addEditControlObject != null && this.addEditControlObject != "") {
      sTemplate = this.addEditControlObject.getControlTemplateName();
    }
    this.view.setControlTemplate(sTemplate);
  }

  onControlFieldChange(sfield, oElement) {
    this.getData()[sfield] = oElement.value;
  }

  onControlActiveSelectChange(oElement) {

    if (oElement.checked) {
      this.getData().active = 1;
    } else {
      this.getData().active = 0;
    }
  }

  onControlTopicSelectChange(oElement) {
    let iTopicId = oElement.options[oElement.selectedIndex].value;
    this.getData().setTopicId(iTopicId);
  }

  onControlTypeSelectChange(oElement) {
    let sControlType = oElement.options[oElement.selectedIndex].value;
    this.setControlType(sControlType);
  }

  validateFields() {

    let aErroredFields = [];

    aErroredFields = this.getData().validateDataFields();
    if (this.getControlObject() != null) {
      let aSpecficTypeFieldErrors = this.getControlObject().validateControlSpecificFields();
      aErroredFields = aErroredFields.concat(aSpecficTypeFieldErrors);
    }
    return aErroredFields;
  }

  getFields() {

    let oFieldData = this.getData().returnDataForRequest();

    //if (this.getControlObject() != null) {
    //  oProperties = this.getControlObject().getControlSpecificProperties();
    //}
    try {
      oFieldData.properties = JSON.stringify(this.getControlObject().getProperties());
    } catch (error) {
      oFieldData.properties = null;
    }

    try {
      oFieldData.statusProperties = JSON.stringify(this.getControlObject().getStatusProperties());
    } catch (error) {
      oFieldData.statusProperties = null;
    }

    try {
      oFieldData.eventProperties = JSON.stringify(this.getControlObject().getEventProperties());
    } catch (error) {
      oFieldData.eventProperties = null;
    }

    return oFieldData;
  }

  toJSON() {
    var jsonArr = {};
    jsonArr.data = this.data.toJSON();
    jsonArr.view = this.view.toJSON();
    jsonArr.loadedTopicId = this.loadedTopicId;

    return jsonArr;
  }

  render() {

    this.getUIControlState(function(aElementState) {

      let oBaseModel = this.toJSON();
      let oModelFromSpecificType = {};
      if (this.getControlObject() != null && this.getControlObject() != "") {
        oModelFromSpecificType = this.getControlObject().toJSON();
      }

      let oModel = Object.assign({}, oBaseModel, oModelFromSpecificType);
      oModel.template = this.view.template;

      let that = this;

      HomeAutomation.Util.executeHandlebars("modal-content", "ControlAddEdit", oModel, "clear", function() {

        if(aElementState != undefined && aElementState.length > 0) {
          that.returnUIControlsToPreviousState(aElementState);
        }
      });

    });

  }

  getUIControlState(fCallback) {

    let aElementState = [];

    // Get any open accordians and remember state
    let aElements = document.getElementsByClassName('accordion-button');

    for(let i = 0; i < aElements.length; i++) {
      let eElement = aElements[i];
      if(!eElement.classList.value.includes('collapsed')) {

        aElementState.push({
          type: 'accordion',
          id: eElement.id,
          state: 'open'
        });
      }
    }

    if (fCallback && typeof(fCallback) === 'function') {
      fCallback.apply(this, new Array(aElementState));
    }
  }

  returnUIControlsToPreviousState(aElementState) {

    for(let i = 0; i < aElementState.length; i++) {

      let oElementState = aElementState[i];
      if(oElementState.type == 'accordion') {
        if(oElementState.state == 'open') {
          let sElementHeaderId = oElementState.id;
          let sElementSectionId = oElementState.id + '-Section';

          HomeAutomation.Util.removeElementClass(sElementHeaderId, 'collapsed');
          HomeAutomation.Util.addElementClass(sElementSectionId, 'show');
        }
      }
    }
  }

  getAddEditControlObject(sType) {

    var oAddEditControl = null;

    switch (sType) {

      case 'SWITCH':
        oAddEditControl = new HomeAutomation.Control.Class.SwitchBuilderClass(this);
        break;

      case 'SENSOR':
        oAddEditControl = new HomeAutomation.Control.Class.SensorBuilderClass(this);
        break;

      case 'LED_CONTROLLER':
        oAddEditControl = new HomeAutomation.Control.Class.LEDControlBuilderClass(this);
        break;
    }

    return oAddEditControl;
  }

}
