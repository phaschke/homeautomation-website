const namespace = 'HomeAutomation.Control.Timer';

const _SWITCH_TIMERS = [];
let _TIMER_INTERVAL_ACTIVE = false;

export function isTimerIntervalActive() {
  return _TIMER_INTERVAL_ACTIVE;
}

function setIntervalTimerActive(bActive) {
  _TIMER_INTERVAL_ACTIVE = bActive;
}

export function removeTimer(controlId) {

  var index = findTimer(controlId);
  if (index >= 0) {
    _SWITCH_TIMERS.splice(index, 1);
  }
  if (document.getElementById(`timer-${controlId}`)) {
    document.getElementById(`timer-${controlId}`).innerHTML = '';
  }
}

function findTimer(controlId) {

  for (var i = 0; i < _SWITCH_TIMERS.length; i++) {
    if (_SWITCH_TIMERS[i].id == controlId) {
      return i;
    }
  }
  return -1;
}

export function addTimerAndStartInterval(controlId, duration) {

  removeTimer(controlId);

  let timerJson = {
    id: controlId,
    seconds: (parseInt(duration, 10))
  }
  _SWITCH_TIMERS.push(timerJson);

  if (!isTimerIntervalActive()) {
    startTimers();
  }
}

export function openTimerSetModal(iControlId) {

  // Control id will be of the form {{controlId}}-{{optional params}}
  //console.log(controlId);

  var modalData = {
    controlId: iControlId
  };

  HomeAutomation.Util.executeHandlebars('modal-content', 'ControlSetTimer', modalData, 'clear');
}

export function setTimer(sControlTimerId) {

  console.log('set timer: '+sControlTimerId);

  if (validateFormInputs()) return false;

  let formInput = getFormInputValues();
  var iTimerSeconds = (formInput.h * 60 * 60) + (formInput.m * 60);

  if(iTimerSeconds === 0) return false;

  HomeAutomation.Util.closeModal();

  let aSplitControlTimerId = sControlTimerId.split('-');
  let iControlId = parseInt(aSplitControlTimerId[0], 10);

  let aTimerOptionalParams = [];
  if(aSplitControlTimerId.length >= 2)  {
    aSplitControlTimerId.shift()
    aTimerOptionalParams = aSplitControlTimerId;
  }

  // Get control object
  let oLoadedControl = HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  oLoadedControl.getControlObject().toggleDevice(iControlId, iTimerSeconds, aTimerOptionalParams);

  setControlTimerDisplay(sControlTimerId, iTimerSeconds);

  function validateFormInputs() {
    var isError = false;
    if (!HomeAutomation.Util.validateField('timerHourField', 'timerErrorHolder')) isError = true;
    if (!HomeAutomation.Util.validateField('timerMinuteField', 'timerErrorHolder')) isError = true;

    return isError;
  }

  function getFormInputValues() {
    var h = document.getElementById('timerHourField').value;
    var m = document.getElementById('timerMinuteField').value;

    return {
      h: h,
      m: m
    };
  }
}

export function startTimers() {

  if (isTimerIntervalActive()) {
    // Avoid starting multiple intervals
    return;
  }

  setIntervalTimerActive(true);

  // Update the count down every 1 second
  var toggleTimer = setInterval(function() {

    for (var i = 0; i < _SWITCH_TIMERS.length; i++) {

      var timer = _SWITCH_TIMERS[i];

      if (timer.seconds > 0) {
        //document.getElementById('timer-' + timer.id).innerHTML = formatSeconds(timer.seconds);
        setControlTimerDisplay(timer.id, timer.seconds);
        timer.seconds -= 1;

      }

      if (timer.seconds <= 0) {
        document.getElementById(`timer-${timer.id}`).innerHTML = '';
        document.getElementById(`switch-${timer.id}`).checked = false;
        _SWITCH_TIMERS.splice(i, 1);
      }
    }

    if (_SWITCH_TIMERS.length <= 0) {
      clearInterval(toggleTimer);
      setIntervalTimerActive(false);
    }
  }, 1000);


}

function setControlTimerDisplay(controlId, timerSeconds) {

  if(document.getElementById(`timer-${controlId}`) != null) {
    document.getElementById(`timer-${controlId}`).innerHTML = formatSeconds(timerSeconds);
  }

  function formatSeconds(totalSeconds) {
    var h = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    var m = Math.floor(totalSeconds / 60);
    var s = totalSeconds % 60;
    return h + ':' + ('0' + m).slice(-2) + ':' + ('0' + s).slice(-2);
  }
}
