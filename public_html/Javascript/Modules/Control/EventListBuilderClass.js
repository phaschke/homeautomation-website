export class EventListBuilder {

  constructor(oControlProperties) {
    this.iterator = 0;
    this.events = {};
    this.events.error = '';
    this.events.list = this.initalize(oControlProperties);
  }

  getIterator() {
    return this.iterator;
  }

  incrementIterator() {
    return this.iterator += 1;
  }

  getEvents() {
    return this.events;
  }

  getList() {
    return this.events.list;
  }

  setError(sMessage) {
    this.events.error = sMessage;
  }

  clearError() {
    this.events.error = "";
  }

  initalize(aEventProperties) {

    let aEventsParsed = [];

    if (aEventProperties) {
      for (let i = 0; i < aEventProperties.length; i++) {
        let oEvent = aEventProperties[i];
        oEvent.id = this.incrementIterator();

        aEventsParsed.push(oEvent);
      }
    }

    return aEventsParsed;
  }

  addEvent() {
    let iEventId = this.incrementIterator();
    let eventEntry = {
      id: iEventId,
      name: '',
      custom: true
    }

    this.getList().push(eventEntry);
  }

  updateEvent(iEventId, eElement) {

    let sValue = eElement.value;

    let iIndex = this.findIndexById(iEventId);
    if (iIndex >= 0) {
      this.getList()[iIndex]['name'] = sValue;
    }
  }

  findAndRemoveEvent(iEventId) {

    let iIndex = this.findIndexById(iEventId);

    this.getList().splice(iIndex, 1);
    this.clearError();
  }

  findIndexById(iEventId) {

    for (let i = 0; i < this.getList().length; i++) {
      if (iEventId == this.getList()[i].id) {
        return i;
      }
    }
    return -1;
  }

  validateList() {

    let errors = [];

    for (let i = 0; i < this.getList().length; i++) {

      let oEvent = this.getList()[i];

      errors.push(this.validateVariable("eventName", oEvent.name, oEvent.id));
    }

    return errors;
  }

  validateVariable(sFieldName, sFieldValue, iId) {

    let oResult = {
      fieldId: sFieldName + "-" + iId,
      fieldErrorId: sFieldName + "ErrorHolder-" + iId
    }
    let isError = false;
    if (sFieldValue == "") {
      isError = true;
    }
    oResult.isError = isError;
    return oResult;
  }

  getListForSave() {

    let aEventEntries = this.getList();

    let aEntries = [];

    for (let i = 0; i < this.getList().length; i++) {
      let oEvent = this.getList()[i];
      aEntries.push({
        name: oEvent.name,
        custom: oEvent.custom
      });
    }

    return aEntries;
  }

  toJSON() {
    return this.getEvents();
  }

}
