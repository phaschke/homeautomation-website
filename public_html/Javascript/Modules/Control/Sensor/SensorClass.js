export class Sensor {

  constructor(oControl) {
    this.oControl = oControl;
    this.setTemplate();
    this.initialize();
  }

  getControlData() {
    return this.oControl;
  }

  initialize() {

    let statusFormattingJSON = this.oControl.statusPropertiesJSON;
    let formattedStatusList = [];

    for (var i = 0; i < statusFormattingJSON.length; i++) {

      if (this.oControl.currentState[statusFormattingJSON[i].status]) { // value
        statusFormattingJSON[i].value = this.oControl.currentState[statusFormattingJSON[i].status];
        if (statusFormattingJSON[i].formatter != "") {
          statusFormattingJSON[i].formatter = statusFormattingJSON[i].formatter.replace("{{}}", statusFormattingJSON[i].value);
        } else {
          statusFormattingJSON[i].formatter = statusFormattingJSON[i].value;
        }
        formattedStatusList.push(statusFormattingJSON[i]);
      }
    }

    this.oControl.formattedStatusList = formattedStatusList;
    this.oControl.loaded = 1;
  }

  setTemplate() {
    this.oControl.template = "ControlSensor";
  }

  isLoaded() {
    return false;
  }

  needsRefresh() {
    return true;
  }

  toJSON() {
    return {};
  }

}
