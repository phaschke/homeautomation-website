export class SensorBuilderClass {

  constructor(oControlAddEditModel) {
    this.controlAddEditModel = oControlAddEditModel;
    this.statusListObject = new HomeAutomation.Control.Class.StatusListBuilderClass(oControlAddEditModel.getData().statusPropertiesJSON);
    this.eventListObject = new HomeAutomation.Control.Class.EventListBuilder(oControlAddEditModel.getData().eventPropertiesJSON);
  }

  getStatusListObject() {
    return this.statusListObject;
  }

  getEventListObject() {
    return this.eventListObject;
  }

  getEventListObject() {
    return this.eventListObject;
  }

  getControlTemplateName() {
    return 'CONTROL_ADD_EDIT_SENSOR';
  }

  validateControlSpecificFields() {

    let aResult = [];

    let aValidateStatusEntries = this.getStatusListObject().validateStatusEntries();
    let aValidateEventEntries = this.getEventListObject().validateList();

    aResult = aResult.concat(aValidateStatusEntries);
    aResult = aResult.concat(aValidateEventEntries);

    return aResult;
  }

  getProperties() {
    return undefined;
  }

  getEventProperties() {
    let oEventList = this.getEventListObject().getListForSave();
    return oEventList;
  }

  getStatusProperties() {
    let oStatusList = this.getStatusListObject().getStatusListForSave();
    return oStatusList;
  }

  toJSON() {

    let jsonArr = {};
    jsonArr.statusList = this.getStatusListObject().toJSON();
    jsonArr.eventList = this.getEventListObject().toJSON();

    return jsonArr;
  }

}
