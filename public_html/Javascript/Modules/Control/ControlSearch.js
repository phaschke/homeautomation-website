const sNamespace = 'HomeAutomation.Control.Search';

var _TOPICS = [];
var _CALLBACK;
var _CONTEXT;

export function openSearchModal(fCallback, oContext, oArgs) {

  if(oArgs) {
    fCallback = oArgs[1];
  }

  let oData = {
    filters: {}
  };

  let oAuthData = {
    'check': true,
    'callback': {
      namespace: sNamespace,
      func: 'openSearchModal',
      params: [fCallback]
    }
  };

  _CALLBACK = fCallback;
  _CONTEXT = oContext;

  HomeAutomation.Topic.Service.getAll(oAuthData).then(function(aTopics) {

    if (!HomeAutomation.Util.validateWebserviceCall(aTopics)) return;

    oData.filters.topics = aTopics;
    _TOPICS = aTopics;

    HomeAutomation.Util.executeHandlebars('modal-content', 'ControlSearch', oData, 'clear');

  });
}

export function onSearch() {

  let oAuthData = {
    check: true,
    callback: {
      namespace: sNamespace,
      func: 'onSearch',
      params: []
    }
  };

  let sTopicId = document.getElementById('controlSearchTopicSelect').value;

  let oData = {
    filters: {
      selectedTopicId: sTopicId,
      topics: _TOPICS
    }
  }

  HomeAutomation.Control.Service.getAllByTopic(oAuthData, sTopicId, false).then(function(aControls) {

    if (!HomeAutomation.Util.validateWebserviceCall(aControls)) return;
    oData.controls = aControls;

    HomeAutomation.Util.executeHandlebars('modal-content', 'ControlSearch', oData, 'clear');

  });


}

export function onControlSelect(iControlId) {

  let oAuthData = {
    check: true,
    callback: {
      namespace: sNamespace,
      func: 'onControlSelect',
      params: [iControlId]
    }
  };

  HomeAutomation.Control.Service.getOne(oAuthData, iControlId).then(function(oControl) {

    if(oControl instanceof HomeAutomation.Class.Message) {

      let sTopicId = document.getElementById('controlSearchTopicSelect').value;

      let oData = {
        filters: {
          selectedTopicId: sTopicId,
          topics: _TOPICS,
          error: 'Failed to retrieve control.'
        }
      };

    } else {

      let aArgs = [oControl];

      _CALLBACK.apply(_CONTEXT, aArgs);

      HomeAutomation.Util.closeModal();

    }

  });

}
