export function getOne(oAuthData, iControlId) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/controls/${iControlId}`, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Getting Control', xhrObj);
  });

}

export function getAll(oAuthData) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', '/api/controls', oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return parseResponseList(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Controls', xhrObj);
  });

}

export function getAllByTopic(oAuthData, iTopicId, bActive) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/controls/bytopic/${iTopicId}?active=${bActive}`, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return parseResponseList(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Controls', xhrObj);
  });

}

export function refreshControls(oAuthData, sControlIds) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/controls/status?controlids=${sControlIds}`, oAuthData, false);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return parseResponseList(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error refreshing controls', xhrObj);
  });

}

export function add(oAuthData, oControl) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oControl), 'json', '/api/controls', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Adding Control', xhrObj);
  });
}

export function update(oAuthData, oControl) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oControl), 'json', `/api/controls`, oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Updating Control', xhrObj);
  });
}

export function doAction(oAuthData, iControlId, oAction) {

  let request = new HomeAutomation.Class.WebserviceRequest('PATCH', JSON.stringify(oAction), 'json', `/api/controls/${iControlId}/action`, oAuthData, false);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Executing Control Action', xhrObj);
  });

}

export function updateState(oAuthData, iControlId, oState) {

  let request = new HomeAutomation.Class.WebserviceRequest('PATCH', JSON.stringify(oState), 'json', `/api/controls/${iControlId}/state`, oAuthData, false);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Updating State', xhrObj);
  });
}

export function getStatus(oAuthData, iControlId) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/controls/${iControlId}/status`, oAuthData, false);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Getting Control Status', xhrObj);
  });

}

export function updateDisplayOrder(oAuthData, aControls) {

  let request = new HomeAutomation.Class.WebserviceRequest('PATCH', JSON.stringify(aControls), 'json', '/api/controls/reorder', oAuthData, false);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return parseResponseList(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Reordering Controls', xhrObj);
  });
}

export function remove(oAuthData, iControlId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, 'json', `/api/controls/${iControlId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Deleting Event', xhrObj);
  });
}

function parseResponseList(oResponse) {

  if (oResponse._embedded && oResponse._embedded.controls) {
    return oResponse._embedded.controls;
  } else {
    return [];
  }
}
