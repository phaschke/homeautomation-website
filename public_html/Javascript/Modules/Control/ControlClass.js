export class Control {

  constructor(controlJSON) {

    if (controlJSON === undefined) {
      var controlJSON = {};
    }
    this.id = controlJSON.id === undefined ? "" : controlJSON.id;
    this.uniqueName = controlJSON.uniqueName === undefined ? "" : controlJSON.uniqueName;
    this.active = controlJSON.active === undefined ? "" : controlJSON.active;
    this.displayOrder = controlJSON.displayOrder === undefined ? "" : controlJSON.displayOrder;
    this.description = controlJSON.description === undefined ? "" : controlJSON.description;
    this.broker = controlJSON.broker === undefined ? "" : controlJSON.broker;
    this.subtopic = controlJSON.subtopic === undefined ? "" : controlJSON.subtopic;
    this.type = controlJSON.type === undefined ? "" : controlJSON.type;
    this.title = controlJSON.title === undefined ? "" : controlJSON.title;
    this.topic = controlJSON.topic === undefined ? "" : controlJSON.topic;
    this.persistState = controlJSON.persistState === undefined ? 0 : controlJSON.persistState;

    this.propertiesJSON = controlJSON.properties === undefined ? {} : this.parseJSON(controlJSON.properties);
    this.eventPropertiesJSON = controlJSON.eventProperties === undefined ? JSON.parse('[]') : this.parseJSON(controlJSON.eventProperties);
    this.statusPropertiesJSON = controlJSON.statusProperties === undefined ? JSON.parse('[]') : this.parseJSON(controlJSON.statusProperties);
    this.actionPropertiesJSON = controlJSON.actionProperties === undefined ? {} : this.parseJSON(controlJSON.actionProperties);

    this.statusJSON = controlJSON.status === undefined ? null : this.parseJSON(controlJSON.status);
    this.stateJSON = controlJSON.state === undefined ? null : this.parseJSON(controlJSON.state);

    this.currentState = {};

    if(this.statusJSON && (Object.keys(this.statusJSON).length > 0)) {
      this.currentState = this.statusJSON;
    } else {
      if(this.stateJSON && this.persistState === 1) {
        this.currentState = this.stateJSON;
      }
    }

    this.loaded = 0;
    this.editMode = controlJSON.editMode === undefined ? false : controlJSON.editMode;;

    this.controlObject = this.getControlObjectFromType(this.type);

    this.isError();

    //console.log(this);
  }

  getControlObject() {
    return this.controlObject;
  }

  parseJSON(input) {
    if (input == undefined) {
      return {};
    }
    return JSON.parse(input)
  }

  getId() {
    return this.id;
  }

  setActive(active) {
    this.isActive = active;
  }

  setDisplayOrder(iOrder) {
    this.displayOrder = iOrder;
  }

  setLoaded(isLoaded) {
    this.loaded = isLoaded;
  }

  isError() {
    if (this.currentState === -1) {
      this.isError = 1;
      return true;
    }
    if (this.currentState.error !== undefined) {
      this.isError = 1;
      return true;
    }
    this.isError = 0;
    return false;
  }

  isLoaded() {
    if (this.getControlObject().isLoaded()) {
      this.loaded = 1;
    } else {
      this.loaded = 0;
    }
  }

  needsRefresh() {

    if (this.getControlObject().needsRefresh()) {
      this.setLoaded(0);
      return true;
    } else {
      this.setLoaded(1);
      return false;
    }
  }

  getControlObjectFromType(sType) {

    var oControlObject = null;

    switch (sType) {

      case "SWITCH":
        oControlObject = new HomeAutomation.Control.Class.Switch(this);
        break;

      case "SENSOR":
        oControlObject = new HomeAutomation.Control.Class.Sensor(this);
        break;

      case "LED_CONTROLLER":
        oControlObject = new HomeAutomation.Control.Class.LEDController(this);
        break;

      default:
        throw "Unsupported control type";
    }

    return oControlObject;
  }

  toJSON() {
    let oBaseJSON = {
      id: this.id,
      uniqueName: this.uniqueName,
      active: this.active,
      displayOrder: this.displayOrder,
      description: this.description,
      broker: this.broker,
      subtopic: this.subtopic,
      type: this.type,
      title: this.title,
      topic: this.topic,
      persistState: this.persistState,
      currentState: this.currentState,
      loaded: this.loaded,
      isError: this.isError,
      formattedStatusList: this.formattedStatusList
    };

    let oJSONFromSpecificType = {};
    if (this.getControlObject() != null && this.getControlObject() != "") {
      oJSONFromSpecificType = this.getControlObject().toJSON();
    }
    let oJSON = Object.assign({}, oBaseJSON, oJSONFromSpecificType);

    return oJSON;
  }

  render() {

    //console.log(this.toJSON());
    HomeAutomation.Util.executeHandlebars("control-" + this.id, this.template, this.toJSON(), "clear");
  }
}
