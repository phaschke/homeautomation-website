const sNamespace = 'HomeAutomation.Control';

var _LOADED_CONTROLS = new Map();

export function addControlToLoadedControlList(oControl) {
  _LOADED_CONTROLS.set(oControl.getId(), oControl);
}

export function getLoadedControlsListControl(iId) {
  return _LOADED_CONTROLS.get(iId);
}

function getLoadedControlsListValues() {
  return _LOADED_CONTROLS.values();
}

function clearLoadedControlList() {
  _LOADED_CONTROLS.clear();
}

function getSortedLoadedControlList() {

  let aControlList = Array.from(_LOADED_CONTROLS.values());

  aControlList.sort((a, b) => {
    return a.displayOrder - b.displayOrder;
  });

  return aControlList;
}

export function displayList(aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: sNamespace,
      func: 'displayList',
      params: [iTopicId]
    }
  };

  if (aArgs[1]) {
    oAuthData.callback = aArgs[1];
  }

  let bGetOnlyActive = true;

  return new Promise((resolve, reject) => {

    HomeAutomation.Control.Service.getAllByTopic(oAuthData, iTopicId, bGetOnlyActive).then(function(aControls) {

      if (!HomeAutomation.Util.validateWebserviceCall(aControls, 'control-list-holder')) return reject();
      if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aControls, new HomeAutomation.Class.Message('No Controls Found.'), 'control-list-holder')) return resolve();
      clearLoadedControlList();

      parseControlsLoadedList(aControls);

      let aControlsToRender = getLoadedControlsToRender(getLoadedControlsListValues());
      let aControlsToRefresh = getControlsToRefresh(getLoadedControlsListValues());

      let oData = {};
      oData.controls = aControlsToRender;

      HomeAutomation.Util.executeHandlebars('control-list-holder', 'ControlList', oData, 'clear');

      refreshControls(aControlsToRefresh);

      return resolve();
    });

  });

  function parseControlsLoadedList(aControls) {

    for (let i = 0; i < aControls.length; i++) {
      var oControl = new HomeAutomation.Control.Class.Control(aControls[i]);
      //oControl.isError();
      addControlToLoadedControlList(oControl);
    }
  }

  function getLoadedControlsToRender(aControls) {

    let aControlsToRender = [];

    for (let oControl of aControls) {

      oControl.isLoaded();
      aControlsToRender.push(oControl.toJSON());
    }

    return aControlsToRender;
  }

  function getControlsToRefresh(aControls) {

    let aControlsToRefresh = [];

    for (let oControl of aControls) {
      if (oControl.getControlObject().needsRefresh()) {
        aControlsToRefresh.push(oControl.getId());
      }
    }

    return aControlsToRefresh;
  }

}

export function displayListForEdit(iTopicId, aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
  }

  var oAuthData = {
    'check': true,
    'callback': {
      namespace: sNamespace,
      func: 'displayListForEdit',
      params: [iTopicId]
    }
  };

  let bGetOnlyActive = false;

  HomeAutomation.Control.Service.getAllByTopic(oAuthData, iTopicId, bGetOnlyActive).then(function(aControls) {

    if (!HomeAutomation.Util.validateWebserviceCall(aControls, 'control-list-holder')) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aControls, new HomeAutomation.Class.Message('No Controls Found.'), 'control-list-holder')) return;

    clearLoadedControlList();
    parseControlsLoadedListForEdit(aControls);

    let aControlsToRender = getControlsToRenderEdit(getLoadedControlsListValues());

    let oData = {};
    oData.controls = aControlsToRender;
    oData.listOrderToggle = false;

    HomeAutomation.Util.executeHandlebars('control-list-holder', 'ControlListEdit', oData, 'clear');

  });
}

function parseControlsLoadedListForEdit(controls) {

  clearLoadedControlList();

  for (let i = 0; i < controls.length; i++) {
    var control = controls[i];
    control.editMode = true;
    var control = new HomeAutomation.Control.Class.Control(control);
    addControlToLoadedControlList(control);
  }
}

function getControlsToRenderEdit(aControls) {

  let aControlsToRender = [];

  for (let oControl of aControls) {
    var oControlJSON = oControl.toJSON();
    oControlJSON.editTemplate = `${oControlJSON.type}_EDIT`;
    aControlsToRender.push(oControlJSON);
  }

  return aControlsToRender;
}

function refreshControls(aControlsToRefresh, aArgs) {

  if (aArgs) {
    var aControlsToRefresh = aArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: sNamespace,
      func: 'refreshControls',
      params: [aControlsToRefresh]
    }
  };

  if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aControlsToRefresh)) return;

  aControlsToRefresh.forEach((iControlId, i) => {

    HomeAutomation.Control.Service.getStatus(oAuthData, iControlId).then(function(oResponseControl) {

      if (!HomeAutomation.Util.validateWebserviceCall(oResponseControl)) return;

      let oControl = new HomeAutomation.Control.Class.Control(oResponseControl);
      oControl.render();

    });

  });

  function updateFrontendControls(aControls) {

    for (var i = 0; i < aControls.length; i++) {

      let oControl = new HomeAutomation.Control.Class.Control(aControls[i]);
      oControl.render();
    }
  }

}

let _CONTROL_MODEL = {};

function clearControlModel() {
  _CONTROL_MODEL = new HomeAutomation.Control.Class.ControlAddEditModel();
}

export function getControlModel() {
  return _CONTROL_MODEL;
}

export function getControlModelTypeObject() {
  return getControlModel().getControlObject();
}

export function openAddEditModal(iTopicId, iControlId, aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
    var iControlId = aArgs[1];
  }

  var oAuthData = {
    'check': true,
    'callback': {
      namespace: sNamespace,
      func: 'openEditModal',
      params: [iTopicId, iControlId]
    }
  };

  HomeAutomation.Util.displayLoadingModal();
  clearControlModel();

  HomeAutomation.Topic.Service.getAll(oAuthData).then(function(aTopics) {

    if (!HomeAutomation.Util.validateWebserviceCall(aTopics)) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aTopics, new HomeAutomation.Class.Message('No Topics Found', 'No topics found, add valid topics first.'))) return;

    if (iControlId == undefined) {
      displayAddEditModal(iTopicId, aTopics);
    } else {
      displayEditModal(iTopicId, iControlId, aTopics);
    }

  });

  function displayAddEditModal(iTopicId, aTopics) {

    let oControlModel = getControlModel();

    let oControlView = new HomeAutomation.Control.Class.AddEditControlViewProperties();
    oControlView.setTopics(aTopics);
    oControlView.setModalType('add');

    oControlModel.setData(new HomeAutomation.Control.Class.ControlBuilder());
    oControlModel.setView(oControlView);
    oControlModel.setLoadedTopicId(iTopicId);
    // Set intial topic id in data model
    if (validateTopicId(iTopicId, aTopics)) {
      oControlModel.getData().setTopicId(iTopicId);
    } else {
      throw 'Invalid Topic Id';
    }

    oControlModel.render();
  }

  function displayEditModal(iTopicId, iControlId, aTopics) {

    let oControl = HomeAutomation.Control.getLoadedControlsListControl(parseInt(iControlId));

    if (!oControl) {
      displayControlLoadingErrorMessage();
      return;
    }

    let oControlModel = getControlModel();

    let oControlView = new HomeAutomation.Control.Class.AddEditControlViewProperties();
    oControlView.setTopics(aTopics);
    oControlView.setModalType('edit');

    oControlModel.setData(new HomeAutomation.Control.Class.ControlBuilder(oControl));
    oControlModel.setView(oControlView);
    oControlModel.setLoadedTopicId(iTopicId);
    oControlModel.setControlObject();
    oControlModel.getControlTemplateName();
    oControlModel.render();

  }

  function displayControlLoadingErrorMessage() {
    let message = new HomeAutomation.Class.Message('Error Loading Control', 'Control was not preloaded. Refresh page.');
    message.setType('ERR');
    message.displayModal();
  }

  function validateTopicId(iTopicId, aTopics) {

    for (let i = 0; i < aTopics.length; i++) {
      if (iTopicId == aTopics[i].id) return true;
    }
    return false;
  }
}

export function onAddOrUpdate(iLoadedTopicId, iControlId, aArgs) {

  if (aArgs) {
    var iLoadedTopicId = aArgs[0];
    var iControlId = aArgs[1];
  }

  var oAuthData = {
    'check': true,
    'callback': {
      namespace: sNamespace,
      func: 'addOrUpdate',
      params: [iLoadedTopicId, iControlId]
    }
  };

  let oControlModel = getControlModel();
  let isError = validateAndGenerateFieldErrorUI(oControlModel);

  if (isError) return;

  if (iControlId == '' || iControlId == undefined) {

    HomeAutomation.Control.Service.add(oAuthData, oControlModel.getFields()).then(function(oResponse) {
      onAddOrUpdateResponse(oResponse, iLoadedTopicId, oControlModel.getFields());
    });

  } else {

    HomeAutomation.Control.Service.update(oAuthData, oControlModel.getFields()).then(function(oResponse) {
      onAddOrUpdateResponse(oResponse, iLoadedTopicId, oControlModel.getFields());
    });
  }

  function onAddOrUpdateResponse(oResponse, iLoadedTopicId, oControl) {

    if (!HomeAutomation.Util.validateWebserviceCall(oResponse, 'formErrorHolder')) return;

    // Case that topic id of control changes during add/edit
    // If edited topic id is not the same as the loaded page, redirect
    // otherwise update list.
    if (iLoadedTopicId == oResponse.topicId) {
      displayListForEdit(iLoadedTopicId);
    } else {
      HomeAutomation.Util.redirect('/topic/controls/' + oControl.topicId);
    }

    HomeAutomation.Util.closeModal();

  }


  function validateAndGenerateFieldErrorUI(oControlModel) {

    let aErroredFields = oControlModel.validateFields();

    let isError = false;

    if (aErroredFields.length > 0) {
      for (let i = 0; i < aErroredFields.length; i++) {
        let oFieldStatus = aErroredFields[i];
        if (oFieldStatus.isError) {
          HomeAutomation.Util.addFieldError(oFieldStatus.fieldId, oFieldStatus.fieldErrorId, oFieldStatus.fieldError);
          if (!isError) {
            isError = true;
          }
        } else {
          HomeAutomation.Util.removeFieldError(oFieldStatus.fieldId, oFieldStatus.fieldErrorId);
        }
      }
    }
    return isError;
  }

}

export function onDeleteConfirmation(iTopicId, iControlId) {

  var oModalData = {
    header: 'Confirm Deletion',
    message: 'Are you sure you want to delete the control?',
    callback: 'HomeAutomation.Control.onDelete(' + iTopicId + ',' + iControlId + ')'
  };
  HomeAutomation.Util.executeHandlebars('modal-content', 'DeleteConfirmation', oModalData, 'clear');
}

export function onDelete(iTopicId, iControlId, aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
    var iControlId = aArgs[1];
  }

  var oAuthData = {
    'check': true,
    'callback': {
      namespace: sNamespace,
      func: 'delete',
      params: [iTopicId, iControlId]
    }
  };

  HomeAutomation.Control.Service.remove(oAuthData, iControlId).then(function(oResponse) {

    if (!HomeAutomation.Util.validateWebserviceCall(oResponse)) return;

    HomeAutomation.Control.displayListForEdit(iTopicId);
    HomeAutomation.Util.closeModal();
  });

}

export function onReorderListButtonActivate() {

  let oData = {};
  oData.controls = getControlsToRenderEdit(getSortedLoadedControlList());
  oData.listOrderToggle = true;

  HomeAutomation.Util.executeHandlebars('control-list-holder', 'ControlListEdit', oData, 'clear');


  let eSortableTopicList = document.getElementById('sortableControlList');

  Sortable.create(eSortableTopicList, {
    animation: 150,
    ghostClass: 'ghostClass'
  });
}

export function getListOrderThenSave() {

  let eControlList = document.getElementById('sortableControlList');
  let aControlListChildren = eControlList.children;

  let iCounter = 0;

  Array.from(aControlListChildren).forEach(li => {

    let iControlId = li.id.split('-')[1];

    let oControl = getLoadedControlsListControl(parseInt(iControlId, 10));
    oControl.setDisplayOrder(iCounter++);
    addControlToLoadedControlList(oControl);
  });

  onReorderedSave();
}

function onReorderedSave() {

  let oAuthData = {
    'check': true,
    'callback': {
      namespace: sNamespace,
      func: 'onReorderedSave',
      params: []
    }
  };

  let aControls = getControlsToRenderEdit(getSortedLoadedControlList());

  HomeAutomation.Control.Service.updateDisplayOrder(oAuthData, aControls).then(function(aControls) {

    if (!HomeAutomation.Util.validateWebserviceCall(aControls)) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aControls, new HomeAutomation.Class.Message('No Controls Found.'))) return;

    clearLoadedControlList();
    parseControlsLoadedListForEdit(aControls);

    let aControlsToRender = getControlsToRenderEdit(getLoadedControlsListValues());

    let oData = {};
    oData.controls = aControlsToRender;
    oData.listOrderToggle = false;

    HomeAutomation.Util.executeHandlebars('control-list-holder', 'ControlListEdit', oData, 'clear');

  });

}
