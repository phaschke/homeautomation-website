export class StatusListBuilderClass {

  constructor(oInitialControlProperties) {
    this.statusListIterator = 0;
    this.statusList = {};
    this.statusList.entries = this.initalizeStatusDisplayList(oInitialControlProperties);
  }

  getStatusListIterator() {
    return this.statusListIterator;
  }

  incrementStatusListIterator() {
    return this.statusListIterator += 1;
  }

  getStatusListEntries() {
    return this.statusList.entries;
  }

  getStatusListEntriesCount() {
    return this.getStatusListEntries().length;
  }

  validateStatusListLength() {
    if (this.getStatusListEntriesCount() >= 5) {
      return false;
    }
    return true;
  }

  initalizeStatusDisplayList(aStatusProperties) {
    
    var aEntries = [];

      if (aStatusProperties) {
      for (let i = 0; i < aStatusProperties.length; i++) {
        aStatusProperties[i].id = this.incrementStatusListIterator();
      }
    }

    return aStatusProperties;
  }

  addStatusEntry() {

    if (!this.validateStatusListLength()) {
      this.setStatusListError("Max of 5 custom statuses are allowed.");
      return false;
    }
    this.clearStatusListError();

    var iStatusId = this.incrementStatusListIterator();
    let statusEntry = {
      "id": iStatusId,
      "title": "",
      "status": "",
      "formatter": "",
      "display": false,
      "editable": true
    }

    this.getStatusListEntries().push(statusEntry);
  }

  getStatusFromId(iStatusId) {
    return this.getStatusListEntries()[iStatusId];
  }

  updateStatusListEntry(sField, iStatusId, oFieldElement) {

    if (sField == "display") {
      var sValue = oFieldElement.checked;

    } else {
      var sValue = oFieldElement.value;
    }

    let iIndex = this.findIndexById(iStatusId);

    if (iIndex >= 0) {
      this.getStatusListEntries()[iIndex][sField] = sValue;
    }
  }

  findAndRemoveStatusEntry(iStatusId) {
    let iIndex = this.findIndexById(iStatusId);
    if (iIndex >= 0) {
      this.removeStatus(iIndex);
    }
  }

  findIndexById(iStatusId) {

    for (let i = 0; i < this.getStatusListEntriesCount(); i++) {
      if (iStatusId == this.getStatusFromId(i).id) {
        return i;
      }
    }
    return -1;
  }

  removeStatus(iIndex) {
    this.getStatusListEntries().splice(iIndex, 1);
    this.clearStatusListError();
  }

  setStatusListError(sMessage) {
    this.statusList.error = sMessage;
  }

  clearStatusListError() {
    this.statusList.error = "";
  }

  validateStatusEntries() {

    let statusErrors = [];

    for (let i = 0; i < this.getStatusListEntriesCount(); i++) {

      let oStatus = this.getStatusListEntries()[i];

      statusErrors.push(validateStatusVariable("statusTitle", oStatus.title, oStatus.id));
      statusErrors.push(validateStatusVariable("status", oStatus.status, oStatus.id));
    }

    return statusErrors;

    function validateStatusVariable(sFieldName, sFieldValue, iId) {

      let oResult = {
        fieldId: sFieldName + "-" + iId,
        fieldErrorId: sFieldName + "ErrorHolder-" + iId
      }
      let isError = false;
      if (sFieldValue == "") {
        isError = true;
      }
      oResult.isError = isError;
      return oResult;
    }
  }

  getStatusListForSave() {

    let aStatusEntries = this.getStatusListEntries();

    let aEntries = [];

    for (let i = 0; i < this.getStatusListEntriesCount(); i++) {
      let oStatus = this.getStatusListEntries()[i];
      aEntries.push({
        "title": oStatus.title,
        "status": oStatus.status,
        "formatter": oStatus.formatter,
        "display": oStatus.display,
        "editable": oStatus.editable
      });
    }

    //let oReturnObject = {
    //  "status_display": aEntries
    //}

    return aEntries;
  }

  toJSON() {
    //var jsonArr = {};
    //jsonArr.statusList = this.statusList;
    return this.statusList;
  }
}
