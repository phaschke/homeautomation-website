export class LEDController {

  constructor(oControl) {

    this.oControl = oControl;
    this.displaySettings = {};
    this.pickerValues = {};
    this.activePicker = null;
    this.activeTimer = false;
    this.setTemplate();

    this.initialize();
  }

  getControlData() {
    return this.oControl;
  }

  getPickerValues() {
    return this.pickerValues;
  }

  getActivePicker() {
    return this.activePicker;
  }

  clearPickerValues() {
    this.pickerValues = {};
  }

  getActiveTimer() {
    return this.activeTimer;
  }

  setActiveTimer(bActiveFlag) {
    this.activeTimer = bActiveFlag;
  }

  initialize() {

    let oLedViewOptions = {};
    let oLedState = {};

    let oControlData = this.getControlData();

    if (oControlData.propertiesJSON) {
      if (oControlData.propertiesJSON.led) {
        oLedViewOptions = oControlData.propertiesJSON.led;
      }
    }

    if (oControlData.currentState) {
      oLedState = oControlData.currentState;
    }
    this.displaySettings = this.parseDisplaySettings(oLedViewOptions, oLedState);

    this.oControl.loaded = 1;
  }

  setTemplate() {
    this.oControl.template = 'ControlLED';
  }

  isLoaded() {
    if (!this.hasActiveTimer()) {
      return true;
    } else {
      return false;
    }
  }

  needsRefresh() {
    return this.hasActiveTimer();
  }

  hasActiveTimer() {
    return this.getActiveTimer();
  }

  toggleDevice(iControlId, iTimerDuration, aParams) {

    let sMode = aParams[0];
    if (sMode == '') return false;

    HomeAutomation.Control.LEDController.toggleSwitchOn(iControlId, sMode, iTimerDuration);
  }

  parseDisplaySettings(oLedViewOptions, oLedState) {

    let displaySettings = {};
    this.setActiveTimer(false);
    displaySettings.modeList = [];

    if (oLedViewOptions.rgb) {
      if (oLedViewOptions.rgb.enabled) {
        let rgbMode = {
          mode: 'rgb',
          title: 'RGB'
        };
        if (oLedState.rgb != undefined && oLedState.rgb != 0) {
          rgbMode.rgb = oLedState.rgb;
          rgbMode.on = true;
        } else {
          rgbMode.rgb = 0;
          rgbMode.on = false;
        }
        if (oLedState.rgb_r != undefined) {
          rgbMode.r = parseInt(oLedState.rgb_r);
        } else {
          rgbMode.r = 255;
        }
        if (oLedState.rgb_g != undefined) {
          rgbMode.g = parseInt(oLedState.rgb_g);
        } else {
          rgbMode.g = 0;
        }
        if (oLedState.rgb_b != undefined) {
          rgbMode.b = parseInt(oLedState.rgb_b);
        } else {
          rgbMode.b = 0;
        }

        // Set or remove active timer
        if (oLedState.rgb_timer != undefined && oLedState.rgb_timer != 0) {
          this.setLEDTimer('rgb', oLedState.rgb_timer);
        } else {
          this.setLEDTimer('rgb', 0);
        }

        if (oLedViewOptions.rgb.brightness) {
          if (oLedState.rgb_bright != undefined) {
            rgbMode.rgb_bright = parseInt(oLedState.rgb_bright);
          } else {
            rgbMode.rgb_bright = 100;
          }
          rgbMode.rgb_bright_enabled = oLedViewOptions.rgb.brightness;
        } else {
          rgbMode.rgb_bright_enabled = false;
        }

        displaySettings.modeList.push(rgbMode);
      }
    }
    if (oLedViewOptions.tunable) {
      if (oLedViewOptions.tunable.enabled) {
        let tunableMode = {
          mode: 'tunable',
          title: 'Tunable'
        };
        if (oLedState.tunable != undefined && oLedState.tunable != 0) {
          tunableMode.tunable = oLedState.tunable;
          tunableMode.on = true;
        } else {
          tunableMode.tunable = 0;
          tunableMode.on = false;
        }
        if (oLedViewOptions.tunable.minTemp != undefined) {
          tunableMode.minTemp = parseInt(oLedViewOptions.tunable.minTemp);
        }
        if (oLedViewOptions.tunable.maxTemp != undefined) {
          tunableMode.maxTemp = parseInt(oLedViewOptions.tunable.maxTemp);
        }
        if (oLedState.tunable_kelvin != undefined) {
          tunableMode.kelvin = parseInt(oLedState.tunable_kelvin);
        } else {
          tunableMode.kelvin = parseInt(oLedViewOptions.tunable.minTemp);
        }

        // Set or remove active timer
        if (oLedState.tunable_timer != undefined && oLedState.tunable_timer != 0) {
          this.setLEDTimer('tunable', oLedState.tunable_timer);
        } else {
          this.setLEDTimer('tunable', 0);
        }

        if (oLedViewOptions.tunable.brightness) {
          if (oLedState.tunable_bright != undefined) {
            tunableMode.tunable_bright = parseInt(oLedState.tunable_bright);
          } else {
            tunableMode.tunable_bright = 100;
          }
          tunableMode.tunable_bright_enabled = oLedViewOptions.tunable.brightness;
        } else {
          tunableMode.tunable_bright_enabled = false;
        }
        displaySettings.modeList.push(tunableMode);
      }
    }
    if (oLedViewOptions.ww) {
      if (oLedViewOptions.ww.enabled) {
        let wwMode = {
          mode: 'ww',
          title: 'WW'
        };
        if (oLedState.ww != undefined && oLedState.ww != 0) {
          wwMode.ww = oLedState.ww;
          wwMode.on = true;
        } else {
          wwMode.ww = 0;
          wwMode.on = false;
        }

        // Set or remove active timer
        if (oLedState.ww_timer != undefined && oLedState.ww_timer != 0) {
          this.setLEDTimer('ww', oLedState.ww_timer);
        } else {
          this.setLEDTimer('ww', 0);
        }

        if (oLedViewOptions.ww.brightness) {
          if (oLedState.ww_bright != undefined) {
            wwMode.ww_bright = parseInt(oLedState.ww_bright);
          } else {
            wwMode.ww_bright = 100;
          }
          wwMode.ww_bright_enabled = oLedViewOptions.ww.brightness;
        } else {
          wwMode.ww_bright_enabled = false;
        }
        displaySettings.modeList.push(wwMode);
      }
    }
    if (oLedViewOptions.cw) {
      if (oLedViewOptions.cw.enabled) {
        let cwMode = {
          mode: 'cw',
          title: 'CW'
        };
        if (oLedState.cw != undefined && oLedState.cw != 0) {
          cwMode.cw = oLedState.cw;
          cwMode.on = true;
        } else {
          cwMode.cw = 0;
          cwMode.on = false;
        }

        // Set or remove active timer
        if (oLedState.cw_timer != undefined && oLedState.cw_timer != 0) {
          this.setLEDTimer('cw', oLedState.cw_timer);
        } else {
          this.setLEDTimer('cw', 0);
        }

        if (oLedViewOptions.ww.brightness) {
          if (oLedState.cw_bright != undefined) {
            cwMode.cw_bright = parseInt(oLedState.cw_bright);
          } else {
            cwMode.cw_bright = 100;
          }
          cwMode.cw_bright_enabled = oLedViewOptions.cw.brightness;
        } else {
          cwMode.cw_bright_enabled = false;
        }
        displaySettings.modeList.push(cwMode);
      }
    }

    return displaySettings;

  }

  setLEDTimer(sMode, iTimer) {

    // Do not start timers if flag is set to false
    if (this.oControl.editMode) {
      return;
    }

    let sTimerId = `${this.getControlData().getId()}-${sMode}`;

    if (iTimer > 0) {
      // set timer here
      HomeAutomation.Control.Timer.addTimerAndStartInterval(sTimerId, iTimer);
      this.setActiveTimer(true);

    } else {
      // remove switch from timer list if timer is not defined
      HomeAutomation.Control.Timer.removeTimer(sTimerId);
    }

  }

  toJSON() {
    return {
      'led': this.displaySettings
    };
  }

  openLEDSetModal(sMode) {

    this.clearPickerValues();
    this.activePicker = sMode;

    let oDisplaySettings = this.displaySettings;
    let iControlId = this.getControlData().id;

    for (let i = 0; i < oDisplaySettings.modeList.length; i++) {

      let oModeEntry = oDisplaySettings.modeList[i];

      if (oModeEntry.mode == sMode) {
        oModeEntry.controlId = iControlId;
        HomeAutomation.Util.executeHandlebars('modal-content', 'ControlLEDSettings', oModeEntry, 'clear');
        this.generatePickers(oModeEntry);
      }

    }
  }

  generatePickers(oModeEntry) {

    let context = this;

    switch (oModeEntry.mode) {

      case 'rgb':

        let rgb = {
          r: oModeEntry.r,
          g: oModeEntry.g,
          b: oModeEntry.b
        };

        var pickerColor = new iro.ColorPicker('#picker-color', {
          layout: [{
            component: iro.ui.Wheel,
            options: {}
          }, ]
        });

        pickerColor.color.rgb = rgb;
        this.getPickerValues().rgb_r = rgb.r;
        this.getPickerValues().rgb_g = rgb.g;
        this.getPickerValues().rgb_b = rgb.b;

        pickerColor.on('color:change', function(color) {
          context.pickerColorChangeCallback(color, context);
        });
        document.getElementById('picker-color-value').innerHTML = pickerColor.color.rgbString;

        if (oModeEntry.rgb_bright_enabled) {

          var rgbBightnessValue = oModeEntry.rgb_bright;

          var pickerColorBrightness = new iro.ColorPicker('#picker-color-brightness', {
            layout: [{
              component: iro.ui.Slider,
              options: {
                sliderType: 'value',
              }
            }, ]
          });
          // Set intial brightness value
          pickerColorBrightness.color.value = rgbBightnessValue;
          this.getPickerValues().rgb_bright = rgbBightnessValue;
          let sPickerBrightnessValue = 'picker-brightness-value';
          document.getElementById(sPickerBrightnessValue).innerHTML = rgbBightnessValue + '%';
          pickerColorBrightness.on('color:change', function(color) {
            context.pickerBrightnessChangeCallback(color, 'rgb_bright', sPickerBrightnessValue, context);
          });
        }

        break;

      case 'tunable':

        let rMin = 2200;
        let rMax = 11000;
        let tMin = parseInt(oModeEntry.minTemp, 10);
        let tMax = parseInt(oModeEntry.maxTemp, 10);

        let tempValue = parseInt(oModeEntry.kelvin, 10);

        var pickerTemperature = new iro.ColorPicker('#picker-temperature', {
          layout: [{
            component: iro.ui.Slider,
            options: {
              sliderType: 'kelvin', // can also be 'saturation', 'value', 'alpha' or 'kelvin'
              minTemperature: 2200,
              maxTemperature: 11000,
            }
          }, ]
        });

        // Set inital temperature
        pickerTemperature.color.kelvin = this.scaleNumberUp(tempValue, rMin, rMax, tMin, tMax);
        this.getPickerValues().tunable_kelvin = tempValue;

        //document.getElementById('picker-temperature-value').innerHTML = this.scaleNumber(tempValue, rMin, rMax, tMin, tMax) + 'K';
        document.getElementById('picker-temperature-value').innerHTML = tempValue + 'K';
        pickerTemperature.on('color:change', function(color) {
          context.pickerTemperatureChangeCallback(color, tMin, tMax, context);
        });

        if (oModeEntry.tunable_bright_enabled) {

          var iTunableBightnessValue = oModeEntry.tunable_bright;
          var pickerTunableBrightness = new iro.ColorPicker('#picker-tunable-brightness', {
            layout: [{
              component: iro.ui.Slider,
              options: {
                sliderType: 'value',
              }
            }, ]
          });
          // Set intial brightness value
          pickerTunableBrightness.color.value = iTunableBightnessValue;
          this.getPickerValues().tunable_bright = iTunableBightnessValue;
          let sPickerBrightnessValue = 'picker-tunable-brightness-value';
          document.getElementById(sPickerBrightnessValue).innerHTML = iTunableBightnessValue + '%';
          pickerTunableBrightness.on('color:change', function(color) {
            context.pickerBrightnessChangeCallback(color, 'tunable_bright', sPickerBrightnessValue, context);
          });
        }

        break;

      case 'ww':

        if (oModeEntry.ww_bright_enabled) {

          var iWWBightnessValue = oModeEntry.ww_bright;

          var pickerWWBrightness = new iro.ColorPicker('#picker-ww-brightness', {
            layout: [{
              component: iro.ui.Slider,
              options: {
                sliderType: 'value',
              }
            }, ]
          });
          // Set intial brightness value
          pickerWWBrightness.color.value = iWWBightnessValue;
          this.getPickerValues().ww_bright = iWWBightnessValue;
          let sPickerBrightnessValue = 'picker-ww-brightness-value';
          document.getElementById(sPickerBrightnessValue).innerHTML = iWWBightnessValue + '%';
          pickerWWBrightness.on('color:change', function(color) {
            context.pickerBrightnessChangeCallback(color, 'ww_bright', sPickerBrightnessValue, context);
          });

        }

        break;

      case 'cw':

        if (oModeEntry.cw_bright_enabled) {

          var iCWBightnessValue = oModeEntry.cw_bright;

          var pickerCWBrightness = new iro.ColorPicker('#picker-cw-brightness', {
            layout: [{
              component: iro.ui.Slider,
              options: {
                sliderType: 'value',
              }
            }, ]
          });
          // Set intial brightness value
          pickerCWBrightness.color.value = iCWBightnessValue;
          this.getPickerValues().cw_bright = iCWBightnessValue;
          let sPickerBrightnessValue = 'picker-cw-brightness-value';
          document.getElementById(sPickerBrightnessValue).innerHTML = iCWBightnessValue + '%';
          pickerCWBrightness.on('color:change', function(color) {
            context.pickerBrightnessChangeCallback(color, 'cw_bright', sPickerBrightnessValue, context);
          });

        }
        break;

    }
  }

  updateControlSettings() {

    let iControlId = this.getControlData().id;

    var oAuthData = {
      check: true,
      callback: {
        namespace: 'HomeAutomation.Control.LEDController',
        func: 'updateControlSettings',
        params: [iControlId]
      }
    };

    var that = this;

    HomeAutomation.Control.Service.updateState(oAuthData, iControlId, this.getPickerValues()).then(function(oReturnedControl) {

      if (!HomeAutomation.Util.validateWebserviceCall(oReturnedControl)) return;

      let oControl = new HomeAutomation.Control.Class.Control(oReturnedControl);
      HomeAutomation.Control.addControlToLoadedControlList(oControl);

      HomeAutomation.Util.closeModal();
      that.clearPickerValues();

      let sMode = that.getActivePicker();

      if (oControl.currentState[sMode] === 1) {
        HomeAutomation.Control.LEDController.toggleSwitchOn(iControlId, sMode);
      }
    });
  }

  pickerColorChangeCallback(oColor, oContext) {
    document.getElementById('picker-color-value').innerHTML = oColor.rgbString;
    oContext.getPickerValues().rgb_r = oColor.rgb.r;
    oContext.getPickerValues().rgb_g = oColor.rgb.g;
    oContext.getPickerValues().rgb_b = oColor.rgb.b;
  }

  pickerBrightnessChangeCallback(oColor, sModeBrightness, sModeId, oContext) {
    document.getElementById(sModeId).innerHTML = oColor.value + '%';
    oContext.getPickerValues()[sModeBrightness] = oColor.value;
  }

  pickerTemperatureChangeCallback(oColor, tMin, tMax, oContext) {

    let rMin = 2200;
    let rMax = 11000;

    let scaled = oContext.scaleNumber(oColor.kelvin, rMin, rMax, tMin, tMax);
    document.getElementById('picker-temperature-value').innerHTML = scaled + 'K';
    oContext.getPickerValues().tunable_kelvin = scaled;
  }

  scaleNumberUp(input, rMin, rMax, tMin, tMax) {

    return Math.floor((input - tMin) / (tMax - tMin) * (rMax - rMin) + rMin);
  }

  scaleNumber(input, rMin, rMax, tMin, tMax) {

    if (input < rMin) {
      input = rMin;
    }
    if (input > rMax - 10) {
      input = rMax;
    }

    return Math.floor((input - rMin) / (rMax - rMin) * (tMax - tMin) + tMin);
  }

}
