const namespace = 'HomeAutomation.Control.LEDController';

// Used in callback case from class method
export function updateControlSettings(iControlId, args) {

  if (args) {
    var iControlId = args[0];
  }

  var authData = {
    'check': true,
    'callback': {
      namespace: 'HomeAutomation.Control.LEDController',
      func: 'updateControlSettings',
      params: [iControlId]
    }
  };

  let oControl = HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  if (oControl) {
    oControl.updateControlSettings();
  }
}

export function toggleSwitchOn(iControlId, sMode, iDuration, oArgs) {

  if (oArgs) {
    var iControlId = args[0]
    var sMode = args[1];
    var iDuration = args[2];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'toggleSwitchOn',
      params: [iControlId, sMode, iDuration]
    }
  };

  let oAction = {
    action: 'TOGGLE_ON',
    parameters: [{
      parameterName: 'mode',
      parameter: sMode
    }]
  }

  if (iDuration > 0) {
    oAction.parameters.push({
      parameterName: 'duration',
      parameter: iDuration
    });
  }

  let oControl = new HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  oControl.loaded = false;
  oControl.render();

  HomeAutomation.Control.Service.doAction(oAuthData, iControlId, oAction).then(function(oReturnedControl) {

    if (!HomeAutomation.Util.validateWebserviceCall(oReturnedControl)) return;

    oControl = new HomeAutomation.Control.Class.Control(oReturnedControl);
    oControl.render();
  });

}

export function toggleSwitchOff(iControlId, sMode, oArgs) {

  if (oArgs) {
    var iControlId = args[0]
    var sMode = args[1];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'toggleSwitchOff',
      params: [iControlId, sMode]
    }
  };

  let oAction = {
    action: 'TOGGLE_OFF',
    parameters: [{
      parameterName: 'mode',
      parameter: sMode
    }]
  }

  let oControl = new HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  oControl.loaded = false;
  oControl.render();

  HomeAutomation.Control.Service.doAction(oAuthData, iControlId, oAction).then(function(oReturnedControl) {

    if (!HomeAutomation.Util.validateWebserviceCall(oReturnedControl)) return;

    HomeAutomation.Control.Timer.removeTimer(iControlId);

    oControl = new HomeAutomation.Control.Class.Control(oReturnedControl);
    oControl.render();
  });

}
