export class LEDControlBuilderClass {

  constructor(oControlAddEditModel) {
    this.controlAddEditModel = oControlAddEditModel;
    this.ledControllerObject = new LEDControllerCreate(oControlAddEditModel);
    this.switchObject = new SwitchCreate(oControlAddEditModel);
  }

  getLEDControllerObject() {
    return this.ledControllerObject;
  }

  getSwitchObject() {
    return this.switchObject;
  }

  getControlTemplateName() {
    return 'CONTROL_ADD_EDIT_LED_CONTROLLER';
  }

  validateControlSpecificFields() {
    // TODO
    //return this.getStatusListObject().validateLEDOptions();
    return [];
  }

  getProperties() {
    return this.getLEDControllerObject().toJSON();
  }

  getEventProperties() {
    return undefined;
  }

  getStatusProperties() {
    return undefined;
  }

  toJSON() {
    let oLEDController = this.getLEDControllerObject().toJSON();
    return oLEDController;
  }

}

class SwitchCreate {

  constructor(oInitialControlProperties) {
    this.controlData = oInitialControlProperties.data;
  }

  getControlData() {
    return this.controlData;
  }

  onControlPersistSelectChange(oElement) {

    if (oElement.checked) {
      this.getControlData().persistState = 1;
    } else {
      this.getControlData().persistState = 0;
    }
  }
}

class LEDControllerCreate {

  constructor(oInitialControlProperties) {
    this.controlData = oInitialControlProperties.data;
    this.ledOptions = {
      led: {
        rgb: {
          enabled: false,
          brightness: false
        },
        tunable: {
          enabled: false,
          brightness: false,
          minTemp: 0,
          maxTemp: 0
        },
        ww: {
          enabled: false,
          brightness: false
        },
        cw: {
          enabled: false,
          brightness: false
        }
      }
    };
    this.initalizeLEDOptions(this.controlData.propertiesJSON);
  }

  getLEDOptions() {
    return this.ledOptions.led;
  }

  initalizeLEDOptions(oPropertiesOptions) {

    const oLEDOptions = this.getLEDOptions();

    if (oPropertiesOptions) {
      let oLEDProperties = oPropertiesOptions.led;
      if (oLEDProperties) {
        let oLEDRGB = oLEDProperties.rgb;
        if (oLEDRGB) {
          if (oLEDRGB.enabled) {
            oLEDOptions.rgb.enabled = true;
          }
          if (oLEDRGB.brightness) {
            oLEDOptions.rgb.brightness = true;
          }
        }
        let oLEDTunable = oLEDProperties.tunable;
        if (oLEDTunable) {
          if (oLEDTunable.enabled) {
            oLEDOptions.tunable.enabled = true;
          }
          if (oLEDTunable.brightness) {
            oLEDOptions.tunable.brightness = true;
          }
          if (oLEDTunable.minTemp) {
            oLEDOptions.tunable.minTemp = oLEDTunable.minTemp;
          }
          if (oLEDTunable.maxTemp) {
            oLEDOptions.tunable.maxTemp = oLEDTunable.maxTemp;
          }
        }
        let oLEDWW = oLEDProperties.ww;
        if (oLEDWW) {
          if (oLEDWW.enabled) {
            oLEDOptions.ww.enabled = true;
          }
          if (oLEDWW.brightness) {
            oLEDOptions.ww.brightness = true;
          }
        }
        let oLEDCW = oLEDProperties.cw;
        if (oLEDCW) {
          if (oLEDCW.enabled) {
            oLEDOptions.cw.enabled = true;
          }
          if (oLEDCW.brightness) {
            oLEDOptions.cw.brightness = true;
          }
        }
      }
    }

  }

  selectLEDMode(sType, oFieldElement) {

    let sValue = oFieldElement.checked;
    this.getLEDOptions()[sType].enabled = sValue;
  }

  selectLEDOption(sType, sField, oFieldElement) {

    let sValue = "";
    if (sType === "tunable" && (sField === "minTemp" || sField === "maxTemp")) {
      sValue = oFieldElement.value;
    } else {
      sValue = oFieldElement.checked;
    }

    this.getLEDOptions()[sType][sField] = sValue;
  }

  validateLEDOptions() {

    let oLEDTunableMinTemp = this.getLEDOptions().tunable.minTemp;
    /*
    if(minTemp < 0) {
      HomeAutomation.Util.setFieldError("LEDMinTempErrorHolder", "Min Temp must be greater than 0");
      return true;
    }
    if(maxTemp > 12000) {
      HomeAutomation.Util.setFieldError("LEDMaxTempErrorHolder", "Max Temp must less than 12000");
      return true;
    }
    if(minTemp > maxTemp) {
      HomeAutomation.Util.setFieldError("LEDMaxTempErrorHolder", "Min Temp must be less than Max Temp");
      HomeAutomation.Util.setFieldError("LEDMaxTempErrorHolder", "Max Temp must greater than Min Temp");
      return true;
    }
    */
    if (oLEDTunableMinTemp < 0) {
      // TODO: push error with message
    }
  }

  toJSON() {
    return this.ledOptions;
  }

}
