export class SwitchBuilderClass {

  constructor(oControlAddEditModel) {

    this.controlAddEditModel = oControlAddEditModel;
    this.statusListObject = new HomeAutomation.Control.Class.StatusListBuilderClass(oControlAddEditModel.getData().statusPropertiesJSON);
    this.switchObject = new SwitchCreate(oControlAddEditModel);
  }

  getStatusListObject() {
    return this.statusListObject;
  }

  getSwitchObject() {
    return this.switchObject;
  }

  getControlTemplateName() {
    return 'CONTROL_ADD_EDIT_SWITCH';
  }

  validateControlSpecificFields() {
    return this.getStatusListObject().validateStatusEntries();
  }

  getProperties() {
    return undefined;
  }

  getStatusProperties() {
    return this.getStatusListObject().getStatusListForSave();
  }

  getEventProperties() {
    return undefined;
  }

  toJSON() {
    let jsonArr = {};

    jsonArr.statusList = this.getStatusListObject().toJSON();

    return jsonArr;
  }

}

class SwitchCreate {

  constructor(oInitialControlProperties) {
    this.controlData = oInitialControlProperties.data;
  }

  getControlData() {
    return this.controlData;
  }

  onControlPersistSelectChange(oElement) {

    if (oElement.checked) {
      this.getControlData().persistState = 1;
    } else {
      this.getControlData().persistState = 0;
    }
  }
}
