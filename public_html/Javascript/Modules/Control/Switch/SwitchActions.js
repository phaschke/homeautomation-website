const namespace = 'HomeAutomation.Control.Switch';

export function toggleSwitchOn(iControlId, iDuration) {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'toggleSwitchOn',
      params: [iControlId, iDuration]
    }
  };

  let oAction = {
    action: 'TOGGLE_ON',
    parameters: []
  }

  if (iDuration > 0) {
    oAction.parameters.push({
      parameterName: 'duration',
      parameter: iDuration
    });
  }

  let oControl = new HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  oControl.loaded = false;
  oControl.render();

  HomeAutomation.Control.Service.doAction(oAuthData, iControlId, oAction).then(function(oReturnedControl) {

    if (!HomeAutomation.Util.validateWebserviceCall(oReturnedControl)) return;

    oControl = new HomeAutomation.Control.Class.Control(oReturnedControl);
    oControl.render();
  });
}

export function toggleSwitchOff(iControlId) {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'toggleSwitchOff',
      params: [iControlId]
    }
  };

  let oAction = {
    action: 'TOGGLE_OFF',
    parameters: []
  }

  let oControl = new HomeAutomation.Control.getLoadedControlsListControl(iControlId);
  oControl.loaded = false;
  oControl.render();

  HomeAutomation.Control.Service.doAction(oAuthData, iControlId, oAction).then(function(oReturnedControl) {

    if (!HomeAutomation.Util.validateWebserviceCall(oReturnedControl)) return;

    HomeAutomation.Control.Timer.removeTimer(iControlId);

    oControl = new HomeAutomation.Control.Class.Control(oReturnedControl);
    oControl.render();
  });
}
