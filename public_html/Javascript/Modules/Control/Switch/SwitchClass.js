export class Switch {

  constructor(oControl) {
    this.oControl = oControl;
    this.activeTimer = false;
    this.setTemplate();

    this.initialize();
  }

  getControlData() {
    return this.oControl;
  }

  getActiveTimer() {
    return this.activeTimer;
  }

  setActiveTimer(bActiveFlag) {
    this.activeTimer = bActiveFlag;
  }

  initialize() {

    if(this.oControl.isError != 0) {
      if (this.oControl.currentState.timer !== undefined) {

        // Do not start timers if flag is set to false
        if(this.oControl.editMode) {
          return;
        }

        if (this.oControl.currentState.timer > 0) {

          // set timer here
          HomeAutomation.Control.Timer.addTimerAndStartInterval(this.oControl.id, this.oControl.currentState.timer);
          this.setActiveTimer(true);

        } else {
          // remove switch from timer list if timer is not defined
          HomeAutomation.Control.Timer.removeTimer(this.oControl.id);
          this.setActiveTimer(false);
        }
      }
    }

    this.oControl.loaded = 1;
  }

  setTemplate() {
    this.oControl.template = "ControlSwitch";
  }

  isLoaded() {
    if (!this.hasActiveTimer()) {
      return true;
    } else {
      return false;
    }
  }

  needsRefresh() {
    if (this.hasActiveTimer()) {
      return true;
    } else {
      return false;
    }
  }

  hasActiveTimer() {
    if (this.getControlData().currentState.timer !== undefined && this.getControlData().currentState.timer > 0) {
      return true;
    }
    return false;
  }

  toggleDevice(iControlId, iTimerDuration, aParams) {
    
      HomeAutomation.Control.Switch.toggleSwitchOn(iControlId, iTimerDuration);
  }

  toJSON() {
    return {};
  }

}
