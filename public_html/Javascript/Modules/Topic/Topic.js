const namespace = 'HomeAutomation.Topic';

var _LOADED_TOPICS = new Map();

function addTopicToLoadedTopicList(oTopic) {
  _LOADED_TOPICS.set(oTopic.id, oTopic);
}

function addToLoadedTopicList(aTopics) {

  for (var i = 0; i < aTopics.length; i++) {
    addTopicToLoadedTopicList(aTopics[i]);
  }
}

function getLoadedTopic(iId) {
  return _LOADED_TOPICS.get(iId);
}

function getSortedLoadedTopicList() {

  let aTopicList = Array.from(_LOADED_TOPICS.values());

  aTopicList.sort((a, b) => {
    return a.displayOrder - b.displayOrder;
  });

  return aTopicList;
}

function clearLoadedTopicList() {
  _LOADED_TOPICS.clear();
}

export function displayList(aArgs) {

  // aArgs[0] sType
  // aArgs[1] oCallback (optional)

  var sType = aArgs[0];

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'displayList',
      params: [aArgs[0]]
    }
  };

  if (aArgs[1]) {
    oAuthData.callback = aArgs[1];
  }

  return new Promise((resolve, reject) => {

    HomeAutomation.Topic.Service.getAll(oAuthData).then(function(aTopics) {

      if (!HomeAutomation.Util.validateWebserviceCall(aTopics, 'topic-list-holder')) return reject();
      if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aTopics, new HomeAutomation.Class.Message('No Topics Found.'), 'topic-list-holder')) return reject();

      clearLoadedTopicList();
      addToLoadedTopicList(aTopics);

      let oData = {};
      oData.topics = getSortedLoadedTopicList();
      oData.rootURL = HomeAutomation.Config.ROOT_URL;
      oData.listOrderToggle = false;

      if (sType == 'view') {
        HomeAutomation.Util.executeHandlebars('topic-list-holder', 'TopicList', oData, 'clear');
        return resolve();
      }
      if (sType == 'edit') {
        HomeAutomation.Util.executeHandlebars('topic-list-holder', 'TopicEditList', oData, 'clear');
        return resolve();
      }

    });
  });
}

export function openAddEditModal(iTopicId, aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
  }

  let oAuthData = {
    'check': true,
    'callback': {
      namespace: namespace,
      func: 'openAddEditModal',
      params: [iTopicId]
    }
  };

  let oData = {};
  oData.view = {};

  if (iTopicId != undefined && iTopicId != '') {
    oData.view.edit = true;

    HomeAutomation.Topic.Service.getOne(oAuthData, iTopicId).then(function(oTopic) {

      if (!HomeAutomation.Util.validateWebserviceCall(oTopic)) return;
      oData.topic = oTopic;

      HomeAutomation.Util.executeHandlebars('modal-content', 'TopicAddEdit', oData, 'clear');
    });
  } else {

    HomeAutomation.Util.executeHandlebars('modal-content', 'TopicAddEdit', oData, 'clear');
  }

}

export function onAddOrUpdate(iTopicId, aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
  }

  let oAuthData = {
    'check': true,
    'callback': {
      namespace: namespace,
      func: 'onAddOrUpdate',
      params: [iTopicId]
    }
  };

  if (validateForm()) return;

  let oTopic = getFormData();

  if (iTopicId != undefined && iTopicId != '') {
    oTopic.id = iTopicId;

    HomeAutomation.Topic.Service.update(oAuthData, oTopic).then(function(bResult) {
      afterAddOrUpdate(bResult);
    });

  } else {

    HomeAutomation.Topic.Service.add(oAuthData, oTopic).then(function(bResult) {
      afterAddOrUpdate(bResult);
    });

  }

  function validateForm() {

    let bIsError = false;

    if (!HomeAutomation.Util.validateField('topicTitleField', 'topicTitleErrorHolder')) bIsError = true;

    if (!HomeAutomation.Util.validateField('topicNameField', 'topicNameErrorHolder')) {
      bIsError = true;
    } else {
      if (!HomeAutomation.Util.validateObjectNameInput('topicNameField', 'topicNameErrorHolder')) bIsError = true;
    }

    return bIsError;
  }

  function getFormData() {

    let sTitle = document.getElementById('topicTitleField').value;
    let sName = document.getElementById('topicNameField').value;
    let iActive = 1;
    if (!document.getElementById('activeField').checked) iActive = 0;

    return {
      topicTitle: sTitle,
      topic: sName,
      active: iActive
    }
  }

  function afterAddOrUpdate(bResult) {
    if (!HomeAutomation.Util.validateWebserviceCall(bResult, 'formErrorHolder')) return;

    let aArgs = ['edit'];
    displayList(aArgs);

    HomeAutomation.Util.closeModal();
    HomeAutomation.Util.addToast('success', 'Success', 'Successfully saved the topic.');
  }

}

export function displayDeleteConfirmation(iTopicId) {

  let oData = {
    header: 'Confirm Deletion',
    message: 'Are you sure you want to delete the topic?',
    callback: `HomeAutomation.Topic.onDelete(${iTopicId})`
  };
  HomeAutomation.Util.executeHandlebars('modal-content', 'DeleteConfirmation', oData, 'clear');
}

export function onDelete(iTopicId, aArgs) {

  if (aArgs) {
    var iTopicId = aArgs[0];
  }

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onDelete',
      params: [iTopicId]
    }
  };

  HomeAutomation.Topic.Service.remove(oAuthData, iTopicId).then(function(bResult) {
    if (!HomeAutomation.Util.validateWebserviceCall(bResult)) return;

    let aArgs = ['edit'];
    displayList(aArgs);

    HomeAutomation.Util.closeModal();
  });

}

export function onReorderListButtonActivate() {

  let oData = {};
  oData.topics = getSortedLoadedTopicList();
  oData.listOrderToggle = true;
  HomeAutomation.Util.executeHandlebars('topic-list-holder', 'TopicEditList', oData, 'clear');

  let eSortableTopicList = document.getElementById('sortableTopicList');

  Sortable.create(eSortableTopicList, {
    animation: 150,
    ghostClass: 'ghostClass'
  });
}

export function getTopicListOrderThenSave() {

  let eTopicList = document.getElementById('sortableTopicList');

  let aTopicListChildren = eTopicList.children;

  let iCounter = 0;
  Array.from(aTopicListChildren).forEach(li => {

    let iTopicId = li.id.split('-')[1];
    let oTopic = getLoadedTopic(parseInt(iTopicId, 10));
    oTopic.displayOrder = iCounter++;
    addTopicToLoadedTopicList(oTopic);
  });

  onReorderListButtonSave();
}

function onReorderListButtonSave() {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onReorderListButtonSave',
      params: []
    }
  };

  let aTopics = getSortedLoadedTopicList();

  HomeAutomation.Topic.Service.updateDisplayOrder(oAuthData, aTopics).then(function(aTopics) {

    if (!HomeAutomation.Util.validateWebserviceCall(aTopics)) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aTopics, new HomeAutomation.Class.Message('No Topics Found.'))) return;

    clearLoadedTopicList();
    addToLoadedTopicList(aTopics);

    let oData = {};
    oData.topics = getSortedLoadedTopicList();
    oData.rootURL = HomeAutomation.Config.ROOT_URL;
    oData.listOrderToggle = false;

    HomeAutomation.Util.executeHandlebars('topic-list-holder', 'TopicEditList', oData, 'clear');

  });

}
