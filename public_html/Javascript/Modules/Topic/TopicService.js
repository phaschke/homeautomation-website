const namespace = 'HomeAutomation.Topic.Service';

export function getOne(oAuthData, iTopicId) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/topics/${iTopicId}`, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Topic', xhrObj);
  });
}

export function getAll(oAuthData) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', '/api/topics', oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return unpackResponse(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Topics', xhrObj);
  });

}

export function add(oAuthData, oTopic) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oTopic), 'json', '/api/topics', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Adding Topic', xhrObj);
  });
}

export function update(oAuthData, oTopic) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oTopic), 'json', '/api/topics', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Updating Topic', xhrObj);
  });
}

export function updateDisplayOrder(oAuthData, aTopics) {

  let request = new HomeAutomation.Class.WebserviceRequest('PATCH', JSON.stringify(aTopics), 'json', '/api/topics/reorder', oAuthData, false);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return unpackResponse(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Reordering Topics', xhrObj);
  });
}

export function remove(oAuthData, iTopicId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, 'json', `/api/topics/${iTopicId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Deleting Topic', xhrObj);
  });
}

function unpackResponse(oResponse) {

  if(oResponse._embedded && oResponse._embedded.topics) {
    return oResponse._embedded.topics;
  } else {
    return [];
  }
}
