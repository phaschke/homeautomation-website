export class Schedule {

  constructor(oScheduleJSON) {
    if (oScheduleJSON === undefined) {
      var oScheduleJSON = {};
    }
    this.id = oScheduleJSON.id === undefined ? '' : oScheduleJSON.id;
    this.name = oScheduleJSON.name === undefined ? '' : oScheduleJSON.name;
    this.type = oScheduleJSON.type === undefined ? '' : oScheduleJSON.type;
    this.day = oScheduleJSON.day === undefined ? '' : oScheduleJSON.day;
    this.days = oScheduleJSON.days === undefined ? '' : oScheduleJSON.days;
    this.date = oScheduleJSON.date === undefined ? '' : oScheduleJSON.date;
    this.minute = oScheduleJSON.minute === undefined ? '' : oScheduleJSON.minute;
    this.time = oScheduleJSON.time === undefined ? '' : oScheduleJSON.time;
    this.active = oScheduleJSON.active === undefined ? '' : oScheduleJSON.active;
    this.actionId = oScheduleJSON.actionId === undefined ? '' : oScheduleJSON.actionId;
    this.parsedDays = {};

    this.errors = {};

    this.view = {};
    this.view.edit = false;
    this.view.eventSelectionEnabled = false;
    this.view.action = null;
    this.view.actionSelectCallback = '';

    this.view.namespace = 'HomeAutomation.Schedule';
    this.view.binding = 'HomeAutomation.Schedule.getLoadedSchedule()';
  }

  parseSchedule() {

    if(this.days) {
      let oParsedDays = {
        sun: false,
        mon: false,
        tue: false,
        wed: false,
        thu: false,
        fri: false,
        sat: false,
      };

      let aDayLookup = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
      let aDays = JSON.parse(this.days);
      aDays.forEach((day, i) => {
        oParsedDays[aDayLookup[day]] = true;
      });

      this.parsedDays = oParsedDays;
    }
  }

  setAction(aAction) {
    this.view.action = aAction;
  }

  setEdit(bEdit) {
    this.view.edit = bEdit;
  }

  onNameInput(sName) {
    this.name = sName;
  }

  onActiveInput(eElement) {
    let iActive = 1;
    if(!eElement.checked) iActive = 0;

    this.active = iActive;
  }

  onActionSelect(iActionId) {
    this.actionId = iActionId;
    this.render();
  }

  onTypeSelect(sType) {
    this.type = sType;
    this.day = '';
    this.days = [];
    this.time = '';
    this.date = '';
    this.render();
  }

  onTimeInput(sTime) {
    this.time = sTime;
  }

  onDaysInput(sDay, eElement) {
    let bChecked = false;
    if(eElement.checked) {
      bChecked = true;
    }
    this.parsedDays[sDay] = bChecked;
  }

  onDayInput(sDay, iMax) {
    this.day = sDay;
  }

  onMinuteInput(sMinute, iMax) {
    this.minute = sMinute;
  }

  onDateInput(sDate) {
    this.date = sDate;
  }

  validateDay(sDay, iMax) {
    if(parseInt(sDay) < 0) {
      this.addError('day', this.createRequiredModel(true, `Day must be greater than 0.`));
      this.render();
      return false;
    }
    if(parseInt(sDay) > iMax) {
      this.addError('day', this.createRequiredModel(true, `Day must be less than ${iMax} days.`));
      this.render();
      return false;
    }
    return true;
  }

  validateMinute(sMinute, iMax) {
    if(parseInt(sMinute) < 0) {
      this.addError('minute', this.createRequiredModel(true, `Minute must be greater than 0.`));
      this.render();
      return false;
    }
    if(parseInt(sMinute) > iMax) {
      this.addError('minute', this.createRequiredModel(true, `Minute must be less than ${iMax} minutes.`));
      this.render();
      return false;
    }
    return true;
  }

  parseDaysToJSON(oDays) {
    let aDays = [];

    if(oDays.sun) aDays.push(0);
    if(oDays.mon) aDays.push(1);
    if(oDays.tue) aDays.push(2);
    if(oDays.wed) aDays.push(3);
    if(oDays.thu) aDays.push(4);
    if(oDays.fri) aDays.push(5);
    if(oDays.sat) aDays.push(6);

    return aDays;
  }

  clearErrors() {
    this.errors = {};
  }

  addError(sError, sErrorText) {
    this.errors[sError] = sErrorText;
  }

  removeError(sError) {
    delete this.errors[sError];
  }

  validate() {

    let oErrorModel = {};
    let bIsError = false;
    this.clearErrors();
    let sRequiredMessage = 'This field is required.';

    if (!this.validateRequiredVariable('name')) {
      this.addError('name', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('actionId')) {
      this.addError('actionSelect', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('type')) {
      this.addError('typeSelect', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    if(this.type) {

      if(this.type != 'HOURLY') {
        if (!this.validateRequiredVariable('time')) {
          this.addError('time', this.createRequiredModel(true, sRequiredMessage));
          bIsError = true;
        }
      }

      if(this.type == 'HOURLY') {
        if (!this.validateRequiredVariable('minute')) {
          this.addError('minute', this.createRequiredModel(true, sRequiredMessage));
          bIsError = true;
        } else {
          if(!this.validateMinute(this.minute, 59)) {
            bIsError = true;
          }
        }
      }

      if(this.type == 'WEEKLY') {
        if (!this.validateParsedDays()) {
          this.addError('days', this.createRequiredModel(true, sRequiredMessage));
          bIsError = true;
        }
      }

      if(this.type == 'MONTHLY') {
        if (!this.validateRequiredVariable('day')) {
          this.addError('day', this.createRequiredModel(true, sRequiredMessage));
          bIsError = true;
        } else {
          if(!this.validateDay(this.day, 31)) {
            bIsError = true;
          }
        }
      }

      if(this.type == 'YEARLY') {
        if (!this.validateRequiredVariable('day')) {
          this.addError('day', this.createRequiredModel(true, sRequiredMessage));
          bIsError = true;
        } else {
          if(!this.validateDay(this.day, 366)) {
            bIsError = true;
          }
        }
      }

      if(this.type == 'FIXED') {
        if (!this.validateRequiredVariable('date')) {
          this.addError('date', this.createRequiredModel(true, sRequiredMessage));
          bIsError = true;
        }
      }
    }

    this.render();
    return bIsError;
  }

  validateParsedDays() {
    let bValid = false;

    let aKeys = Object.keys(this.parsedDays);
    for (var i = 0; i < aKeys.length; i++) {
      if(this.parsedDays[aKeys[i]]) bValid = true;
    }

    return bValid;
  }

  validateRequiredVariable(sVariable) {
    if (this[sVariable] != "" && this[sVariable] != undefined) {
      return true;
    }
    return false;
  }

  createRequiredModel(bShowMessage, sMessage) {
    let oErrorSegment = {
      errClass: 'is-invalid'
    }
    if (bShowMessage) {
      oErrorSegment.errMsg = sMessage;
    }
    return oErrorSegment;
  }

  toPostJSON() {

    let oPostJSON = {
      id: this.id,
      name: this.name,
      type: this.type,
      minute: this.minute,
      day: this.day,
      days: JSON.stringify(this.parseDaysToJSON(this.parsedDays)),
      time: this.time,
      date: this.date,
      active: this.active,
      actionId: this.actionId
    }

    return oPostJSON;

  }

  toViewJSON() {
    return {
      id: this.id,
      name: this.name,
      type: this.type,
      minute: this.minute,
      day: this.day,
      parsedDays: this.parsedDays,
      date: this.date,
      time: this.time,
      active: this.active,
      actionId: this.actionId,
      view: this.view,
      errors: this.errors
    }
  }

  render(eFocusElement) {

    HomeAutomation.Util.executeHandlebars('modal-content', 'ScheduleAddEdit', this.toViewJSON(), 'clear', function() {
      if(eFocusElement) {
        eFocusElement.focus();
        eFocusElement.select();
      }
    });
  }

}
