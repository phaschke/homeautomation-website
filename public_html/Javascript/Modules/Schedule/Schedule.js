const namespace = 'HomeAutomation.Schedule';

var _LOADED_SCHEDULE_ = null;

function setLoadedSchedule(oSchedule) {
  _LOADED_SCHEDULE_ = oSchedule;
}

export function getLoadedSchedule() {
  return _LOADED_SCHEDULE_;
}

export function displayList(oFilter) {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'displayList',
      params: [oFilter]
    }
  };

  if(oFilter && !oFilter.getActive()) {
    oFilter = undefined;
  }

  HomeAutomation.Util.executeHandlebars('schedule-list-holder', 'ScheduleList', {}, 'clear');

  HomeAutomation.Schedule.Service.getAll(oAuthData, oFilter).then(function(aSchedules) {

    if (!HomeAutomation.Util.validateWebserviceCall(aSchedules, 'schedule-list-holder')) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aSchedules, new HomeAutomation.Class.Message('No Schedules Found.'), 'schedule-table')) return;

    let oData = {};
    oData.schedules = aSchedules;

    if(oFilter) {
      oData.filters = oFilter.toJSON();
      oData.showFilters = true;
    }

    HomeAutomation.Util.executeHandlebars('schedule-list-holder', 'ScheduleList', oData, 'clear');
  });
}

export function openAddEditModal(iScheduleId, oArgs) {

  if (oArgs) {
    var iScheduleId = oArgs[0];
  }

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'openAddEditModal',
      params: [iScheduleId]
    }
  };

  let sActionSetCallback = 'Schedule.onSetAction';

  if (iScheduleId) {

    HomeAutomation.Schedule.Service.getOne(oAuthData, iScheduleId).then(function(oLoadedSchedule) {

      if (!HomeAutomation.Util.validateWebserviceCall(oLoadedSchedule)) return;
      parseAndDisplayEditModal(oLoadedSchedule, iScheduleId);
    });

  } else {

    displayAddModal();
  }

  function parseAndDisplayEditModal(oLoadedSchedule) {

    let oSchedule = new HomeAutomation.Schedule.Class.Schedule(oLoadedSchedule);

    HomeAutomation.Action.Service.getOne(oAuthData, oSchedule.actionId).then(function(aAction) {

      if (!HomeAutomation.Util.validateWebserviceCall(aAction)) return;

      oSchedule.setAction(aAction);
      oSchedule.parseSchedule();
      oSchedule.setEdit(true);
      oSchedule.view.actionSelectCallback = sActionSetCallback;

      setLoadedSchedule(oSchedule);
      oSchedule.render();
    });
  }


  function displayAddModal(aActions) {

    let oSchedule = new HomeAutomation.Schedule.Class.Schedule();

    oSchedule.view.actionSelectCallback = sActionSetCallback;

    setLoadedSchedule(oSchedule);
    oSchedule.render();
  }

}

export function getSelectedValue(eElement) {
  return eElement.options[eElement.selectedIndex].value;
}

export function getInputValue(eElement) {
  return eElement.value;
}

export function onAddOrUpdate(iScheduleId, oArgs) {

  if (oArgs) {
    var iScheduleId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onAddOrUpdate',
      params: [iScheduleId]
    }
  };

  let oLoadedSchedule = getLoadedSchedule();
  if (oLoadedSchedule.validate()) {
    return;
  }

  let oSchedule = oLoadedSchedule.toPostJSON();

  if(iScheduleId) {

    HomeAutomation.Schedule.Service.update(oAuthData, oSchedule).then(function(oResult) {

      updateAndShowList(oResult, oLoadedSchedule);
    });

  } else {

    HomeAutomation.Schedule.Service.add(oAuthData, oSchedule).then(function(oResult) {
      updateAndShowList(oResult, oLoadedSchedule);
    });
  }

  function updateAndShowList(oResult, oLoadedSchedule) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      if (oResult.type == "ERR") {
        oLoadedSchedule.addError("formError", oResult.header + " " + oResult.details);
        oLoadedSchedule.render();
        return;
      }
    }

    displayList();
    HomeAutomation.Util.closeModal();
  }
}

export function onDeleteConfirmation(iScheduleId) {

  let oData = {
    header: 'Confirm Deletion',
    message: 'Are you sure you want to delete the schedule?',
    callback: `HomeAutomation.Schedule.onDelete(${iScheduleId})`
  };

  HomeAutomation.Util.executeHandlebars('modal-content', 'DeleteConfirmation', oData, 'clear');
}

export function onDelete(iScheduleId, oArgs) {

  if (oArgs) {
    var iScheduleId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onDelete',
      params: [iScheduleId]
    }
  }

  HomeAutomation.Schedule.Service.remove(oAuthData, iScheduleId).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      oResult.displayModal();

    } else {

      displayList();
      HomeAutomation.Util.closeModal();
    }
  });
}

export function onSearch() {

  let oFilter = getFilters();
  displayList(oFilter);
}

function getFilters() {

  let eNameFilter = document.getElementById('nameFilter');
  let eTypeFilter = document.getElementById('typeFilter');

  let oFilter = new HomeAutomation.Class.Filter;

  if(!eNameFilter || !eTypeFilter) {
    oFilter.setActive(false);
    return oFilter;
  }

  let sNameFilter = eNameFilter.value;
  let sTypeFilter = eTypeFilter.value;

  oFilter.addFilter('name', sNameFilter);
  oFilter.addFilter('type', sTypeFilter);

  return oFilter;
}

export function showFilters() {

  let oData = {
    showFilters: true
  }
  HomeAutomation.Util.executeHandlebars('scheduleFilters', 'ScheduleFilter', oData, 'clear');
}

export function hideFilters() {

  let oData = {
    showFilters: false
  }
  HomeAutomation.Util.executeHandlebars('scheduleFilters', 'ScheduleFilter', oData, 'clear');

  displayList();
}

export function onSetActive(iScheduleId, bActive, oArgs) {

  if (oArgs) {
    var iScheduleId = oArgs[0];
    var bActive = oArgs[1];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onSetActive',
      params: [iScheduleId, bActive]
    }
  }

  HomeAutomation.Schedule.Service.patchActiveFlag(oAuthData, iScheduleId, bActive).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      oResult.displayModal();

    } else {

      let oFilter = getFilters();
      displayList(oFilter);
    }
  });
}

export function onSetAction(iActionId, sActionTitle, sActionName) {

  let oLoadedSchedule = getLoadedSchedule();
  oLoadedSchedule.onActionSelect(iActionId);

  if(!oLoadedSchedule.view.action) {
    oLoadedSchedule.view.action = {
      title: '',
      name: ''
    }
  }
  oLoadedSchedule.view.action.title = sActionTitle;
  oLoadedSchedule.view.action.name = sActionName;

  setLoadedSchedule(oLoadedSchedule);

  oLoadedSchedule.render();
}

export function getServiceStatus() {

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'getServiceStatus',
      params: []
    }
  }

  let oData = {};
  oData.loading = true;
  HomeAutomation.Util.executeHandlebars('scheduler-status-holder', 'SchedulerServiceStatus', oData, 'clear');

  HomeAutomation.Schedule.Service.getServiceStatus(oAuthData).then(function(oResult) {

    oData = {};

    if (oResult instanceof HomeAutomation.Class.Message) {
      oData.fail = true;
    } else {
      oData.success = true;
    }
    HomeAutomation.Util.executeHandlebars('scheduler-status-holder', 'SchedulerServiceStatus', oData, 'clear');
  });
}
