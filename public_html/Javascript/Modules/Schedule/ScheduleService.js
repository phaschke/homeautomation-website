const namespace = 'HomeAutomation.Schedule.Service';

export function getOne(oAuthData, iScheduleId) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/schedules/${iScheduleId}`, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Schedule', xhrObj);
  });

}

export function getAll(oAuthData, oFilter) {

  let sRequestURL = '/api/schedules';
  if(oFilter) {
    if(oFilter.filterCount() > 0) {
        sRequestURL = `/api/schedules?${oFilter.toURLRequest()}`;
    }
  }

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', sRequestURL, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return unpackResponse(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Schedules', xhrObj);
  });

  function unpackResponse(response) {

    if (response._embedded && response._embedded.schedules) {
      return response._embedded.schedules;
    } else {
      return [];
    }
  }

}

export function add(oAuthData, oSchedule) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oSchedule), 'json', '/api/schedules', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {


    return HomeAutomation.Util.buildErrorMessage('Error Adding Schedule', xhrObj);
  });
}

export function update(oAuthData, oSchedule) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oSchedule), "json", `/api/schedules/${oSchedule.id}`, oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Updating Schedule', xhrObj);
  });
}

export function patchActiveFlag(oAuthData, iScheduleId, bFlag) {

  let oRequest;
  if(bFlag) {
    oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/schedules/${iScheduleId}/active`, oAuthData, true);
  } else {
    oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/schedules/${iScheduleId}/inactive`, oAuthData, true);
  }
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Setting Schedule Active/Inactive', xhrObj);
  });
}

export function remove(oAuthData, iScheduleId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, 'json', `/api/schedules/${iScheduleId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Deleting Schedule', xhrObj);
  });
}

export function getServiceStatus(oAuthData) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', '/api/scheduler/status', oAuthData, true);
  request.setServiceURL(HomeAutomation.Config.SCHEDULER_WEBSERVICE_URL);

  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    if(xhrObj && xhrObj.status && xhrObj.status == '200') {
      return true;
    }

    return HomeAutomation.Util.buildErrorMessage('Error contacting service', xhrObj);
  });
}
