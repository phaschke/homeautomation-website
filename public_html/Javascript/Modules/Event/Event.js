const namespace = "HomeAutomation.Event";

var _LOADED_EVENT_ = null;

function setLoadedEvent(oEvent) {
  _LOADED_EVENT_ = oEvent;
}

export function getLoadedEvent() {
  return _LOADED_EVENT_;
}

export function displayList(oFilter) {

  let oAuthData = {
    "check": true,
    "callback": {
      namespace: namespace,
      func: "displayList",
      params: [oFilter]
    }
  };

  if(oFilter && !oFilter.getActive()) {
    oFilter = undefined;
  }

  HomeAutomation.Util.executeHandlebars("events-list-holder", "EventList", {}, "clear");

  HomeAutomation.Event.Service.getAll(oAuthData, oFilter).then(function(aEvents) {

    if (!HomeAutomation.Util.validateWebserviceCall(aEvents, "events-list-holder")) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aEvents, new HomeAutomation.Class.Message("No Events Found."), "event-table")) return;

    let oData = {};
    oData.events = aEvents;

    if(oFilter) {
      oData.filters = oFilter.toJSON();
      oData.showFilters = true;
    }

    HomeAutomation.Util.executeHandlebars("events-list-holder", "EventList", oData, "clear");

  });

}

export function openAddEditModal(iEventId, oArgs) {

  if (oArgs) {
    var iEventId = oArgs[0];
  }

  var oAuthData = {
    "check": true,
    "callback": {
      namespace: namespace,
      func: "openAddEditModal",
      params: [iEventId]
    }
  };

  let sActionSetCallback = "Event.onSetAction";

  HomeAutomation.Control.Service.getAll(oAuthData).then(function(aControls) {

    if (!HomeAutomation.Util.validateWebserviceCall(aControls)) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aControls, new HomeAutomation.Class.Message("No Controls Found", "No controls found, add valid controls first."))) return;

    if (iEventId) {
      parseAndDisplayEditModal(aControls, iEventId);

    } else {
      displayAddModal(aControls);
    }
  });

  function parseAndDisplayEditModal(aControls, iEventId) {

    HomeAutomation.Event.Service.getOne(oAuthData, iEventId).then(function(oLoadedEvent) {

      if (!HomeAutomation.Util.validateWebserviceCall(oLoadedEvent)) return;
      let oEvent = new HomeAutomation.Event.Class.Event(oLoadedEvent);

      HomeAutomation.Action.Service.getOne(oAuthData, oEvent.actionId).then(function(aAction) {

        if (!HomeAutomation.Util.validateWebserviceCall(aAction)) return;

          oEvent.setControls(aControls);
          oEvent.parseLoadedEvent();
          oEvent.setEditTrue();
          oEvent.setAction(aAction);
          oEvent.view.actionSelectCallback = sActionSetCallback;

          setLoadedEvent(oEvent);

          oEvent.render();
      });
    });
  }

  function displayAddModal(aControls) {

    let oEvent = new HomeAutomation.Event.Class.Event();

    oEvent.setControls(aControls);
    oEvent.view.actionSelectCallback = sActionSetCallback;

    setLoadedEvent(oEvent);

    oEvent.render();
  }
}

export function onAddOrUpdate(iEventId, oArgs) {

  if (oArgs) {
    var iEventId = oArgs[0];
  }

  var oAuthData = {
    "check": true,
    "callback": {
      namespace: namespace,
      func: "onAddOrUpdate",
      params: [iEventId]
    }
  };

  let oLoadedEvent = getLoadedEvent();
  if (oLoadedEvent.validateDataAndSetErrorModel()) {
    return;
  }

  if(iEventId != undefined && iEventId != "") {

    HomeAutomation.Event.Service.update(oAuthData, oLoadedEvent.toPostJSON()).then(function(oResult) {

      updateAndShowEventList(oResult, oLoadedEvent);
    });

  } else {

    HomeAutomation.Event.Service.add(oAuthData, oLoadedEvent.toPostJSON()).then(function(oResult) {
      updateAndShowEventList(oResult, oLoadedEvent);
    });
  }

  function updateAndShowEventList(oResult, oLoadedEvent) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      if (oResult.type == "ERR") {
        oLoadedEvent.addError("formError", oResult.header + " " + oResult.details);
        oLoadedEvent.render();
        return;
      }
    }

    displayList();
    HomeAutomation.Util.closeModal();
  }
}

export function onDeleteConfirmation(iEventId) {

  let oData = {
    header: "Confirm Deletion",
    message: "Are you sure you want to delete the event?",
    callback: "HomeAutomation.Event.onDelete(" + iEventId + ")"
  };

  HomeAutomation.Util.executeHandlebars("modal-content", "DeleteConfirmation", oData, "clear");
}

export function onDelete(iEventId, oArgs) {

  if (oArgs) {
    var iEventId = oArgs[0];
  }

  var oAuthData = {
    "check": true,
    "callback": {
      namespace: namespace,
      func: "onDelete",
      params: [iEventId]
    }
  }

  HomeAutomation.Event.Service.remove(oAuthData, iEventId).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      oResult.displayModal();

    } else {

      displayList();
      HomeAutomation.Util.closeModal();
    }
  });

}

export function onSetActive(iEventId, bActive, oArgs) {

  if (oArgs) {
    var iEventId = oArgs[0];
    var bActive = oArgs[1];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onSetActive',
      params: [iEventId, bActive]
    }
  }

  HomeAutomation.Event.Service.patchActiveFlag(oAuthData, iEventId, bActive).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      oResult.displayModal();

    } else {

      let oFilter = getFilters();
      displayList(oFilter);
    }
  });
}

export function onGenerateActions(iEventId, oArgs) {

  if (oArgs) {
    var iEventId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onGenerateActions',
      params: [iEventId]
    }
  }

  HomeAutomation.Event.Service.patchGenerateAction(oAuthData, iEventId, 'generate').then(function(oEvent) {

    if (oEvent instanceof HomeAutomation.Class.Message) {
      oEvent.displayText('actionGenerateErrorHolder');

    } else {

      let oLoadedEvent = getLoadedEvent();
      oLoadedEvent.setRelatedActions(oEvent.relatedActions);
      setLoadedEvent(oLoadedEvent);
      oLoadedEvent.render();
    }
  });
}

export function onDeleteGeneratedActions(iEventId, oArgs) {

  if (oArgs) {
    var iEventId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onDeleteGeneratedActions',
      params: [iEventId]
    }
  }

  HomeAutomation.Event.Service.patchGenerateAction(oAuthData, iEventId, 'delete').then(function(oEvent) {

    if (oEvent instanceof HomeAutomation.Class.Message) {
      oEvent.displayText('actionGenerateErrorHolder');

    } else {

      let oLoadedEvent = getLoadedEvent();
      oLoadedEvent.setRelatedActions(oEvent.relatedActions);
      setLoadedEvent(oLoadedEvent);
      oLoadedEvent.render();
    }
  });
}

export function getSelectedValue(eElement) {
  return eElement.options[eElement.selectedIndex].value;
}

export function getInputValue(eElement) {
  return eElement.value;
}

export function showFilters() {

  let oData = {
    showFilters: true
  }
  HomeAutomation.Util.executeHandlebars('eventFilters', 'EventFilter', oData, 'clear');
}

export function hideFilters() {

  let oData = {
    showFilters: false
  }
  HomeAutomation.Util.executeHandlebars('eventFilters', 'EventFilter', oData, 'clear');

  displayList();
}

export function onSearch() {

  let oFilter = getFilters();
  displayList(oFilter);
}

function getFilters() {


  let eTitleFilter = document.getElementById('titleFilter');
  let eEventFilter = document.getElementById('eventFilter');
  let eControlFilter = document.getElementById('controlFilter');

  let oFilter = new HomeAutomation.Class.Filter;

  if(!eTitleFilter) {
    oFilter.setActive(false);
    return oFilter;
  }

  let sTitleFilter = document.getElementById('titleFilter').value;
  let sEventFilter = document.getElementById('eventFilter').value;
  let sControlFilter = document.getElementById('controlFilter').value;

  oFilter.addFilter('title', sTitleFilter);
  oFilter.addFilter('event', sEventFilter);
  oFilter.addFilter('controluniquename', sControlFilter);

  return oFilter;
}

export function onSetAction(iActionId, sActionTitle, sActionName) {

  let oLoadedEvent = getLoadedEvent();
  oLoadedEvent.onActionSelect(iActionId);

  if(!oLoadedEvent.view.action) {
    oLoadedEvent.view.action = {
      title: '',
      name: ''
    }
  }
  oLoadedEvent.view.action.title = sActionTitle;
  oLoadedEvent.view.action.name = sActionName;

  setLoadedEvent(oLoadedEvent);

  oLoadedEvent.render();
}

export function getServiceStatus() {

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'getServiceStatus',
      params: []
    }
  }

  let oData = {};
  oData.loading = true;
  HomeAutomation.Util.executeHandlebars('event-status-holder', 'EventServiceStatus', oData, 'clear');

  HomeAutomation.Event.Service.getServiceStatus(oAuthData).then(function(oResult) {

    oData = {};

    if (oResult instanceof HomeAutomation.Class.Message) {
      oData.fail = true;
    } else {
      oData.success = true;
    }
    HomeAutomation.Util.executeHandlebars('event-status-holder', 'EventServiceStatus', oData, 'clear');
  });
}
