export class Event {

  constructor(oEventJSON) {
    if (oEventJSON === undefined) {
      var oEventJSON = {};
    }
    this.id = oEventJSON.id === undefined ? "" : oEventJSON.id;
    this.controlId = oEventJSON.controlId === undefined ? "" : oEventJSON.controlId;
    this.actionId = oEventJSON.actionId === undefined ? "" : oEventJSON.actionId;
    this.event = oEventJSON.event === undefined ? "" : oEventJSON.event;
    this.active = oEventJSON.active === undefined ? "" : oEventJSON.active;
    this.title = oEventJSON.title === undefined ? "" : oEventJSON.title;
    this.description = oEventJSON.description === undefined ? "" : oEventJSON.description;
    this.relatedActions = oEventJSON.relatedActions === undefined ? [] : oEventJSON.relatedActions;
    this.control = {};

    this.errors = {};

    this.view = {};
    this.view.controls = new Array();
    this.view.eventSelectionEnabled = false;
    this.view.action = null;
    this.view.actionSelectCallback = '';

    this.view.namespace = "HomeAutomation.Event";
    this.view.binding = "HomeAutomation.Event.getLoadedEvent()";
  }

  setRelatedActions(aRelatedActions) {
    this.relatedActions = aRelatedActions;
  }

  setAction(aAction) {
    this.view.action = aAction;
  }

  setEditTrue() {
    this.view.edit = true;
  }

  enableEventSelection() {
    this.view.eventSelectionEnabled = true;
  }

  disableEventSelection() {
    this.view.eventSelectionEnabled = false;
  }

  setControl(oControl) {
    this.control = oControl;
  }

  getControl() {
    return this.control;
  }

  setControls(aControls) {
    this.view.controls = aControls;
  }

  setEvents(aEvents) {
    this.view.events = aEvents;
  }

  getEvents() {
    return this.view.events;
  }

  clearErrors() {
    this.errors = {};
  }

  addError(sError, sErrorText) {
    this.errors[sError] = sErrorText;
  }

  onTitleInput(sTitle) {
    this.title = sTitle;
  }

  onDescriptionInput(sDescription) {
    this.description = sDescription;
  }

  onActiveInput(eElement) {

    let iActive = 1;
    if(!eElement.checked) iActive = 0;

    this.active = iActive;
  }

  parseLoadedEvent() {

    let oControl = this.getSelectedControl(this.controlId);
    if(oControl == null) {
      this.addError("formError", "Error parsing exisiting event.");
      return;
    }

    this.setControl(new HomeAutomation.Control.Class.Control(oControl));

    if(this.parseControlPropertiesAndSetEvents()) {
      this.enableEventSelection();
    }

    let bEventValid = false;
    let aEvents = this.getEvents();

    for(var i = 0; i < aEvents.length; i++) {

      if(this.event == aEvents[i].name) {
        bEventValid = true;
        continue;
      }
    }
    if(!bEventValid) {
      this.addError("formError", "Error parsing exisiting event, event invalid.");
      return;
    }

  }

  onControlSelect(iControlId) {

    let oControl = this.getSelectedControl(iControlId);
    if(oControl == null) return;

    this.setControl(new HomeAutomation.Control.Class.Control(oControl));
    this.controlId = oControl.id;
    if(this.parseControlPropertiesAndSetEvents()) {
      this.enableEventSelection();
    }

    this.render();
  }

  parseControlPropertiesAndSetEvents() {

    this.clearErrors();

    try {

      let oControl = this.getControl();
      this.setEvents(oControl.eventPropertiesJSON);

      return true;

    } catch(e) {

      this.addError("formError", "Error parsing controls properties.");
      return false;
    }
  }

  getSelectedControl(iControlId) {

    let aControls = this.view.controls;

    for(let i = 0; i < aControls.length; i++) {
      if(aControls[i].id == iControlId) return aControls[i];
    }
    return null;
  }

  onEventSelect(sEvent) {
    this.event = sEvent;
  }

  onActionSelect(iActionId) {
    this.actionId = iActionId;
    this.render();
  }

  validateDataAndSetErrorModel() {

    let oErrorModel = {};
    let bIsError = false;

    this.clearErrors();

    if (!this.validateRequiredVariable('title')) {
      this.addError('title', this.createRequiredModel(true));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('controlId')) {
      this.addError('controlSelect', this.createRequiredModel(true));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('event')) {
      this.addError('eventSelect', this.createRequiredModel(true));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('actionId')) {
      this.addError('actionSelect', this.createRequiredModel(true));
      bIsError = true;
    }

    this.render();

    return bIsError;
  }

  validateRequiredVariable(sVariable) {
    if (this[sVariable] != "" && this[sVariable] != undefined) {
      return true;
    }
    return false;
  }

  createRequiredModel(bShowMessage) {
    let oErrorSegment = {
      errClass: "is-invalid"
    }
    if (bShowMessage) {
      oErrorSegment.errMsg = "This field is required.";
    }
    return oErrorSegment;
  }

  toViewJSON() {
    return {
      id: this.id,
      controlId: this.controlId,
      actionId: this.actionId,
      event: this.event,
      active: this.active,
      title: this.title,
      description: this.description,
      relatedActions: this.relatedActions,
      view: this.view,
      errors: this.errors
    };
  }

  toPostJSON() {
    return {
      id: this.id,
      controlId: this.controlId,
      actionId: this.actionId,
      event: this.event,
      active: this.active,
      title: this.title,
      description: this.description,
    };
  }

  render() {
    HomeAutomation.Util.executeHandlebars("modal-content", "EventAddEdit", this.toViewJSON(), "clear");
  }

}
