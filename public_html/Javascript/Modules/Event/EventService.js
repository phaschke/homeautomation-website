const namespace = 'HomeAutomation.Event.Service';

export function getOne(oAuthData, iEventId) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/events/${iEventId}`, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Event', xhrObj);
  });

}

export function getAll(oAuthData, oFilter) {

  let sRequestURL = '/api/events';
  if(oFilter) {
    if(oFilter.filterCount() > 0) {
        sRequestURL = `/api/events?${oFilter.toURLRequest()}`;
    }
  }

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', sRequestURL, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return handleSuccessResponse(response);

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Loading Events', xhrObj);
  });


  function handleSuccessResponse(response) {

    if(response._embedded && response._embedded.events) {
      return response._embedded.events;
    } else {
      return [];
    }
  }

}

export function add(oAuthData, oEvent) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oEvent), 'json', '/api/events', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {


    return HomeAutomation.Util.buildErrorMessage('Error Adding Event', xhrObj);
  });
}

export function update(oAuthData, oEvent) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oEvent), 'json',  `/api/events/${oEvent.id}`, oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Updating Event', xhrObj);
  });
}

export function patchActiveFlag(oAuthData, iEventId, bFlag) {

  let oRequest;
  if(bFlag) {
    oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/events/${iEventId}/active`, oAuthData, true);
  } else {
    oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/events/${iEventId}/inactive`, oAuthData, true);
  }
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error setting event active/inactive', xhrObj);
  });
}

export function patchGenerateAction(oAuthData, iEventId, sType) {

  let oRequest;
  if(sType == 'generate') {
    oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/events/${iEventId}/generate_actions`, oAuthData, true);
  }
  if(sType == 'delete') {
    oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/events/${iEventId}/delete_actions`, oAuthData, true);
  }
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage(`${sType.charAt(0).toUpperCase()} error event actions`, xhrObj);
  });
}

export function remove(oAuthData, iEventId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, 'json', `/api/events/${iEventId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return HomeAutomation.Util.buildErrorMessage('Error Deleting Event', xhrObj);
  });
}

export function getServiceStatus(oAuthData) {

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', '/api/event/status', oAuthData, true);
  request.setServiceURL(HomeAutomation.Config.EVENT_WEBSERVICE_URL);

  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    if(xhrObj && xhrObj.status && xhrObj.status == '200') {
      return true;
    }

    return HomeAutomation.Util.buildErrorMessage('Error contacting service', xhrObj);
  });
}
