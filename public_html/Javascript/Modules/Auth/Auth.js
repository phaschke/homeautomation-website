const namespace = "HomeAutomation.Auth";

var TOKEN_VAR_NAME = 'token';

export function checkTokenExists(oCallbackDetails) {

  // Check if token is present in local storage
  if (localStorage.getItem('token') == null) {
    this.authenticateWebservice(oCallbackDetails);
    return false;
  }

  return true;
}

export function checkPrivilege() {

  let oResult = {};

  var token = localStorage.getItem('token');

  if (token && token !== '') {

    let jwt = token.replace('Bearer Bearer ','');
    let jwtData = jwt.split('.')[1];
    let decodedJwtJsonData = window.atob(jwtData);
    let decodedJwtData = JSON.parse(decodedJwtJsonData);

    oResult.username = decodedJwtData.sub;
    oResult.loggedIn = true;
    oResult.admin = false;

    if(decodedJwtData.rol.indexOf('ROLE_ADMIN') > -1) {
      oResult.admin = true;
    }

  } else {
    oResult.loggedIn = false;
  }


  return oResult;
}

export function clearAuthToken() {

  localStorage.setItem('token', '');
}

/**
 * Open authentication modal
 */
export function authenticateWebservice(oCallbackDetails) {

  // Open dialog to login
  HomeAutomation.Util.openModal(this.openAuthenticationModal(oCallbackDetails));

}

export function openAuthenticationModal(oCallbackDetails) {

  let oData = {};
  oData.baseUrl = HomeAutomation.Config.ROOT_URL;

  if(oCallbackDetails != undefined) {
    let sCallback = JSON.stringify(oCallbackDetails);
    oData.callback = sCallback.replace('"', '\"');
  }

  HomeAutomation.Util.executeHandlebars('modal-content', 'AuthenticationModal', oData, "clear");
}

export function attemptAuthentication(sCallbackDetails) {

  // Get values from form
  if (!HomeAutomation.Util.validateField("usernameField", "usernameErrorHolder")) return false;
  if (!HomeAutomation.Util.validateField("passwordField", "passwordErrorHolder")) return false;

  var sUsername = document.getElementById("usernameField").value;
  var sPassword = document.getElementById("passwordField").value;

  var oBody = {
    "username": sUsername,
    "password": sPassword
  }

  var oAuthData = {
    "check": false
  };

  HomeAutomation.Util.setFieldError("loginResultErrorHolder", "");

  return Promise.resolve(

    $.ajax({
      type: "POST",
      processData: false,
      headers: {},
      data: JSON.stringify(oBody),
      dataType: "text",
      contentType: 'application/json',
      url: HomeAutomation.Config.ROOT_WEBSERVICE_URL + "/api/authenticate",

      complete: function(response) {

        if (response && response.status == 403) {

          HomeAutomation.Util.setFieldError("loginResultErrorHolder", "Failed to Authenticate, please try again.");

          return;
        }

        if (response && response.status == 200) {

          var token = "Bearer " + response.getResponseHeader("Authorization");
          localStorage.setItem("token", token);

          HomeAutomation.Util.closeModal();

          if(sCallbackDetails) {
            let oCallbackDetails = JSON.parse(sCallbackDetails);

            HomeAutomation.Util.executeFunctionByName(oCallbackDetails.namespace + "." + oCallbackDetails.func, window, oCallbackDetails.params);
          }

        }
      }

    })
  );
}
