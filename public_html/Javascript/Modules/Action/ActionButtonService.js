const namespace = 'HomeAutomation.Action.Button.Service';

export function getOne(oAuthData, iActionButtonId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/actions/buttons/${iActionButtonId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Action Button', xhrObj);
  });
}

export function getAll(oAuthData) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', '/api/actions/buttons/', oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return unpackResponse(response);

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Action Buttons', xhrObj);
  });
}

export function getAllByTopic(oAuthData, iTopicId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest("GET", {}, 'json', `/api/actions/buttons/bytopic/${iTopicId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return unpackResponse(response);

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Loading Action Buttons', xhrObj);
  });
}

function unpackResponse(response) {

  if (response._embedded && response._embedded.scenarios) {
    return response._embedded.scenarios;
  } else {
    return [];
  }
}

export function add(oAuthData, oActionButton) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oActionButton), 'json', '/api/actions/buttons/', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Adding Action Button', xhrObj);
  });
}

export function update(oAuthData, oActionButton) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oActionButton), 'json', `/api/actions/buttons/${oActionButton.id}`, oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Updating Action Button', xhrObj);
  });
}

export function remove(oAuthData, iActionButtonId) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, "json", `/api/actions/buttons/${iActionButtonId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Deleting Action Button', xhrObj);
  });
}

export function patch(oAuthData, iActionButton) {

  let oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, 'json', `/api/actions/buttons/${iActionButton}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage('Error Patching Action Button', xhrObj);
  });
}

function buildErrorMessage(sErrorMessage, oXhrObj) {

  let message = new HomeAutomation.Class.Message(sErrorMessage, oXhrObj.responseText);
  message.setType('ERR');

  return message;
}
