const namespace = 'HomeAutomation.Action';

var _LOADED_ACTION = null;

function setLoadedAction(action) {
  _LOADED_ACTION = action;
}

export function getLoadedAction() {
  return _LOADED_ACTION;
}

export function openAddSelectionModal() {
  let oData = {};
  oData.rootURL = HomeAutomation.Config.ROOT_URL;
  HomeAutomation.Util.executeHandlebars('modal-content', 'AddSelection', oData, 'clear');
}

export function displayList(oFilter, sListType, sSelectCallBack, oArgs) {

  if (oArgs) {
    var oFilter = oArgs[0];
    var sListType = oArgs[1];
    var sSelectCallBack = oArgs[2];
  }

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'displayList',
      params: [oFilter, sListType, sSelectCallBack]
    }
  };

  if (oFilter && !oFilter.getActive()) {
    oFilter = undefined;
  }

  HomeAutomation.Action.Service.getAll(oAuthData, oFilter).then(function(aActions) {

    if (!HomeAutomation.Util.validateWebserviceCall(aActions, 'actions-list-holder')) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aActions, new HomeAutomation.Class.Message('No Actions Found.'), 'actions-list-holder')) return;

    let oData = {};
    oData.actions = aActions;

    if(!sListType) {
      sListType = 'ActionList'
    }

    oData.filters = {};
    oData.filters.listType = sListType;
    if (oFilter) {
      oData.filters = Object.assign(oData.filters, oFilter.toJSON());
      oData.showFilters = true;
    }

    if(sSelectCallBack) {
      oData.filters.selectCallback = sSelectCallBack;
    }

    let sListHolder = 'actions-list-holder';
    if(sListType == 'ActionSearchList') {
      sListHolder = 'actions-search-list-holder';
    }

    HomeAutomation.Util.executeHandlebars(sListHolder, sListType, oData, 'clear');
  });

}

export function displayAddEditForm(iActionId, oArgs) {

  if (oArgs) {
    var iActionId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: "displayAddEditForm",
      params: [iActionId]
    }
  };

    if (iActionId) {
      parseAndDisplayEditModal(iActionId);

    } else {
      displayAddModal();
    }

  async function parseAndDisplayEditModal(iActionId) {

    // Get requested action
    HomeAutomation.Action.Service.getOne(oAuthData, iActionId).then(async function(oReadAction) {

      if (!validateWebserviceError(oReadAction)) return;

      console.log(oReadAction);
      let oAction = new HomeAutomation.Action.Class.Action(oReadAction);

      await oAction.parseActionAndGetControls();

      oAction.setViewModelType('edit');
      setLoadedAction(oAction);

      getLoadedAction().render();
    });

  }

  function displayAddModal(aControls) {

    let oAction = new HomeAutomation.Action.Class.Action();

    oAction.setViewModelType('add');
    setLoadedAction(oAction);

    getLoadedAction().render();
  }

}

export function onAddOrUpdate(iActionId, oArgs) {

  if (oArgs) {
    var iActionId = oArgs[0];
  }

  var oAuthData = {
    "check": true,
    "callback": {
      namespace: namespace,
      func: "onAddOrUpdate",
      params: [iActionId]
    }
  };

  let oLoadedAction = getLoadedAction();
  if (oLoadedAction == null) {
    return;
  }

  if (oLoadedAction.validateDataAndSetErrorModel()) {
    HomeAutomation.Util.addToast('error', 'Error', 'Failed to save action, invalid action.');
    return;
  }

  if (iActionId != undefined && iActionId != "") {

    HomeAutomation.Action.Service.update(oAuthData, oLoadedAction.toPostJSON()).then(function(oResult) {

      onAfterAddOrUpdate(oResult);
    });

  } else {

    HomeAutomation.Action.Service.add(oAuthData, oLoadedAction.toPostJSON()).then(function(oResult) {

      onAfterAddOrUpdate(oResult);
    });
  }

  async function onAfterAddOrUpdate(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
        getLoadedAction().setError(`${oResult.header} ${oResult.details}`);
        getLoadedAction().render();

        HomeAutomation.Util.addToast('error', 'Error', 'Failed to save action.');
        return;
    }

    HomeAutomation.Util.addToast('success', 'Success', 'Successfully saved action.');

    let oAction = new HomeAutomation.Action.Class.Action(oResult);

    await oAction.parseActionAndGetControls();

    oAction.setViewModelType('edit');
    setLoadedAction(oAction);

    getLoadedAction().render();
  }

}

export function onDeleteConfirmation(iActionId) {

  let oData = {
    header: "Confirm Deletion",
    message: "Are you sure you want to delete the action?",
    callback: "HomeAutomation.Action.onDelete(" + iActionId + ")"
  };

  HomeAutomation.Util.executeHandlebars("modal-content", "DeleteConfirmation", oData, "clear");
}

export function onDelete(iActionId, oArgs) {

  if (oArgs) {
    var iActionId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onDelete',
      params: [iActionId]
    }
  }

  HomeAutomation.Action.Service.remove(oAuthData, iActionId).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      oResult.displayModal();

    } else {
      HomeAutomation.Util.redirect(`${HomeAutomation.Config.ROOT_URL}/action/settings`, function() {

        displayList();
        HomeAutomation.Action.Button.displayList();
        setTimeout(function() {
          console.log("here");
          HomeAutomation.Util.addToast('success', 'Success', 'Successfully deleted action.');
        }, 2000);
      });
    }
  });

}

export function openTestActionModel(iActionId, oArgs) {

  if (oArgs) {
    var iActionId = oArgs[0];
  }

  let oAuthData = {
    'check': true,
    'callback': {
      namespace: namespace,
      func: 'openTestActionModel',
      params: [iActionId]
    }
  };

  let oTestModel = {
    log: new Array('Click the test button to run the test.')
  }

  // Get requested action
  HomeAutomation.Action.Service.getOne(oAuthData, iActionId).then(function(oReadAction) {

    if (!HomeAutomation.Util.validateWebserviceCall(oReadAction)) return;

    parseAction(oReadAction);
  });

  async function parseAction(oReadAction) {

    let oAction = new HomeAutomation.Action.Class.Action(oReadAction);

    await oAction.parseActionAndGetControls();

    setLoadedAction(oAction);

    let oData = oTestModel;
    oData.action = oAction;

    HomeAutomation.Util.executeHandlebars('modal-content', 'ActionTest', oData, 'clear');
  }

}

export function testAction() {

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'testAction',
      params: []
    }
  };

  let oAction = getLoadedAction();

  let oTestModel = {
    log: ['Running test...'],
    action: oAction,
    isError: 1
  }

  HomeAutomation.Util.executeHandlebars("modal-content", "ActionTest", oTestModel, "clear");


  HomeAutomation.Action.Service.test(oAuthData, oAction.id).then(function(oResult) {

    if (oResult instanceof HomeAutomation.Class.Message) {

      let sError = oResult.getDetails();

      let oTestModel = {
        log: ["Test failed: " + sError],
        action: oAction,
        isError: 0,
        results: {
          error: 1
        }
      }
      HomeAutomation.Util.executeHandlebars("modal-content", "ActionTest", oTestModel, "clear");

    } else {

      oTestModel = {
        log: oResult.log,
        action: oAction
      }

      // Set success conditions based on input
      oTestModel.results = getTestResultFlags(oResult);

      HomeAutomation.Util.executeHandlebars("modal-content", "ActionTest", oTestModel, "clear");
    }
  });

  function getTestResultFlags(oResponse) {

    let oResults = {
      error: 0,
      success: 0,
      ifCondition: 0,
    };

    if (oResponse.timeDuration) {
      oResults.timeDuration = oResponse.timeDuration / 1000000000;
    }


    if (oResponse.successFlag != undefined) {
      if (!oResponse.successFlag) {
        oResults.error = 1;
        return oResults;
      }
    }

    if (oResponse.conditionalEvaluation != undefined) {
      if (!oResponse.conditionalEvaluation) {
        oResults.ifCondition = 1;
      } else {
        oResults.success = 1;
      }
    }
    return oResults;
  }

}

export function getSelectedValue(element) {
  return element.options[element.selectedIndex].value;
}

export function getInputValue(element) {
  return element.value;
}

function validateWebserviceError(oResult, sErrorHolder) {

  if (oResult instanceof HomeAutomation.Class.Message) {
    oResult.setType('ERR');
    oResult.setAppend(false);
    if (sErrorHolder != undefined && sErrorHolder != '') {
      oResult.displayText(sErrorHolder);
    } else {
      oResult.displayModal();
    }

    return false;
  }
  return true;
}

export function showFilters(sListType, sSelectCallBack) {

  let oData = {
    showFilters: true
  }
  oData.filters = {};
  oData.filters.listType = sListType;
  oData.filters.selectCallback = sSelectCallBack;

  HomeAutomation.Util.executeHandlebars('actionFilters', 'ActionFilter', oData, 'clear');

}

export function hideFilters(sListType, sSelectCallBack) {

  let oData = {
    showFilters: false
  }
  oData.filters = {};
  oData.filters.selectCallback = sSelectCallBack;

  HomeAutomation.Util.executeHandlebars('actionFilters', 'ActionFilter', oData, 'clear');

  displayList(null, sListType, sSelectCallBack);
}

export function onSearch(sListType, sSelectCallBack) {

  let oFilter = getFilters();
  displayList(oFilter, sListType, sSelectCallBack);
}

function getFilters() {

  let eTitleFilter = document.getElementById('titleFilter');
  let eNameFilter = document.getElementById('nameFilter');
  let eUserCreatedFilter = document.getElementById('userCreatedFilter');

  let oFilter = new HomeAutomation.Class.Filter;

  if (!eTitleFilter || !eNameFilter || !eUserCreatedFilter) {
    oFilter.setActive(false);
    return oFilter;
  }

  let sTitleFilter = eTitleFilter.value;
  let sNameFilter = eNameFilter.value;
  let bUserCreatedFilter = 0;
  if(eUserCreatedFilter.checked) {
    bUserCreatedFilter = 1;
  }

  oFilter.addFilter('title', sTitleFilter);
  oFilter.addFilter('name', sNameFilter);
  oFilter.addFilter('user_created', bUserCreatedFilter);

  oFilter.setActive(true);

  return oFilter;
}

export function onOpenActionSearchModal(fCallbackSetAction) {

  let oModalDetails = {
    id: 'modal-custom',
    scrollable: true
  }

  let aParameters = [fCallbackSetAction];

  HomeAutomation.Util.openCustomModal(oModalDetails, HomeAutomation.Action.renderActionSearch, aParameters);
}

export function openViewActionSummaryModel(iActionId) {

  if(!iActionId) return;

  let oModalDetails = {
    id: 'modal-custom1',
    size: 'md',
    scrollable: true
  }

  HomeAutomation.Util.openCustomModal(oModalDetails, HomeAutomation.Action.renderViewActionSummary, [iActionId]);
}

export function renderActionSearch(sSelectCallBack) {

  let oData = {};
  oData.modalId = 'custom';
  oData.showFilters = true;
  oData.filters = {};
  oData.filters.listType = 'ActionSearchList';
  oData.filters.selectCallback = sSelectCallBack;

  HomeAutomation.Util.executeHandlebars(`modal-content-${oData.modalId}`, 'ActionSearch', oData, 'clear');
}

export function renderViewActionSummary(iActionId) {

  if(!iActionId) return;

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'renderViewActionSummary',
      params: [iActionId]
    }
  };

  HomeAutomation.Action.Service.getOne(oAuthData, iActionId).then(function(oAction) {

    if (!HomeAutomation.Util.validateWebserviceCall(oAction, 'modal-body')) return;

    displaySummary(oAction);

  });

  async function displaySummary(oReadAction) {

    let oAction = new HomeAutomation.Action.Class.Action(oReadAction);
    await oAction.parseActionAndGetControls();

    console.log(oAction);

    let oData = {};
    oData.action = oAction;
    oData.modalId = 'custom1';

    console.log(oData);

    HomeAutomation.Util.executeHandlebars(`modal-content-${oData.modalId}`, 'ActionSummary', oData, 'clear');
  }

}

export function selectAction(iActionId, sActionTitle, sActionName, sModelId, sSelectActionCallback) {

  if(sSelectActionCallback) {
    let aParts = sSelectActionCallback.split('.');
    if(aParts.length == 2) {
      HomeAutomation[aParts[0]][aParts[1]](iActionId, sActionTitle, sActionName);
    }
    if(aParts.length == 3) {
      HomeAutomation[aParts[0]][aParts[1]][aParts[2]](iActionId, sActionTitle, sActionName);
    }
  }

  HomeAutomation.Util.closeModal(sModelId);
}
