const namespace = 'HomeAutomation.Action.Service';

export function getOne(oAuthData, iActionId) {

  if(iActionId == null || iActionId == "") return;

  let oRequest = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', `/api/actions/${iActionId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage(xhrObj, "Error Loading Action");
  });
}

export function getAll(oAuthData, oFilter) {

  let sRequestURL = '/api/actions';

  if(oFilter) {
    if(oFilter.filterCount() > 0) {
        sRequestURL = `/api/actions?${oFilter.toURLRequest()}`;
    }
  }

  let request = new HomeAutomation.Class.WebserviceRequest('GET', {}, 'json', sRequestURL, oAuthData, true);
  return request.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return handleSuccessResponse(response);

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage(xhrObj, 'Error Loading Actions');
  });

  function handleSuccessResponse(response) {

    if (response._embedded && response._embedded.actions) {
      return response._embedded.actions;
    } else {
      return [];
    }
  }
}

export function add(oAuthData, oAction) {

  var oRequest = new HomeAutomation.Class.WebserviceRequest('POST', JSON.stringify(oAction), 'json', '/api/actions', oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage(xhrObj, 'Error Adding Action');
  });
}

export function update(oAuthData, oAction) {

  var oRequest = new HomeAutomation.Class.WebserviceRequest('PUT', JSON.stringify(oAction), 'json', `/api/actions/${oAction.id}`, oAuthData, false);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage(xhrObj, 'Error Updating Action');
  });
}

export function remove(oAuthData, iActionId) {

  var oRequest = new HomeAutomation.Class.WebserviceRequest('DELETE', {}, 'json', `/api/actions/${iActionId}`, oAuthData, true);

  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return true;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage(xhrObj, 'Error Deleting Action');
  });
}

export function test(oAuthData, iActionId) {

  var oRequest = new HomeAutomation.Class.WebserviceRequest('PATCH', {}, "json", `/api/actions/test/${iActionId}`, oAuthData, true);
  return oRequest.authenticateAndMakeRequest().then(function(response, statusText, xhrObj) {

    return response;

  }, function(xhrObj, textStatus, err) {

    return buildErrorMessage(xhrObj, 'Error Testing Action');
  });
}

function buildErrorMessage(oXhrObj, sErrorMessage) {

  let oMessage = new HomeAutomation.Class.Message(sErrorMessage, oXhrObj.responseText);
  oMessage.setType('ERR');

  return oMessage;
}
