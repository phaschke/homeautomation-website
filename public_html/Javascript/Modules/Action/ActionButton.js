const namespace = "HomeAutomation.Action.Button";

var _LOADED_ACTION_ = null;

function setLoadedActionButton(oEvent) {
  _LOADED_ACTION_ = oEvent;
}

export function getLoadedActionButton() {
  return _LOADED_ACTION_;
}

export function displayList() {

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'displayList',
      params: []
    }
  };

  HomeAutomation.Action.Button.Service.getAll(oAuthData).then(function(aActionButtons) {

    if (!HomeAutomation.Util.validateWebserviceCall(aActionButtons, "scenarios-list-holder")) return;
    if (!HomeAutomation.Util.validateListLengthGreaterThanZero(aActionButtons, new HomeAutomation.Class.Message("No Action Buttons Found."), "scenarios-list-holder")) return;

    let oData = {};
    oData.actionbuttons = aActionButtons;

    HomeAutomation.Util.executeHandlebars("scenarios-list-holder", "ActionButtonList", oData, "clear");
  });

}

export function displayByTopic(oArgs) {

  // oArg[0] iTopicId
  // oArg[1] oCallback (optional)

  let iTopicId = oArgs[0];

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: "displayByTopic",
      params: [oArgs[0]]
    }
  };

  if(oArgs[1]) {
    oAuthData.callback = oArgs[1];
  }

  return new Promise ((resolve, reject) => {

    HomeAutomation.Action.Button.Service.getAllByTopic(oAuthData, iTopicId).then(function(aActionButtons) {

      if (!HomeAutomation.Util.validateWebserviceCall(aActionButtons, "action-button-holder")) {
        return reject();
      }

      let oData = {};
      oData.scenarios = aActionButtons;

      HomeAutomation.Util.executeHandlebars("action-button-holder", "ActionButtons", oData, "clear");

      return resolve();
    });
  });
}

export function onActionButtonPress(iActionButtonId, oArgs) {

  if (oArgs) {
    var iActionButtonId = args[0];
  }

  var oAuthData = {
    "check": true,
    "callback": {
      namespace: namespace,
      func: "onActionButtonPress",
      params: [iActionButtonId]
    }
  };

  HomeAutomation.Action.Button.Service.patch(oAuthData, iActionButtonId).then(function(oResult) {

    // Show error modal when
    if (!HomeAutomation.Util.validateWebserviceCall(oResult)) return;

    if(!oResult.successFlag) {
      let message = new HomeAutomation.Class.Message("Action Error", "The action failed to execute.");
      message.setType("ERR");
      message.displayModal();
    }

    return;
  });

}

export function openAddEditModal(iActionButtonId, oArgs) {

  if (oArgs) {
    var iActionButtonId = args[0];
  }

  let oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'openAddEditModal',
      params: [iActionButtonId]
    }
  };

  let sActionSetCallback = 'Action.Button.onSetAction';

  HomeAutomation.Topic.Service.getAll(oAuthData).then(function(aTopics) {
    if (!HomeAutomation.Util.validateWebserviceCall(aTopics)) return;

    if (iActionButtonId) {
      HomeAutomation.Action.Button.Service.getOne(oAuthData, iActionButtonId).then(function(oActionButton) {

        if (!HomeAutomation.Util.validateWebserviceCall(oActionButton)) return;
        parseAndDisplayEditModal(oActionButton, aTopics);
      });

    } else {
      displayAddModal(aTopics);
    }
  });

  function parseAndDisplayEditModal(oLoadedActionButton, aTopics) {

    let oActionButton = new HomeAutomation.Action.Button.Class.ActionButton(oLoadedActionButton);

    HomeAutomation.Action.Service.getOne(oAuthData, oActionButton.actionId).then(function(aAction) {

      if (!HomeAutomation.Util.validateWebserviceCall(aAction)) return;

      oActionButton.setEdit(true);
      oActionButton.setTopics(aTopics);
      oActionButton.view.actionSelectCallback = sActionSetCallback;
      oActionButton.setAction(aAction);

      setLoadedActionButton(oActionButton);
      oActionButton.render();
    });
  }

  function displayAddModal(aTopics) {

    let oActionButton = new HomeAutomation.Action.Button.Class.ActionButton();

    oActionButton.setEdit(false);
    oActionButton.setTopics(aTopics);
    oActionButton.view.actionSelectCallback = sActionSetCallback;

    setLoadedActionButton(oActionButton);
    oActionButton.render();
  }
}

export function getSelectedValue(eElement) {
  return eElement.options[eElement.selectedIndex].value;
}

export function getInputValue(eElement) {
  return eElement.value;
}

export function onAddOrUpdate(iActionButtonId, oArgs) {

  if (oArgs) {
    var iActionButtonId = oArgs[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'addOrUpdate',
      params: [iActionButtonId]
    }
  };

  let oLoadedActionButton = getLoadedActionButton();
  if (oLoadedActionButton.validate()) {
    return;
  }
  let oActionButton = oLoadedActionButton.toPostJSON();

  if(iActionButtonId) {

    HomeAutomation.Action.Button.Service.update(oAuthData, oActionButton).then(function(oResult) {
      updateAndShowList(oResult, oLoadedActionButton);
    });

  } else {

    HomeAutomation.Action.Button.Service.add(oAuthData, oActionButton).then(function(oResult) {
      updateAndShowList(oResult, oLoadedActionButton);
    });
  }

  function updateAndShowList(oResult, oLoadedActionButton) {

    if (oResult instanceof HomeAutomation.Class.Message) {
      if (oResult.type == 'ERR') {
        oLoadedActionButton.addError('formError', `${oResult.header} ${oResult.details}`);
        oLoadedActionButton.render();
        return;
      }
    }
    displayList();
    HomeAutomation.Util.closeModal();
  }

}

export function onDeleteConfirmation(iActionButtonId) {

  var oData = {
    header: 'Confirm Deletion',
    message: 'Are you sure you want to delete the action button?',
    callback: `HomeAutomation.Action.Button.onDelete(${iActionButtonId})`
  };
  HomeAutomation.Util.executeHandlebars('modal-content', 'DeleteConfirmation', oData, 'clear');
}

export function onDelete(iActionButtonId, oArgs) {

  if (oArgs) {
    var iActionButtonId = args[0];
  }

  var oAuthData = {
    check: true,
    callback: {
      namespace: namespace,
      func: 'onDelete',
      params: [iActionButtonId]
    }
  };

  HomeAutomation.Action.Button.Service.remove(oAuthData, iActionButtonId).then(function(bResult) {

    if (!HomeAutomation.Util.validateWebserviceCall(bResult)) return;

    displayList();
    HomeAutomation.Util.closeModal();
  });
}

export function onActionSelect() {

  let eSelect = document.getElementById('actionSelect');

  if(eSelect.value == '') {
    document.getElementById('actionViewBtn').disabled = true;

  } else {
    document.getElementById('actionViewBtn').disabled = false;
  }
}

export function onSetAction(iActionId, sActionTitle, sActionName) {

  let oLoadedActionButton = getLoadedActionButton();
  oLoadedActionButton.onActionSelect(iActionId);

  if(!oLoadedActionButton.view.action) {
    oLoadedActionButton.view.action = {
      title: '',
      name: ''
    }
  }
  oLoadedActionButton.view.action.title = sActionTitle;
  oLoadedActionButton.view.action.name = sActionName;

  setLoadedActionButton(oLoadedActionButton);

  oLoadedActionButton.render();
}
