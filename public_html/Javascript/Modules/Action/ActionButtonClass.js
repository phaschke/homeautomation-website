export class ActionButton {

  constructor(oActionButtonJSON) {
    if (oActionButtonJSON === undefined) {
      var oActionButtonJSON = {};
    }
    this.id = oActionButtonJSON.id === undefined ? "" : oActionButtonJSON.id;
    this.title = oActionButtonJSON.title === undefined ? "" : oActionButtonJSON.title;
    this.topicId = oActionButtonJSON.topicId === undefined ? "" : oActionButtonJSON.topicId;
    this.actionId = oActionButtonJSON.actionId === undefined ? "" : oActionButtonJSON.actionId;
    this.description = oActionButtonJSON.description === undefined ? "" : oActionButtonJSON.description;
    this.icon = oActionButtonJSON.icon === undefined ? "" : oActionButtonJSON.icon;

    this.errors = {};

    this.view = {};
    this.view.edit = false;
    this.view.topics = [];
    this.view.action = null;
    this.view.actionSelectCallback = '';
    this.view.namespace = 'HomeAutomation.Action.Button';
    this.view.binding = 'HomeAutomation.Action.Button.getLoadedActionButton()';

  }

  setEdit(bEdit) {
    this.view.edit = bEdit;
  }

  setTopics(aTopics) {
    this.view.topics = aTopics;
  }

  setAction(oAction) {
    this.view.action = oAction;
  }

  onTitleInput(sTitle) {
    this.title = sTitle;
  }

  onTopicSelect(iTopicId) {
    this.topicId = iTopicId;
  }

  onActionSelect(iActionId) {
    this.actionId = iActionId;
  }

  onIconSelect(sIcon) {
    this.icon = sIcon;
    this.render();
  }

  onDescriptionInput(sDescription) {
    this.sDescription = sDescription;
  }

  clearErrors() {
    this.errors = {};
  }

  addError(sError, sErrorText) {
    this.errors[sError] = sErrorText;
  }

  removeError(sError) {
    delete this.errors[sError];
  }

  validate() {

    let oErrorModel = {};
    let bIsError = false;
    this.clearErrors();
    let sRequiredMessage = 'This field is required.';

    if (!this.validateRequiredVariable('title')) {
      this.addError('title', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('topicId')) {
      this.addError('topic', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('actionId')) {
      this.addError('action', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    if (!this.validateRequiredVariable('icon')) {
      this.addError('icon', this.createRequiredModel(true, sRequiredMessage));
      bIsError = true;
    }

    this.render();
    return bIsError;
  }

  validateRequiredVariable(sVariable) {
    if (this[sVariable] != "" && this[sVariable] != undefined) {
      return true;
    }
    return false;
  }

  createRequiredModel(bShowMessage, sMessage) {
    let oErrorSegment = {
      errClass: 'is-invalid'
    }
    if (bShowMessage) {
      oErrorSegment.errMsg = sMessage;
    }
    return oErrorSegment;
  }

  toPostJSON() {
    return {
      id: this.id,
      title: this.title,
      topicId: this.topicId,
      actionId: this.actionId,
      icon: this.icon,
      description: this.description
    }

  }

  toViewJSON() {
    return {
      id: this.id,
      title: this.title,
      topicId: this.topicId,
      actionId: this.actionId,
      icon: this.icon,
      description: this.description,
      view: this.view,
      errors: this.errors
    }
  }

  render() {

    HomeAutomation.Util.executeHandlebars('modal-content', 'ActionButtonAddEdit', this.toViewJSON(), "clear");
  }

  }
