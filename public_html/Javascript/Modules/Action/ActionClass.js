export class Action {

  constructor(oActionJSON) {

    if (oActionJSON === undefined) {
      var oActionJSON = {};
    }
    this.id = oActionJSON.id === undefined ? '' : oActionJSON.id;
    this.name = oActionJSON.name === undefined ? '' : oActionJSON.name;
    this.title = oActionJSON.title === undefined ? '' : oActionJSON.title;
    this.description = oActionJSON.description === undefined ? '' : oActionJSON.description;
    this.error = '';
    this.ifs = oActionJSON.ifs === undefined ? '' : oActionJSON.ifs;
    this.parsedIfs = new Array();
    this.ifIterator = 0;
    this.thens = oActionJSON.thens === undefined ? '' : oActionJSON.thens;
    this.parsedThens = new Array();
    this.thenIterator = 0;
    this.elses = oActionJSON.elses === undefined ? '' : oActionJSON.elses;
    this.parsedElses = new Array();
    this.elseIterator = 0;
    this.ifExecution = oActionJSON.ifExecution === undefined ? 'SERIAL' : oActionJSON.ifExecution;
    this.thenExecution = oActionJSON.thenExecution === undefined ? 'SERIAL' : oActionJSON.thenExecution;
    this.elseExecution = oActionJSON.elseExecution === undefined ? 'SERIAL' : oActionJSON.elseExecution;

    this.view = {};
    this.view.ifReorderMode = false;
    this.view.thenReorderMode = false;
    this.view.elseReorderMode = false;
    this.namespace = 'HomeAutomation.Action';
    this.binding = 'HomeAutomation.Action.getLoadedAction()';
    this.errors = {};

    this.associatedControls = new Map();

    this.controls = new Array();

  }

  setError(sError) {
    this.error = sError;
  }

  setControls(aControls) {
    this.controls = aControls;
  }

  setViewModelType(sType) {
    this.view.modalType = sType;
  }

  onTitleInput(sValue) {
    this.title = sValue;
  }

  onNameInput(sValue) {
    this.name = sValue;
  }

  onDescriptionInput(sValue) {
    this.description = sValue;
  }

  onIfExecutionSelect(sValue) {
    this.ifExecution = sValue;
  }

  onThenExecutionSelect(sValue) {
    this.thenExecution = sValue;
  }

  onElseExecutionSelect(sValue) {
    this.elseExecution = sValue;
  }

  generateIfSegmentId() {
    let retValue = this.ifIterator;
    this.ifIterator = this.ifIterator + 1;
    return retValue;
  }

  generateTaskSegmentId(sType) {

    let iRetValue = 0;

    if(sType === 'then') {
      iRetValue = this.thenIterator;
      this.thenIterator = this.thenIterator + 1;
    }

    if(sType === 'else') {
      iRetValue = this.elseIterator;
      this.elseIterator = this.elseIterator + 1;
    }

    return iRetValue;
  }

  async parseActionAndGetControls() {

    let oIfJSON = {};
    let oThenJSON = {};
    let oElseJSON = {};

    try {
      oIfJSON = JSON.parse(this.ifs);

    } catch (err) {
      this.error = 'Failed to parse if segments.'
      this.render;
      return;
    }

    try {
      oThenJSON = JSON.parse(this.thens);

    } catch (err) {
      this.error = 'Failed to parse then segments.'
      this.render;
      return;
    }

    try {
      oElseJSON = JSON.parse(this.elses);

    } catch (err) {
      this.error = 'Failed to parse else segments.'
      this.render;
      return;
    }

    await this.loadControls(oIfJSON, oThenJSON, oElseJSON);
  }

  async loadControls(oIfJSON, oThenJSON, oElseJSON) {

    let associatedControls = this.associatedControls;
    let context = this;

    return new Promise(async (resolve, reject) => {

      oIfJSON.forEach((oIf, i) => {
        associatedControls.set(parseInt(oIf.objectId), {});
      });

      oThenJSON.forEach((oThen, i) => {
        associatedControls.set(parseInt(oThen.objectId), {});
      });

      oElseJSON.forEach((oElse, i) => {
        associatedControls.set(parseInt(oElse.objectId), {});
      });

      let oAuthData = {
        check: true,
        callback: {}
      };

      const controls = (iKey) => {

        return new Promise(resolve => {
          HomeAutomation.Control.Service.getOne(oAuthData, iKey).then(function(oControl) {
            if (oControl instanceof HomeAutomation.Class.Message) {
              associatedControls.delete(iKey);
            } else {
              associatedControls.set(iKey, oControl);
            }

            resolve();
          }, iKey);
        });
      }

      Promise.all(
          Array.from(associatedControls.keys()).map(c => controls(parseInt(c)))
        )
        .then(() => {

          if(oIfJSON != null) {
            context.parseIfEntries(oIfJSON);
          }
          if(oThenJSON != null) {
            context.parseTaskEntries(oThenJSON, 'then');
          }
          if(oElseJSON != null) {
            context.parseTaskEntries(oElseJSON, 'else');
          }

          resolve();
        });
    });
  }

  parseIfEntries(oIfJSON) {

    oIfJSON.forEach((oIf, i) => {

      let oIfEntry = this.generateIf();

      let iObjectId = parseInt(oIf.objectId);
      if (!this.associatedControls.has(iObjectId)) {
        oIfEntry.error = 'Failed to find segment\'s control, has the control been deleted?';
        this.parsedIfs.push(oIfEntry);
        return;
      }
      let oControl = new HomeAutomation.Control.Class.Control(this.associatedControls.get(iObjectId));
      oIfEntry.control = oControl;

      let aActions = this.parseIfActions(oControl);
      if (typeof aActions === 'string' || aActions instanceof String) {
        oIfEntry.error = aActions;
        this.parsedIfs.push(oIfEntry);
        return;
      }

      let oAction = this.validateActionInControl(aActions, oIf.conditional);
      if (typeof oAction === 'boolean') {
        oIfEntry.error = 'Failed to find segment\'s action, has the control type been changed?';
        this.parsedIfs.push(oIfEntry);
        return;
      }

      oIfEntry.objectType = oIf.objectType;
      oIfEntry.conditional = oIf.conditional;
      oIfEntry.view.actions = aActions;
      oIfEntry.view.actionEnabled = 1;

      oIfEntry.view.parameters = oAction.parameterKeys;
      oIfEntry.view.parameterEnabled = 1;

      if (!this.validateParameterInAction(oAction.parameterKeys, oIf.parameter)) {
        oIfEntry.error = 'Failed to find segment\'s parameter, has the control type been changed?';
        this.parsedIfs.push(oIfEntry);
        return;
      }
      oIfEntry.parameter = oIf.parameter;

      if (oIf.comparator) {
        oIfEntry.comparator = oIf.comparator;
      }

      if (oIf.value) {
        oIfEntry.value = oIf.value;
      }

      this.parsedIfs.push(oIfEntry);
    });

  }

  parseTaskEntries(oTaskJSON, sTaskType) {

    let oParsedTasks = this.parsedElses;
    if(sTaskType === 'then') {
      oParsedTasks = this.parsedThens;
    }

    oTaskJSON.forEach((oTask, i) => {

      let oTaskEntry = this.generateTask(sTaskType);

      let iObjectId = parseInt(oTask.objectId);
      if (!this.associatedControls.has(iObjectId)) {
        oTaskEntry.error = 'Failed to find segment\'s control, has the control been deleted?';
        oParsedTasks.push(oTaskEntry);
        return;
      }
      let oControl = new HomeAutomation.Control.Class.Control(this.associatedControls.get(iObjectId));
      oTaskEntry.control = oControl;

      oTaskEntry.objectType = oTask.objectType;

      let aActions = this.parseTaskActions(oControl);
      if (typeof aActions === 'string' || aActions instanceof String) {
        oTaskEntry.error = aActions;
        oParsedTasks.push(oTaskEntry);
        return;
      }

      let oAction = this.validateActionInControl(aActions, oTask.action);
      if (typeof oAction === 'boolean') {
        oTaskEntry.error = 'Failed to find segment\'s action, has the control type been changed?';
        oParsedTasks.push(oTaskEntry);
        return;
      }
      oTaskEntry.action = oTask.action;
      oTaskEntry.view.actions = aActions;
      oTaskEntry.view.actionEnabled = 1;

      // Parse parameter definition
      oTaskEntry.parameterKeys = oAction.parameterKeys;
      if(oAction.parameterKeys != null && oAction.parameterKeys.length > 1) {
        oTaskEntry.view.multipleParameters = true;
      }
      if(oAction.parameterKeys == null || oAction.parameterKeys.length == 0) {
        oTaskEntry.noParameters = true;
      } else {
        oTaskEntry.noParameters = false;
      }

      // Parse parameters
      let aParameters = new Array();

      for (var i = 0; i < oTask.parameters.length; i++) {
        aParameters.push({
          id: i,
          key: oTask.parameters[i].key,
          value: oTask.parameters[i].value
        });
      }

      oTaskEntry.parameterRequired = oAction.parameterRequired;
      if(oAction.parameterRequired && aParameters.length === 0) {
        aParameters.push({
          id: 0,
          key: '',
          value: ''
        });
      }
      oTaskEntry.parameters = aParameters;

      oParsedTasks.push(oTaskEntry);
    });

  }

  validateActionInControl(aActions, sAction) {

    if (aActions[sAction] != undefined) {
      return aActions[sAction];
    }
    return false;
  }

  validateParameterInAction(aParameters, sParameter) {
    for (let i = 0; i < aParameters.length; i++) {
      if (aParameters[i] == sParameter) return true;
    }
    return false;
  }

  findSectionIndexById(iSectionId, aSections) {

    for (var i = 0; i < aSections.length; i++) {
      if (aSections[i].id == iSectionId) {
        return i;
      }
    }
    // id does not exist in array
    return -1;
  }

  findControlById(iControlId) {
    for (let i = 0; i < this.controls.length; i++) {
      if (this.controls[i].id == iControlId) {
        return this.controls[i];
      }
    }
    return undefined;
  }

  generateIf(iId) {
    if (iId === undefined) {
      iId = this.generateIfSegmentId();
    }

    return {
      id: iId,
      control: {},
      objectId: null,
      objectType: null,
      action: '',
      parameter: '',
      comparator: '',
      value: '',
      error: null,
      view: {
        parameterEnabled: 0,
        actionEnabled: 0,
        actions: {},
        parameters: []
      }
    }
  }

  openAddIfSelect() {

    let oData = {
      binding: this.binding
    }

    HomeAutomation.Util.openModal();
    HomeAutomation.Util.executeHandlebars('modal-content', 'ActionIfTypeSelect', oData, 'clear');
  }

  openIfObjectTypeSelect(sObjectType) {

    if(sObjectType === 'control') {

      HomeAutomation.Control.Search.openSearchModal(this.addIf, this);

    } else {
      HomeAutomation.Util.closeModal();
    }

  }

  addIf(oControl) {

    let ifJSON = this.generateIf();
    ifJSON.objectType = 'CONTROL'; // TODO: Update this in the case there are multiple object types
    let oControlClass = new HomeAutomation.Control.Class.Control(oControl);
    ifJSON.control = oControlClass;

    // parse and update action field
    let aActions = this.parseIfActions(oControlClass);
    if (typeof aActions === 'string' || aActions instanceof String) {
      ifJSON.error = aActions;
      this.parsedIfs.push(ifJSON);
      this.render();
      return;
    }

    ifJSON.view.actions = aActions;
    ifJSON.view.actionEnabled = 1;

    this.parsedIfs.push(ifJSON);

    this.view.ifReorderMode = false;
    this.render();
  }

  removeIf(iSectionId) {
    let iIndex = this.findSectionIndexById(iSectionId, this.parsedIfs);
    if (iIndex >= 0) {
      this.parsedIfs.splice(iIndex, 1);
    }
    this.render();
  }

  parseIfActions(oControl) {

    try {
      let aIfActions = oControl.actionPropertiesJSON.conditional;
      if (aIfActions == undefined) throw new Exception();

      return aIfActions;

    } catch (error) {
      return 'Failed to parse control if actions.';
    }
  }

  onIfConditionalSelect(iIfId, sConditional) {

    let iIndex = this.findSectionIndexById(iIfId, this.parsedIfs);
    if (iIndex < 0) {
      return;
    }

    // Clear fields after action selection
    this.parsedIfs[iIndex].conditional = '';
    this.parsedIfs[iIndex].parameter = '';
    this.parsedIfs[iIndex].view.parameterEnabled = 0;
    this.parsedIfs[iIndex].view.parameters = new Array();
    this.parsedIfs[iIndex].comparator = '';
    this.parsedIfs[iIndex].value = '';
    this.parsedIfs[iIndex].error = null;

    if (sConditional === '') {
      this.render();
      return;
    }

    let aActions = this.parsedIfs[iIndex].view.actions;
    this.parsedIfs[iIndex].conditional = sConditional;

    let oAction = aActions[sConditional];
    if (oAction.parameterRequired) {

      let aParameters = oAction.parameterKeys;
      if (aParameters.length <= 0) {
        this.parsedIfs[iIndex].error = 'Parameter is required, but control does not have any.';
      } else {
        this.parsedIfs[iIndex].view.parameterEnabled = 1;
        this.parsedIfs[iIndex].view.parameters = aParameters;
      }
    }

    this.render();
  }

  onIfParameterSelect(iIfId, sParameter) {

    let iIndex = this.findSectionIndexById(iIfId, this.parsedIfs);
    if (iIndex < 0) {
      return;
    }

    // Clear fields after parameter selection
    this.parsedIfs[iIndex].parameter = '';
    this.parsedIfs[iIndex].error = null;

    if (sParameter === '') {
      this.render();
      return;
    }

    this.parsedIfs[iIndex].parameter = sParameter;
    this.render();

  }

  onIfComparatorSelect(iIfId, sComp) {

    let iIndex = this.findSectionIndexById(iIfId, this.parsedIfs);
    if (iIndex < 0) {
      return;
    }

    this.parsedIfs[iIndex].comparator = "";
    this.parsedIfs[iIndex].error = null;

    if (sComp == "") {
      this.render();
      return;
    }

    this.parsedIfs[iIndex].comparator = sComp;
    this.render();
  }

  onIfValueChange(iIfId, sValue) {

    let iIndex = this.findSectionIndexById(iIfId, this.parsedIfs);
    if (iIndex < 0) {
      return;
    }
    this.parsedIfs[iIndex].error = null;

    this.parsedIfs[iIndex].value = sValue;
  }

  generateTask(sType, iId) {

    if (iId == undefined) {
      iId = this.generateTaskSegmentId(sType);
    }
    return {
      id: iId,
      control: {},
      objectId: null,
      objectType: null,
      action: '',
      parameters: new Array(),
      parameterRequired: '',
      value: '',
      error: null,
      view: {
        actionEnabled: 0,
        actions: {}
      }
    };
  }

  openAddTaskSelect(sType) {

    let oData = {
      binding: this.binding,
      type: sType
    }

    HomeAutomation.Util.openModal();
    HomeAutomation.Util.executeHandlebars('modal-content', 'ActionTaskTypeSelect', oData, 'clear');
  }

  openTaskObjectTypeSelect(sType, sObjectType) {

    if(sObjectType === 'control') {

      if(sType === 'then') {
        HomeAutomation.Control.Search.openSearchModal(this.addControlThenTask, this);
      }
      if(sType === 'else') {
        HomeAutomation.Control.Search.openSearchModal(this.addControlElseTask, this);
      }

    } else if(sObjectType === 'event') {

      // TODO: Implement task object type

    } else if(sObjectType === 'notification') {

      // TODO: Implement task object type

    } else {
      HomeAutomation.Util.closeModal();
    }
  }

  addControlThenTask(oControl) {
    this.addControlTask(oControl, 'then');
  }

  addControlElseTask(oControl) {
    this.addControlTask(oControl, 'else');
  }

  addControlTask(oControl, sType) {

    let taskJSON = this.generateTask(sType);
    taskJSON.objectType = 'CONTROL';
    let oControlClass = new HomeAutomation.Control.Class.Control(oControl);
    taskJSON.control = oControlClass;

    let aActions = this.parseTaskActions(oControlClass);

    if (typeof aActions === 'string' || aActions instanceof String) {
      taskJSON.error = aActions;

    } else {
      taskJSON.view.actions = aActions;
      taskJSON.view.actionEnabled = 1;
    }

    if(sType === 'then') {
      this.parsedThens.push(taskJSON);
    }
    if(sType === 'else') {
      this.parsedElses.push(taskJSON);
    }

    taskJSON.noParameters = true;

    this.view[`${sType}ReorderMode`] = false;
    this.render();
  }

  removeTask(iSectionId, sTaskType) {

    if(sTaskType === 'then') {
      let iIndex = this.findSectionIndexById(iSectionId, this.parsedThens);
      if (iIndex >= 0) {
        this.parsedThens.splice(iIndex, 1);
      }
    }
    if(sTaskType === 'else') {
      let iIndex = this.findSectionIndexById(iSectionId, this.parsedElses);
      if (iIndex >= 0) {
        this.parsedElses.splice(iIndex, 1);
      }
    }

    this.render();
  }

  parseTaskActions(oControl) {
    try {

      let aTaskActions = oControl.actionPropertiesJSON.task;
      if (aTaskActions == undefined) throw new Exception();

      return aTaskActions;

    } catch (error) {
      return 'Failed to parse control task actions.';
    }
  }

  onTaskActionSelect(iTaskId, sTaskType, sAction) {

    let aParsedTasks = this.getParsedTasks(sTaskType);

    let iIndex = this.findSectionIndexById(iTaskId, aParsedTasks);
    if (iIndex < 0) {
      return;
    }

    let oTaskEntry = aParsedTasks[iIndex];

    // Clear fields after action selection
    oTaskEntry.action = '';
    oTaskEntry.parameter = '';
    oTaskEntry.value = '';
    oTaskEntry.error = null;
    oTaskEntry.noParameters = true;

    if (sAction === '') {
      this.render();
      return;
    }

    let aActions = aParsedTasks[iIndex].view.actions;
    oTaskEntry.action = sAction;

    let oAction = aActions[sAction];

    // Add parameter definition
    oTaskEntry.parameterKeys = oAction.parameterKeys;
    if(oAction.parameterKeys != null && oAction.parameterKeys.length > 1) {
      oTaskEntry.view.multipleParameters = true;
    }
    if(oAction.parameterKeys == null || oAction.parameterKeys.length == 0) {
      oTaskEntry.noParameters = true;
    } else {
      oTaskEntry.noParameters = false;
    }

    // Add required parameter
    oTaskEntry.parameterRequired = oAction.parameterRequired;
    let aParameters = new Array();
    if(oAction.parameterRequired) {
      aParameters.push({
        id: 0,
        key: '',
        value: ''
      });
    }

    oTaskEntry.parameters = aParameters;

    console.log(oTaskEntry);

    this.render();
  }

  onDeleteTaskParameter(iTaskId, sTaskType, iParameterId) {

    let aParsedTasks = this.getParsedTasks(sTaskType);

    let iIndex = this.findSectionIndexById(iTaskId, aParsedTasks);
    if (iIndex < 0) {
      return;
    }

    let aParameters = aParsedTasks[iIndex].parameters;

    for(var i = 0; i < aParameters.length; i++) {
      if(aParameters[i].id === iParameterId) {
        aParameters.splice(i, 1);
      }
    }

    this.render();
  }

  onAddTaskParameter(iTaskId, sTaskType) {

    let aParsedTasks = this.getParsedTasks(sTaskType);

    let iIndex = this.findSectionIndexById(iTaskId, aParsedTasks);
    if (iIndex < 0) {
      return;
    }

    let aParameters = aParsedTasks[iIndex].parameters;

    let iLastId = 0;
    if(aParameters.length > 0) {
      iLastId = aParameters[aParameters.length-1].id + 1;
    }

    aParameters.push({
      id: iLastId,
      key: '',
      value: ''
    });

    this.render();
  }

  onTaskParameterKeySelect(iTaskId, sTaskType, iParameterId, sParameter) {

    let aParsedTasks = this.getParsedTasks(sTaskType);

    let iIndex = this.findSectionIndexById(iTaskId, aParsedTasks);
    if (iIndex < 0) {
      return;
    }
    let sAction = aParsedTasks[iIndex].action;
    let aParameters = aParsedTasks[iIndex].parameters;

    for (var i = 0; i < aParameters.length; i++) {
      var oParameter = aParameters[i];
      if (oParameter.id == iParameterId) {
        oParameter.key = sParameter;
      }
    }

    this.render();
  }

  onTaskParameterValueChange(iTaskId, sTaskType, iParameterId, sValue) {

    let aParsedTasks = this.getParsedTasks(sTaskType);

    let iIndex = this.findSectionIndexById(iTaskId, aParsedTasks);
    if (iIndex < 0) {
      return;
    }

    let sAction = aParsedTasks[iIndex].action;
    let aParameters = aParsedTasks[iIndex].parameters;

    for (var i = 0; i < aParameters.length; i++) {
      var oParameter = aParameters[i];
      if (oParameter.id == iParameterId) {
        oParameter.value = sValue;
      }
    }
  }

  getParsedTasks(sTaskType) {
    let aParsedTasks = null;
    if(sTaskType === 'then') {
      aParsedTasks = this.parsedThens;
    }
    if(sTaskType === 'else') {
      aParsedTasks = this.parsedElses;
    }

    return aParsedTasks;
  }

  onActivateReorderMode(sMode) {

    let sReorderMode = `${sMode}ReorderMode`;
    let sReorderList = `${sMode}ReorderList`;

    this.view[sReorderMode] = true;

    this.render();

    let eSortableList = document.getElementById(sReorderList);
    Sortable.create(eSortableList, {
      animation: 150,
      ghostClass: 'ghostClass'
    });

  }

  onDeactiveReorderMode(sMode) {

    let sReorderList = `${sMode}ReorderList`;
    let aListChildren = document.getElementById(sReorderList).children;

    let aParsedSegments = null;
    let iSegmentIterator = 0;
    if(sMode === 'if') {
      aParsedSegments = this.parsedIfs;
    }
    if(sMode === 'then') {
      aParsedSegments = this.parsedThens;
    }
    if(sMode === 'else') {
      aParsedSegments = this.parsedElses;
    }

    let aUpdatedParsedSegments = new Array();

    let iCounter = 0;
    Array.from(aListChildren).forEach(li => {

      let iSegmentId = li.id.split('-')[1];
      let iIndex = this.findSectionIndexById(iSegmentId, aParsedSegments);
      if (iIndex < 0) {
        return;
      }
      var oSegment = aParsedSegments[iIndex];

      oSegment.id = iCounter;
      aUpdatedParsedSegments.push(oSegment);
      iCounter++;
    });

    if(sMode === 'if') {
      this.parsedIfs = aUpdatedParsedSegments;
      this.ifIterator = iCounter;
    }
    if(sMode === 'then') {
      this.parsedThens = aUpdatedParsedSegments;
      this.thenIterator = iCounter;
    }
    if(sMode === 'else') {
      this.parsedElses = aUpdatedParsedSegments;
      this.elseIterator = iCounter;
    }

    let sReorderMode = `${sMode}ReorderMode`;
    this.view[sReorderMode] = false;
    this.render();
  }

  toViewJSON() {

    let oActionJSON = {
      id: this.id,
      name: this.name,
      title: this.title,
      description: this.description,
      ifs: this.parsedIfs,
      thens: this.parsedThens,
      elses: this.parsedElses,
      ifExecution: this.ifExecution,
      thenExecution: this.thenExecution,
      elseExecution: this.elseExecution,
      error: this.error,
      view: this.view,
      controls: this.controls,
      namespace: this.namespace,
      binding: this.binding,
      errors: this.errors,
      rootURL: HomeAutomation.ROOT_URL
    }

    return oActionJSON;
  }

  toPostJSON() {

    var oAction = {
      name: this.name,
      title: this.title,
      description: this.description,
      ifExecution: this.ifExecution,
      thenExecution: this.thenExecution,
      elseExecution: this.elseExecution,
      ifs: [],
      thens: [],
      elses: []
    };

    this.parsedIfs.forEach((oIf, i) => {
      oAction.ifs.push({
        objectId: oIf.control.id,
        objectType: oIf.objectType,
        conditional: oIf.conditional,
        parameter: oIf.parameter,
        comparator: oIf.comparator,
        value: oIf.value,
        order: i
      });
    });
    oAction.ifs = JSON.stringify(oAction.ifs);

    this.parsedThens.forEach((oThen, i) => {
      oAction.thens.push({
        objectId: oThen.control.id,
        objectType: oThen.objectType,
        parameters: oThen.parameters,
        action: oThen.action,
        order: i
      });
    });
    oAction.thens = JSON.stringify(oAction.thens);

    this.parsedElses.forEach((oElse, i) => {
      oAction.elses.push({
        objectId: oElse.control.id,
        objectType: oElse.objectType,
        parameters: oElse.parameters,
        action: oElse.action,
        order: i
      });
    });
    oAction.elses = JSON.stringify(oAction.elses);

    if (this.id && this.id != '') {
      oAction.id = this.id;
    }

    return oAction;
  }

  render() {

    HomeAutomation.Util.executeHandlebars('action-form-holder', 'ActionAddEdit', this.toViewJSON(), 'clear');
  }

  validateDataAndSetErrorModel() {

    let oErrorModel = {};
    let bIsError = false;
    this.error = '';

    if (!this.validateRequiredActionVariable('title')) {
      oErrorModel['title'] = this.createRequiredModel(true);
      bIsError = true;
    }
    if (!this.validateRequiredActionVariable('name')) {
      oErrorModel['name'] = this.createRequiredModel(true);
      bIsError = true;
    }
    if (!this.validateRequiredActionVariable('ifExecution')) {
      oErrorModel['ifExecution'] = this.createRequiredModel(true);
      bIsError = true;
    }
    if (!this.validateRequiredActionVariable('thenExecution')) {
      oErrorModel['thenExecution'] = this.createRequiredModel(true);
      bIsError = true;
    }

    if (this.parsedThens.length <= 0 && this.parsedElses.length <= 0) {
      this.error = 'At least one then/else task is required.';
      bIsError = true;
    }

    this.errors = oErrorModel;
    this.render();

    let aIfErrors = this.validateIfs();
    if (aIfErrors.length > 0) {
      this.renderErrors(aIfErrors);
      bIsError = true;
    }

    let aThenErrors = this.validateTasks('then');
    if (aThenErrors.length > 0) {
      this.renderErrors(aThenErrors);
      bIsError = true;
    }

    let aElseErrors = this.validateTasks('else');
    if (aElseErrors.length > 0) {
      this.renderErrors(aElseErrors);
      bIsError = true;
    }

    return bIsError;
  }

  validateRequiredActionVariable(sVariable) {
    if (this[sVariable] != "" && this[sVariable] != undefined) {
      return true;
    }
    return false;
  }

  createRequiredModel(bShowMessage) {
    let oErrorSegment = {
      errClass: 'is-invalid'
    }
    if (bShowMessage) {
      oErrorSegment.errMsg = 'This field is required.';
    }
    return oErrorSegment;
  }

  validateIfs() {

    let aIfErrors = [];

    this.parsedIfs.forEach((oIf, i) => {

      oIf.error = null;

      if (oIf.control.id === undefined) {
        aIfErrors.push(this.createFieldErrorEntry(`ifControl-${oIf.id}`, true, false, ''));
      }
      if (oIf.conditional === '') {
        aIfErrors.push(this.createFieldErrorEntry(`ifAction-${oIf.id}`, true, false, ''));
      }
      if (oIf.parameter === '') {
        aIfErrors.push(this.createFieldErrorEntry(`ifParameter-${oIf.id}`, true, false, ''));
      }
      if (oIf.comparator === '') {
        aIfErrors.push(this.createFieldErrorEntry(`ifComparator-${oIf.id}`, true, false, ''));
      }
      if (oIf.value === '') {
        aIfErrors.push(this.createFieldErrorEntry(`ifValue-${oIf.id}`, true, false, ''));

      } else {

        if ((oIf.comparator.toLowerCase().startsWith('int') || oIf.comparator.toLowerCase().startsWith('float')) && !this.isNumeric(oIf.value)) {
          aIfErrors.push(this.createFieldErrorEntry(`ifValue-${oIf.id}`, true, true, 'Value must be numerical.'));
        }
      }
    });

    return aIfErrors;
  }

  isNumeric(sInput) {
    if (typeof sInput != 'string') return false;
    return !isNaN(sInput) && !isNaN(parseFloat(sInput));
  }

  validateTasks(sTaskType) {

    let aTaskErrors = [];

    let aParsedTasks = this.getParsedTasks(sTaskType);

    aParsedTasks.forEach((oTask, i) => {

      oTask.error = null;

      if (oTask.control.id === undefined) {
        aTaskErrors.push(this.createFieldErrorEntry(`${sTaskType}Control-${oTask.id}`, true, false, ''));
      }
      if (oTask.action === '') {
        aTaskErrors.push(this.createFieldErrorEntry(`${sTaskType}Action-${oTask.id}`, true, false, ''));
      }

      for (var i = 0; i < oTask.parameters.length; i++) {

        let oParameter = oTask.parameters[i];

        if (oParameter.key === '' || oParameter.key === null) {
          aTaskErrors.push(this.createFieldErrorEntry(`${sTaskType}Key-${oTask.id}-${oParameter.id}`, true, false, ''));
        }
        if (oParameter.value === '' || oParameter.value === null) {
          aTaskErrors.push(this.createFieldErrorEntry(`${sTaskType}Value-${oTask.id}-${oParameter.id}`, true, false, ''));
        }
      }

      if(oTask.parameterRequired && oTask.parameters.length == 0) {
        oTask.error = "At least one parameter is required.";
      }

    });

    return aTaskErrors;
  }

  createFieldErrorEntry(sFieldId, bFieldStyling, bShowErrMsg, sErrorMsg) {
    return {
      fieldId: sFieldId,
      fieldStyling: bFieldStyling,
      showErrMsg: bShowErrMsg,
      errorMsg: sErrorMsg
    };
  }

  renderErrors(aErrors) {

    for (let i = 0; i < aErrors.length; i++) {

      if (aErrors[i].fieldStyling) {
        HomeAutomation.Util.addInputFieldErrorStyling(aErrors[i].fieldId);
      }
      if (aErrors[i].showErrMsg) {
        HomeAutomation.Util.setFieldError(`${aErrors[i].fieldId}ErrorHolder`, aErrors[i].errorMsg);
      }
    }
  }

  isIfThenSegementError() {

    let bIsError = false;

    this.ifsArray.forEach((oIf, i) => {
      if (oIf.error != null) {
        bIsError = true;
        return;
      }
    });
    if (!bIsError) {
      this.parsedThens.forEach((oThen, i) => {
        if (oThen.error != null) {
          bIsError = true;
          return;
        }
      });
    }
    return bIsError;
  }

}
