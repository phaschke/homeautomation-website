// UTILS
HomeAutomation.Util = HomeAutomation.Util || {};
import * as utilModule from './Modules/Util/Utils.js';
HomeAutomation.Util = utilModule;

HomeAutomation.Class = HomeAutomation.Class || {};
import * as messageClass from './Modules/Util/Classes/Message.js';
import * as webserviceClass from './Modules/Util/Classes/Webservice.js';
import * as filterClass from './Modules/Util/Classes/Filter.js';
HomeAutomation.Class = Object.assign({}, messageClass, webserviceClass, filterClass);

HomeAutomation.DataLoader = HomeAutomation.DataLoader || {};
import * as dataLoaderModule from './Modules/Util/DataLoader.js';
HomeAutomation.DataLoader = dataLoaderModule;

// AUTH
HomeAutomation.Auth = HomeAutomation.Auth || {};
import * as authModule from './Modules/Auth/Auth.js';
HomeAutomation.Auth = authModule;

// USERS
HomeAutomation.User = HomeAutomation.User || {};

import * as userModule from './Modules/User/User.js';
HomeAutomation.User = Object.assign({}, userModule);

import * as userServiceModule from './Modules/User/UserService.js';
HomeAutomation.User.Service = userServiceModule;

// TOPICS
HomeAutomation.Topic = {};

import * as topicModule from './Modules/Topic/Topic.js';
HomeAutomation.Topic = Object.assign({}, topicModule);

import * as topicServiceModule from './Modules/Topic/TopicService.js';
HomeAutomation.Topic.Service = topicServiceModule;


// CONTROLS
HomeAutomation.Control = {};

import * as controlModule from './Modules/Control/Control.js';
HomeAutomation.Control = Object.assign({}, controlModule);

import * as controlServiceModule from './Modules/Control/ControlService.js';
HomeAutomation.Control.Service = controlServiceModule;

import * as controlClassModule from './Modules/Control/ControlClass.js';
import * as controlBuilderClassModule from './Modules/Control/ControlBuilderClass.js';
import * as statusListBuilderClass from './Modules/Control/StatusListBuilderClass.js';
import * as eventListBuilderClass from './Modules/Control/EventListBuilderClass.js';

import * as ledControllerClassModule from './Modules/Control/LEDController/LEDControllerClass.js'
import * as ledControllerBuilderClassModule from './Modules/Control/LEDController/LEDControllerBuilderClass.js'

import * as switchClassModule from './Modules/Control/Switch/SwitchClass.js';
import * as switchBuilderClassModule from './Modules/Control/Switch/SwitchBuilderClass.js';

import * as sensorClassModule from './Modules/Control/Sensor/SensorClass.js';
import * as sensorBuilderClassModule from './Modules/Control/Sensor/SensorBuilderClass.js';

HomeAutomation.Control.Class = Object.assign({},
  controlClassModule,
  controlBuilderClassModule,
  statusListBuilderClass,
  eventListBuilderClass,
  ledControllerClassModule,
  ledControllerBuilderClassModule,
  switchClassModule,
  switchBuilderClassModule,
  sensorClassModule,
  sensorBuilderClassModule,
  );

import * as controlTimerModule from './Modules/Control/ControlTimer.js';
HomeAutomation.Control.Timer = controlTimerModule;

import * as controlActionsModule from './Modules/Control/ControlActions.js';
HomeAutomation.Control.Action = controlActionsModule;

import * as controlSearchModule from './Modules/Control/ControlSearch.js';
HomeAutomation.Control.Search = controlSearchModule;

import * as controlSwitchModule from './Modules/Control/Switch/SwitchActions.js';
HomeAutomation.Control.Switch = controlSwitchModule;

import * as ledControllerModule from './Modules/Control/LEDController/LEDControllerActions.js';
HomeAutomation.Control.LEDController = ledControllerModule;


// ACTIONS
HomeAutomation.Action = {};

import * as actionModule from './Modules/Action/Action.js';
HomeAutomation.Action = Object.assign({}, actionModule);

import * as actionServiceModule from './Modules/Action/ActionService.js';
HomeAutomation.Action.Service = actionServiceModule;

import * as actionClassModule from './Modules/Action/ActionClass.js';
HomeAutomation.Action.Class = actionClassModule;

import * as actionButtonModule from './Modules/Action/ActionButton.js';
HomeAutomation.Action.Button = Object.assign({}, actionButtonModule);

import * as actionButtonService from './Modules/Action/ActionButtonService.js';
HomeAutomation.Action.Button.Service = actionButtonService;

import * as actionButtonClassModule from './Modules/Action/ActionButtonClass.js';
HomeAutomation.Action.Button.Class = actionButtonClassModule;

// EVENTS
HomeAutomation.Event = {};
import * as eventModule from './Modules/Event/Event.js';
HomeAutomation.Event = Object.assign({}, eventModule);

import * as eventService from './Modules/Event/EventService.js';
HomeAutomation.Event.Service = eventService;

import * as eventClassModule from './Modules/Event/EventClass.js';
HomeAutomation.Event.Class = eventClassModule;

// SCHEDULES
HomeAutomation.Schedule = {};
import * as scheduleModule from './Modules/Schedule/Schedule.js'
HomeAutomation.Schedule = Object.assign({}, scheduleModule);

import * as scheduleService from './Modules/Schedule/ScheduleService.js'
HomeAutomation.Schedule.Service = scheduleService;

import * as scheduleClassModule from './Modules/Schedule/ScheduleClass.js';
HomeAutomation.Schedule.Class = scheduleClassModule;

/**
 * Register Handlebars helpers and partials
 */
import * as handlebarsSetup from './Modules/Util/HandlebarsSetup.js';
handlebarsSetup.registerHelpers();
handlebarsSetup.registerPartials();
