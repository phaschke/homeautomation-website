<?php

// Include global config
require_once("../app/Config/global_config.php");

if (isset($_GET['controller']) && $_GET['controller'] === "home") {
  // Home controller
  $controller = $_GET['controller'];
  $action = 'show';

} elseif (isset($_GET['controller']) && isset($_GET['action'])) {

  $controller = $_GET['controller'];
  $action = $_GET['action'];

} else {
  redirect(DEFAULT_PAGE_URL);
}

require_once( ROUTES_PATH.'/Router.php' );

$router = new Router();
$router->requestController($controller, $action);
