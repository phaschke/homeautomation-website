<?php

class TopicController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/topic.css',
    ]//,
    //[ 'type' => 'js',
    //  'path' => 'modules/breweries/js/Brewery.js',
    //]
  );

  /**
   * Produce the topic view
   */
  public function show() {

    if ( isset($_GET["topic"]) || $_GET["topic"]) {
      $pageContent['pageTitle'] = $_GET["topic"];
    } else {
      $pageContent['pageTitle'] = "Topic";
    }

    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      // TODO: Redirect to page not found
      redirect("BASE_URL");
      return;
    }
    $pageContent['topicId'] = $_GET["id"];

    require_once( VIEWS_PATH."/Topic.view.php" );
  }

  public function settings() {

    $pageContent['pageTitle'] = "Edit Topic";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/TopicSettings.view.php" );
  }

  public function controls() {

    if ( isset($_GET["topic"]) || $_GET["topic"]) {
      $pageContent['pageTitle'] = $_GET["topic"];
    } else {
      $pageContent['pageTitle'] = "Edit Topic";
    }

    $pageContent['pageResources'] = $this->pageResources;

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      // TODO: Redirect to page not found
      redirect("BASE_URL");
      return;
    }
    $pageContent['topicId'] = $_GET["id"];

    require_once( VIEWS_PATH."/ControlSettings.view.php" );
  }

}
