<?php

class ActionController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/action.css',
    ]
  );

  public function settings() {

    $pageContent['pageTitle'] = "Action Settings";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/ActionSettings.view.php" );
  }

  public function edit() {

    if ( !isset($_GET["id"]) || !$_GET["id"]) {
      $pageContent['isEdit'] = false;
      $pageContent['pageTitle'] = "Add Action";

    } else {
      $pageContent['isEdit'] = true;
      $pageContent['actionId'] = $_GET["id"];
      $pageContent['pageTitle'] = "Edit Action";
    }

    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/ActionAddEdit.view.php" );
  }

}
