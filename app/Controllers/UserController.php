<?php

class UserController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/user.css',
    ]
  );

  public function settings() {

    $pageContent['pageTitle'] = "User Settings";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/UserSettings.view.php" );
  }

}
