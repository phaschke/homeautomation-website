<?php

class DashboardController {

  /**
   * Define module specific page css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/dashboard.css',
    ]
  );

  /**
   * Produce the dashboard view
   * (Privileged Action)
   */
  public function show() {

    $pageContent['pageTitle'] = "Dashboard";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/Dashboard.view.php" );
  }

}
