<?php

class SchedulerController {

  /**
   * Define specific pages css/js resources
   */
  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/scheduler.css',
    ]
  );

  /**
   * Produce the topic view
   */
  public function settings() {

    $pageContent['pageTitle'] = "Scheduler Settings";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/SchedulerSettings.view.php" );
  }

}
