<?php

class EventController {

  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/event.css',
    ]
  );

  public function settings() {

    $pageContent['pageTitle'] = "Event Settings";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/EventSettings.view.php" );
  }

}
