<?php

class ChangelogController {

  private $pageResources = array(
    [ 'type' => 'css',
      'path' => 'css/changelog.css',
    ]
  );

  public function view() {

    $pageContent['pageTitle'] = "Change Log";
    $pageContent['pageResources'] = $this->pageResources;

    require_once( VIEWS_PATH."/Changelog.view.php" );
  }

}
