<?php

/**
 * Redirect to a given URL using the PHP header Location
 *
 * @param string $url The url to redirect to
 */
function redirect($url) {
  header("Location: ".$url);
  die();
}
