<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content text-center mb-4">

      <div class="row mb-2">
        <div class="col-sm-12">
          <a class="float-start btn btn-outline-primary icon-btn" title="To Main Menu" href="<?php echo BASE_URL;?>/dashboard/show"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
          <a class="float-end btn btn-outline-primary icon-btn" title="Add New Topic" onclick="HomeAutomation.Util.openModal(HomeAutomation.Topic.openAddEditModal())"><i class="float-left fas fa-plus fa-2x"></i></a>
        </div>
      </div>

      <div class="row text-center mb-2">
        <h2 class="header-text full-width">Topic Settings</h2>
      </div>

      <hr>

      <div id="topic-list-holder">
        <img class="loading-gif" src="Resources/Images/loading-white.gif">
      </div>

    </div>
  </div>

    <script>
      window.onload = function() {
        HomeAutomation.DataLoader.loadTopicSettings();
      }
    </script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
