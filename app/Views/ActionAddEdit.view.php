<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

<div class="container">
  <div class="content text-center mb-4">

    <div class="row mb-2">
      <div class="col-sm-12">
        <a class="float-start btn btn-outline-primary icon-btn" href="<?php echo BASE_URL;?>/action/settings"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
      </div>
    </div>

    <div class="text-center mb-2">
      <h2 class="header-text full-width"><?php echo $pageContent['pageTitle'];?></h2>
    </div>

    <hr>

    <div id="action-form-holder">
      <img class="loading-gif" src="Resources/Images/loading-white.gif">
    </div>

  </div>
</div>

<script>
  window.onload = function() {

    HomeAutomation.Action.displayAddEditForm(<?php echo $pageContent['actionId'];?>);
  }
</script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
