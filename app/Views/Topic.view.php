<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

    <div class="container">
      <div class="content text-center mb-4">

        <div class="row mb-2">
          <div class="col-sm-12">
            <a class="float-start btn btn-outline-primary icon-btn" href="<?php echo BASE_URL;?>/dashboard/show"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
          </div>
        </div>

        <div class="row text-center">
          <h2 class="header-text full-width" id="topic-title"><?php echo $pageContent['pageTitle'];?></h2>
        </div>

        <hr>

        <div id="action-button-holder" class="mb-2">
        </div>

        <div class="row mb-2">
          <div id="control-list-holder" class="col-sm-12">
            <img class="loading-gif" src="Resources/Images/loading-white.gif">
          </div>
        </div>
      </div>
    </div>

    <script>
      window.onload = function() {

        HomeAutomation.DataLoader.loadTopic(<?php echo $pageContent['topicId'];?>);
      }
    </script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
