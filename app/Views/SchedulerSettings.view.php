<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

<div class="container">
  <div class="content text-center mb-4">

    <div class="row mb-2">
      <div class="col-sm-12">
        <a class="float-start btn btn-outline-primary icon-btn" href="<?php echo BASE_URL;?>/dashboard/show"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
        <button class="float-end btn btn-outline-primary icon-btn" onclick="HomeAutomation.Util.openModal(HomeAutomation.Schedule.openAddEditModal())"><i class="float-left fas fa-plus fa-2x"></i></button>
      </div>
    </div>

    <div class="row text-center mb-2">
      <h2 class="header-text full-width">Scheduler Settings</h2>
    </div>

    <div class="row mb-2">
      <div class="col-sm-12">
        <p>Service Status <i class="fas fa-sync-alt icon-one-half-size icon icon-refresh" onclick="HomeAutomation.Schedule.getServiceStatus()"></i></p>
      </div>
      <div id="scheduler-status-holder" class="col-sm-12">
        <p><img class="loading-gif" src="Resources/Images/loading-white.gif"></p>
      </div>
    </div>

    <hr>

    <div class="row mb-2">
      <div id="schedule-list-holder" class="col-sm-12">
        <img class="loading-gif" src="Resources/Images/loading-white.gif">
      </div>
    </div>


  </div>
</div>

<script>
  window.onload = function() {
    HomeAutomation.Schedule.displayList();
    HomeAutomation.Schedule.getServiceStatus();
  }
</script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
