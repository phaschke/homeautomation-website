<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

    <div class="container">
      <div class="content text-center mb-4">

        <div class="row mb-2">
          <div class="col-sm-12">
            <a class="float-start btn btn-outline-primary icon-btn" href="<?php echo BASE_URL;?>/topic/settings"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
            <button class="float-end btn btn-outline-primary icon-btn" onclick="HomeAutomation.Util.openModal(HomeAutomation.Control.openAddEditModal('<?php echo $pageContent['topicId'];?>'))"><i class="float-left fas fa-plus fa-2x"></i></button>
          </div>
        </div>

        <div class="row text-center">
          <div class="col-sm-12">
            <h2>Control Settings</h2>
          </div>
        </div>

        <hr>

        <div class="row text-center mb-2">
          <div class="col-sm-12">
            <p class="topic-title" id="topic-title"><?php echo $pageContent['pageTitle'];?></p>
          </div>
        </div>

        <div class="row mb-2">
          <div id="control-list-holder" class="col-sm-12">
            <img class="loading-gif" src="Resources/Images/loading-white.gif">
          </div>
        </div>

      </div>
    </div>

    <script>
      window.onload = function() {
        HomeAutomation.Control.displayListForEdit(<?php echo $pageContent['topicId'];?>);
      }
    </script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
