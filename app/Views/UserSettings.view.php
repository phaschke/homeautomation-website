<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content text-center mb-4">

      <div class="row mb-2">
        <div class="col-sm-12">
          <a class="float-start btn btn-outline-primary icon-btn" title="To Main Menu" href="<?php echo BASE_URL;?>/dashboard/show"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
        </div>
      </div>

      <div class="row text-center mb-2">
        <h2 class="header-text full-width">User Settings</h2>
      </div>

      <hr>

      <div class="text-center mb-2" id="loggedInUserHolder">
      </div>

      <hr>

      <div class="row text-center mb-2" id="adminChangePasswordFormHolder">
      </div>

      <div class="row text-center mb-2" id="guestUserListHolder">
      </div>

    </div>
  </div>

    <script>
      window.onload = function() {
        HomeAutomation.User.displayUserSettings();
      }
    </script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
