<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

<div class="container">
  <div class="content text-center mb-4">

    <div class="row mb-2">
      <div class="col-sm-12">
        <a class="float-start btn btn-outline-primary icon-btn" href="<?php echo BASE_URL;?>/dashboard/show"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
      </div>
    </div>

    <div class="row text-center mb-2">
      <h2 class="header-text full-width">Change Log</h2>
    </div>

    <hr>

    <div class="row">
      <div class="col-md-12 text-start">

        <div class="accordion" id="accordionExample">

          <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                v1.2
              </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>3/1/2021</p>
                <ul>
                  <li>[NEW] Added changelog</li>
                  <li>[NEW] Added ordering of topics and controls</li>
                  <li>[NEW] Added (de)activing of topics and controls</li>
                  <li>[NEW] Added validatation new control broker url</li>
                  <li>[NEW] Order actions and action buttons alphabetically</li>
                  <li>[BUG] Fixed 2x speed timer countdown</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                v1.3
              </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>5/14/2021</p>
                <ul>
                  <li>[NEW] Upgraded Bootstrap v4 to v5</li>
                  <li>[BUG] Fixed login modal and callbacks</li>
                  <li>[NEW] Updated event UI including filterable searching</li>
                  <li>[NEW] Added schedules</li>
                  <li>[NEW] Advanced action selection for action buttons, events, and schedules</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                v1.4
              </button>
            </h2>
            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>6/30/2021</p>
                <ul>
                  <li>[NEW] Added Admin and 'guest' user roles</li>
                  <li>[NEW] Added frontend creation and management of 'guest' user roles</li>
                  <li>[NEW] Added 'guest' user roles per topic regulations</li>
                  <li>[NEW] Hid admin panel buttons from non admin users and limited access to backend service layer calls</li>
                  <li>[BUG] Fixed login modal not closing on successful authentication</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="accordion-item">
            <h2 class="accordion-header" id="heading1_5">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1_5" aria-expanded="false" aria-controls="collapse1_5">
                v1.5
              </button>
            </h2>
            <div id="collapse1_5" class="accordion-collapse collapse" aria-labelledby="heading1_5" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>[FUTURE]</p>
                <ul>
                  <li>[REF] Refactored topic backend functionality</li>
                  <li>[REF] Refactored control backend functionality</li>
                  <li>[REF] Refactored action backend functionality</li>
                  <li>[REF] Refactored action frontend functionality, made action add/edit a full page, with smarter action add</li>
                  <li>[NEW] Added else to action execution, IF, THEN, ELSE</li>
                </ul>
              </div>
            </div>
          </div>

      </div>
    </div>

    </div>
  </div>
</div>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
