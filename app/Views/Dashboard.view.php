<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

  <div class="container">
    <div class="content text-center mb-4">

      <div class="row text-center mb-2">
        <h2 class="header-text full-width">My Home</h2>
      </div>

      <div class="text-center">
        <a id="userSettingsLink" title="To User Settings" href="<?php echo BASE_URL;?>/user/settings"><i class="fas fa-user fa-2x"></i></a>
      </div>

      <hr>

      <div id="action-button-holder" class="mb-3">
      </div>

      <div class="row text-center">
        <div id="topic-list-holder" class="mb-3 col-12">
          <img class="loading-gif" src="Resources/Images/loading-white.gif">
        </div>
      </div>

      <hr>

      <div class="text-center">
        <div id="admin-panel" class="mb-3">
        </div>
      </div>

      <div class="row text-center">
        <div class="col-md-12">
          <a class="changelog" href="<?php echo BASE_URL;?>/changelog/view"><p><i class="fas fa-book"></i> Change Log</p></a>
        </div>
      </div>

    </div>
  </div>

  <script>
    window.onload = function() {

      HomeAutomation.DataLoader.loadDashboard();

    }
  </script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
