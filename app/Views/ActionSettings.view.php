<?php include_once SHARED_VIEWS_PATH . "/Header.part.php" ?>

<div class="container">
  <div class="content text-center mb-4">

    <div class="row mb-2">
      <div class="col-sm-12">
        <a class="float-start btn btn-outline-primary icon-btn" href="<?php echo BASE_URL;?>/dashboard/show"><i class="float-left fas fa-long-arrow-alt-left fa-2x"></i></a>
        <button class="float-end btn btn-outline-primary icon-btn" onclick="HomeAutomation.Util.openModal(HomeAutomation.Action.openAddSelectionModal())"><i class="float-left fas fa-plus fa-2x"></i></button>
      </div>
    </div>

    <div class="row text-center mb-2">
      <h2 class="header-text full-width">Action Settings</h2>
    </div>

    <hr>

    <ul class="nav nav-pills nav-fill mb-3" id="nav-tab" role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="nav-action-tab" data-bs-toggle="tab" data-bs-target="#nav-action" type="button" role="tab" aria-controls="home" aria-selected="true">Actions</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="nav-scenario-tab" data-bs-toggle="tab" data-bs-target="#nav-scenario" type="button" role="tab" aria-controls="contact" aria-selected="false">Buttons</button>
      </li>
    </ul>

    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-action" role="tabpanel" aria-labelledby="nav-action-tab">
        <div id="actions-list-holder">
          <img class="loading-gif" src="Resources/Images/loading-white.gif">
        </div>
      </div>
      <div class="tab-pane fade" id="nav-scenario" role="tabpanel" aria-labelledby="nav-scenario-tab">
        <div id="scenarios-list-holder">
          <img class="loading-gif" src="Resources/Images/loading-white.gif">
        </div>
      </div>
    </div>

  </div>
</div>

<script>
  window.onload = function() {

    HomeAutomation.Action.displayList();
    HomeAutomation.Action.Button.displayList();

  }
</script>

<?php include_once SHARED_VIEWS_PATH . "/Modal.part.php" ?>
<?php include_once SHARED_VIEWS_PATH . "/Footer.part.php" ?>
