<div class="modal" id="modal" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-content" id="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel">Loading...</h5>
          <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row text-center">
            <div class="col-sm-12">
              <img class="loading-gif" src="Resources/Images/loading-white.gif">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-custom" id="modal-custom" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-content" id="modal-content-custom">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel-custom">Loading...</h5>
          <button type="button" class="btn-close btn-close-white" onclick="HomeAutomation.Util.closeModal('modal-custom')"></button>
        </div>
        <div class="modal-body">
          <div class="row text-center">
            <div class="col-sm-12">
              <img class="loading-gif" src="Resources/Images/loading-white.gif">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="HomeAutomation.Util.closeModal('modal-custom')">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-custom" id="modal-custom1" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-content" id="modal-content-custom1">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel-custom1">Loading...</h5>
          <button type="button" class="btn-close btn-close-white" onclick="HomeAutomation.Util.closeModal('modal-custom1')"></button>
        </div>
        <div class="modal-body">
          <div class="row text-center">
            <div class="col-sm-12">
              <img class="loading-gif" src="Resources/Images/loading-white.gif">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="HomeAutomation.Util.closeModal('modal-custom1')">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
