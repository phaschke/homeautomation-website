<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href='https://fonts.googleapis.com/css?family=Nanum Gothic' rel='stylesheet'>

    <!-- This tag MUST be included on every page -->
    <base href="<?php echo BASE_URL."/" ?>"/>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="Resources/Images/home-icon64.ico">

    <!-- Use the set page title, or default -->
    <?php
      if($pageContent['pageTitle']) {
        echo "<title>".$pageContent['pageTitle']."</title>";
      } else {
        echo "<title>Home Automation</title>";
      }
    ?>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <!-- Default Style -->
    <link href="css/defaultstyle.css" rel="stylesheet">

    <!-- jQuery library -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <!-- Handlebars Templater -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.4.2/handlebars.min.js" integrity="sha256-oh7N5nthuhldTk8/34Za7FXv3BkeVN9vAnYk/pLfC78=" crossorigin="anonymous"></script>

    <!-- Color picker -->
    <script src="https://cdn.jsdelivr.net/npm/@jaames/iro@5"></script>

    <!-- Add font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>

    <script type="text/javascript" src="Javascript/Config.json"></script>
    <script src="Javascript/Templates/templates.js"></script>
    <script type="text/javascript" src="Javascript/HomeAutomation.js"></script>
    <script type="module" src="Javascript/Main.js" defer></script>

    <!-- Include any required resources as defined in the controller -->
    <?php
      if(isset($pageContent['pageResources']) && $pageContent['pageResources']) {
        foreach($pageContent['pageResources'] as $pageResource) {

          if($pageResource['type'] == 'css') {
            echo '<link href="'.$pageResource['path'].'" rel="stylesheet">';
          }
          if($pageResource['type'] == 'js') {
            echo '<script type="text/javascript" src="'.$pageResource['path'].'"></script>';
          }
        }
      }
    ?>

  </head>

  <body>

    <div id="toast-container" class="toast-container position-fixed top-0 end-0 p-3" style="z-index: 11">
    </div>
