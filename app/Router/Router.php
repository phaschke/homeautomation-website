<?php

class Router {

  private $routes;

  public function __construct() {

    // Load Routes
    $this->routes = include_once(ROUTES);
  }

  /**
   *
   */
  public function requestController($controller, $action) {

    //error_log("Controller=".$controller." & action=".$action);

    // Check requested controller and action are allowed

    // Check if controller exists and action is not privileged
    if ((array_key_exists($controller, $this->routes)) && (in_array($action, $this->routes[$controller]['public']))) {
      $this->call($controller, $action);

    // Check if controller exists and if user is logged in
    } elseif ((array_key_exists($controller, $this->routes)) && (in_array($action, $this->routes[$controller]['user']))) {

    // Check  if controller exists and action exists and is privileged
    // If privileged, check permissions, if denied, redirect to no permission error page
    } elseif ((array_key_exists($controller, $this->routes)) && (in_array($action, $this->routes[$controller]['privileged']))) {

    // Redirect to not found error page if controller and/or action does not exists
    } else {
      error_log("ERROR");
      $this->call('error', 'not_found');
    }

  }

  /**
   *
   */
  private function call($controller, $action) {

    // Require the file that matches the controller namespace
    $route = $this->routes[$controller];

    // Require the requested controller
    require_once(BASE_PATH."/".$route['folder']."/".$route['controller'].".php");

    // Dynamically create an instance of the requested controller class
    $controller = new $route['controller']();

    // Call the action on the controller
    $controller->{ $action }();

  }


}
