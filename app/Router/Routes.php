<?php

return array(

  'dashboard' => ['folder' => 'Controllers',
             'controller' => 'DashboardController',
             'public' => ['show'],
             'user' => [],
             'privileged' => []
          ],
  'topic' => ['folder' => 'Controllers',
             'controller' => 'TopicController',
             'public' => ['show', 'settings', 'controls'],
             'user' => [],
             'privileged' => []
          ],
  'event' => ['folder' => 'Controllers',
             'controller' => 'EventController',
             'public' => ['settings'],
             'user' => [],
             'privileged' => []
          ],
  'action' => ['folder' => 'Controllers',
             'controller' => 'ActionController',
             'public' => ['settings', 'edit'],
             'user' => [],
             'privileged' => []
          ],
  'scheduler' => ['folder' => 'Controllers',
             'controller' => 'SchedulerController',
             'public' => ['settings'],
             'user' => [],
             'privileged' => []
          ],
  'user' => ['folder' => 'Controllers',
             'controller' => 'UserController',
             'public' => ['settings'],
             'user' => [],
             'privileged' => []
          ],
  'changelog' => ['folder' => 'Controllers',
             'controller' => 'ChangelogController',
             'public' => ['view'],
             'user' => [],
             'privileged' => []
          ],
  'error' => ['folder' => 'Controllers',
              'controller' => 'ErrorController',
              'public' => ['not_found', 'not_implemented', 'permission_denied'],
              'user' => [],
              'privileged' => []
            ]
  );
