Home Automation Website

A web frontend to consume my home automation webservices

Used:
- PHP for page request routing. (Single entry point)
- Javascript/JQuery(AJAX) for page logic and webservice calling
- Handlebars.js for templates
